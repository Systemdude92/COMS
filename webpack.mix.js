let mix = require('laravel-mix');
let webpack = require('webpack');
let path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix




/**
  |--------------------------------------------------------------------------
  | Search Mix Configuration
  |--------------------------------------------------------------------------
  */

  .styles([
      'resources/assets/search/search.css'
  ], 'public/css/search.css')
  .babel([
      'resources/assets/search/search.js'
  ], 'public/js/search.js')

/**
  |--------------------------------------------------------------------------
  | Admin Mix Configuration
  |--------------------------------------------------------------------------
  */

  .styles([
      'resources/assets/admin/admin.css'
  ], 'public/css/admin.css')
  .babel(['resources/assets/admin/admin.js'
    ], 'public/js/admin.js')
  .babel([
      'resources/assets/admin/siteAdmin.js'
  ], 'public/js/siteAdmin.js')


/**
  |--------------------------------------------------------------------------
  | Profile Mix Configuration
  |--------------------------------------------------------------------------
  */

  .styles([
      'resources/assets/css/profilePage.css'
  ], 'public/css/profile.css')
  .babel([
      'resources/assets/js/profile.js'
  ], 'public/js/profile.js')

  /**
  |--------------------------------------------------------------------------
  | List Mix Configuration
  |--------------------------------------------------------------------------
  */

    .babel([
        'resources/assets/js/lists/lists.js',
        'resources/assets/js/lists/components/datesDataTable.js',
        'resources/assets/js/lists/components/defaultDataTable.js',
        'resources/assets/js/lists/components/feesDataTable.js'
    ], 'public/js/lists.js')
    .styles([
        'resources/assets/css/List/list.css'
    ], 'public/css/lists.css')

/**
  |--------------------------------------------------------------------------
  | Concerns Mix Configuration
  |--------------------------------------------------------------------------
  */

  .babel([
    'resources/assets/js/concerns/concernSearch.js',
    'resources/assets/js/concerns/studentForm.js'
  ], 'public/js/concerns.js')
  .styles([
      'resources/assets/css/Concerns/concerns.css',
      'resources/assets/css/Concerns/studentForm.css'
  ], 'public/css/concerns.css')


  /**
  |--------------------------------------------------------------------------
  | Dashboard Mix Configuration
  |--------------------------------------------------------------------------
  */

   .js('resources/assets/dashboard/toDoList.js', 'public/js/dashboard.js')
    .styles(['resources/assets/dashboard/dashboard.css'], 'public/css/dashboard.css')



 /**
  |--------------------------------------------------------------------------
  | Empty Layout Mix Configuration
  |--------------------------------------------------------------------------
  */

.styles([
    'resources/assets/thirdparty/semantic2/dist/semantic.css',
    'resources/assets/thirdparty/font-awesome/css/font-awesome.css',
    'resources/assets/css/main.css'
],'public/css/empty-app.css')
.babel([
    'resources/assets/thirdparty/jquery/dist/jquery.js',
    'resources/assets/thirdparty/backstretch/backstretch.js',
    'resources/assets/thirdparty/semantic2/dist/semantic.js'
],'public/js/empty-vendor.js')


 /**
  |--------------------------------------------------------------------------
  | Main Layout Mix Configuration
  |--------------------------------------------------------------------------
  */
.styles([
        'resources/assets/thirdparty/pace-1.0.2/themes/white/pace-theme-flash.css',
        'resources/assets/thirdparty/semantic-ui-calendar/dist/calendar.css'
    ], 'public/css/vendor.css')
    .styles([
        'resources/assets/css/main.css',
        'resources/assets/css/LayoutComponents/headerBar.css',

    ], 'public/css/app.css')
    .babel([
        'resources/assets/js/LayoutComponents/bugReport.js',
        'resources/assets/js/LayoutComponents/headerBar.js',
        'resources/assets/js/LayoutComponents/leftSideBar.js',
        'resources/assets/js/LayoutComponents/rightSideBar.js',
        'resources/assets/js/LayoutComponents/studentSearchTopHeaderBar.js',
        'resources/assets/js/app.js'
    ], 'public/js/app.js')
    .babel([
        'resources/assets/thirdparty/jquery/dist/jquery.js',
    ], 'public/js/manifest.js')
    .babel([
        'resources/assets/thirdparty/backstretch/backstretch.js',
        'resources/assets/thirdparty/jszip/dist/jszip.js',
        'resources/assets/thirdparty/pace-1.0.2/pace.js',
        'resources/assets/thirdparty/pdfmake/build/pdfmake.js',
        'resources/assets/thirdparty/vfs-fonts/vfs_fonts.js',
        'resources/assets/thirdparty/semantic-ui-calendar/dist/calendar.js',
        'resources/assets/thirdparty/axios/dist/axios.js'
    ], 'public/js/vendor.js')
    .copyDirectory('resources/assets/thirdparty/semantic2', 'public/semantic')
    .copyDirectory('resources/assets/thirdparty/DataTables', 'public/Datatables')
    .copyDirectory('resources/assets/img/background', 'public/img/background')
    .copyDirectory('resources/assets/img/LISDLogo', 'public/img/LISDLogo')
    .copy('resources/assets/img/blank-profile.jpg', 'public/img/blank-profile.jpg')
    .browserSync({
        proxy: 'localhost:8000',
        files: [
            'app/**/*',
            'public/**/*',
            'resources/views/**/*',
            'routes/**/*'
          ],
        open: false
    })
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude:/(node_modules)/,
                    loader: 'babel-loader',
                    query: {
                        compact: false
                    }
                },
                {
                    test: /\.css$/,
                    exclude:/(node_modules)/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.less$/,
                    exclude: /(node_modules)/,
                    use: ['style-loader', 'css-loader', 'less-loader']
                }
            ]
        },

    });

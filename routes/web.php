<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 * Route's Here do not need Authentication
 */
Route::get('/', 'LoginController@home'); //Login Page
Route::get('/logout', 'LoginController@logout')->name('logout'); //Logs User out
Route::post('/login', 'Auth\LdapLoginController@login')->name('login'); // Process of Logging Into the Application




/*
 * Route's in this Group will go through Authentication MiddleWare
 * To make sure that the User has an Active Session Open
 * with the applicaiton
*/
Route::group(['middleware' => ['web']], function(){

    // AUTHENTICATION
    Route::get('/lock', 'LoginController@lock')->name('lock'); // Locking the screen
    Route::post('/unlock', 'LoginController@unlock')->name('unlock'); // Tests to unlock the screen

    // MAIN LAYOUT CONTROLS
    Route::post('/bug/report', 'Layout\BugReportingController@index'); // Reporting Bug
    Route::get('/siteNav', 'Layout\SiteNavController@index'); // GETS SiteNav Icon for Main Layout
    Route::get('/siteNav/{id}', 'Layout\SiteNavController@redirect'); // Used to Redirect page to the appropriate Page for Site Nav
    Route::get('/settings/active', 'SettingsController@getActiveSetting'); //Gets Acive Settings
    Route::get('/settings/value/{type}', 'SettingsController@getCurrentValue'); // GETS seletected Setting values
    Route::get('/settings/values/{type}', 'SettingsController@getCurrentValues'); // GETS All information for the selected Setting
    Route::post('/settings/save', 'SettingsController@save'); //POST Saves Setting that is selected
    Route::get('/search/students', 'Layout\SearchController@studentSearch'); // GET Students from Search
    Route::get('/tech/campuses', 'TechController@campuses');
    Route::post('/tech/save', 'TechController@save');

    // Concerns Form
    Route::get('/concerns', 'ConcernsController@index')->name('concerns'); // Concerns Search Form
    Route::get('/concerns/success', 'ConcernsController@success'); // Redirects page back to Concerns with Success Message
    Route::get('/concerns/{id}', 'ConcernsController@student'); //Concerns Form For Student
    Route::post('/concerns/{id}', 'ConcernsController@save'); // POST Student Concerns Form to DB

    // Reports
    Route::get('/report/{id}', 'ConcernsController@getReport');

    // Form Functions
    Route::post('/form/note', 'FormController@addNotes');
    Route::get('/form/statuses/{id}', 'FormController@getStatuses')->name('getFormStatuses');
    Route::get('/form/{id}', 'FormController@getForm');
    Route::get('/form/payment/types', 'FormController@getPaymentTypes');
    Route::get('/form/amount/{id}', 'FormController@getAmount');

    // DASHBOARD
    Route::get('/dashboard', 'DashboardController@home')->name('dashboard');            // Dashboard
//    Route::get('/analytics','DashboardController@analytics')->name('analytics');      // Analytics Dashboard
    Route::get('/siteadmin','SiteAdminController@index')->name('siteadmin');        // redirected to Site Admin form

    // Admin Control Panel
    Route::get('/admin', 'AdminController@index')->middleware('admin');
    Route::get('/admin/user/{username}', 'AdminController@getUserInfo')->middleware('admin');
    Route::post('admin/user/{username}', 'AdminController@setUserInfo')->middleware('admin');

    // Site Admin Controls
    Route::post('/admin/kiosk', 'SiteAdminController@setKioskSettings')->middleware('admin');

    Route::get('/toDoList/{type}', 'DashboardController@toDoList'); // GETS Lists

    // List Page
    Route::get('/lists/get/{id}', 'FormController@getForms'); // Used to grab Forms with Ajax
    Route::get('/lists/{id}', 'ListController@index'); // Shows List Page for report type

    // Profile Page


    Route::post('/profile/payment','StudentProfileController@addPayment')->name('student');
    Route::get('/profile/student/{id}', 'StudentProfileController@studentProfile')->name('student');
    Route::get('/profile/payment/types', 'StudentProfileController@getPaymentTypes');
    Route::get('/profile/amount/{id}', 'StudentProfileController@getAmount');
 //   Route::post('/profile/payment','StudentProfileController@addPayment')->name('student');

    //Search Page
    Route::get('/search', 'SearchController@index');

});

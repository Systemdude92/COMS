var formsTable = $('#formsTable');
var reportID = formsTable.parent().children('h2').attr('id');
var listFilterDropDown = $('#listFilter');
listFilterDropDown.dropdown({
    onChange: function onChange(value, text, $choice) {
        getData(reportID);
    },
    useLabels: false
});

var statusFilter = $('#statusFilter');
statusFilter.dropdown({
    onChange: function onChange(value, text, $choice) {
        getData(reportID);
    },
    useLabels: false
});

$('#errorHeader').parent().children('i').on('click', function (event) {
    hideErrors();
});

getData(reportID); //Initiates DataTables

var globalForm = $('#globalListForm');

globalForm.on('submit', function (event) {
    event.preventDefault();
    var selectedForms = $("input[name='formSelection']:checked");
    var globalNote = $('#globalNotes').val();
    var status = null;
    var formIDs = [];
    if (globalNote.length > 0) {
        $.each(selectedForms, function (index, input) {
            console.log(input.value);
            formIDs.push(input.value);
        });
        if ($('#globalStatus').length > 0) {
            status = $("input[name='globalStatus']:checked");
        }
        data = {
            formID: formIDs,
            note: globalNote,
            status: status
        };
        postData(data);
    } else {
        displayError({
            header: "ERROR",
            message: 'Notes field is empty but is Required' });
    }
});

function modifyBulkPassButton() {
    var selectedForms = $("input[name='formSelection']:checked").parent().parent().parent().children('td.formTableStudentID');
    var studentIDs = [];
    var href = 'https://munisreporting/ReportServer?%2fCOMS+Reports%2fCOMS_BULK';
    $("input[name='formSelection']:checked").parent().parent().parent().children('td.formTableStudentID').each(function (index, value) {
        var id = $(value);
        if ($.inArray(id.text(), studentIDs) == -1) {
            href += '&StudentIDs=' + id.text();
            studentIDs.push(id.text());
        }
    });
    href += '&rs:Command=Render&rs:Format=PDF';
    $('#bulkOfficePassButton').prop('href', href);
    console.log(href);
}

function postData(data) {
    $.ajax({
        url: '/form/note',
        type: 'POST',
        data: data,
        success: function success(results) {
            var success = JSON.parse(results);
            var errorMessage = { header: 'ERROR!', message: '' };
            var successMessage = { header: 'SUCCESS!', message: '' };
            $.each(success, function (index, value) {
                if (value.success == 'true') {
                    successMessage.message += value.message + "<br/>";
                } else {
                    errorMessage.message += value.message + "<br/>";
                }
            });
            if (errorMessage.message.length > 0) {
                displayError(errorMessage);
            }
            if (successMessage.message.length > 0) {
                displaySuccess(successMessage);
            }
            $('#globalNotes').val('');
            deSelectSelected();
            $('#globalNotes').parent().parent().parent().hide();
            var reportID = $('#mainContentContainter').children('h2').attr('id');
            getData(reportID);
        },
        error: function error(_error) {
            displayError({
                header: 'Error',
                message: 'Error has Occured when trying to Add Note. Please Try again'
            });
        }
    });
}

function deSelectSelected() {
    var selected = $("input[name='formSelection']");
    $.each(selected, function (index, value) {
        value.checked = false;
    });
}

$(document).on('change', "input[name='formSelection']", function (event) {
    var selected = $("input[name='formSelection']:checked");
    if (selected.length > 0) {
        // Make Global Boxes appear
        $('#globalListForm').show();
        $('#bulkOfficePassButton').show();
        modifyBulkPassButton();
    } else {
        //Make Global Boxes disappear
        $('#globalListForm').hide();
        $('#bulkOfficePassButton').hide();
    }
});

function addNoteBox(formID) {
    $('#addNotes' + formID.toString()).show();
}

/**
 * filters out data if there is any filters selected
 * @param {JSON} data
 */
function filterData(data) {
    var listFilterValues = $('#listFilter').dropdown('get value');
    var statusFilterValues = $('#statusFilter').dropdown('get value');
    if (listFilterValues.length > 0) {
        var newData = [];
        var keys = Object.keys(data);
        var dataLength = keys.length;
        for (i = 0; i < dataLength; i++) {
            if (dataLength > 0) {
                if ($.inArray(data[i].reportInfo.id, listFilterValues) != -1) {
                    newData.push(data[i]);
                }
            }
        }
        data = newData;
    }
    if (statusFilterValues.length > 0) {
        var _newData = [];
        var _keys = Object.keys(data);
        var _dataLength = _keys.length;
        for (i = 0; i < _dataLength; i++) {
            if ($.inArray(data[i].status.name, statusFilterValues) != -1) {
                _newData.push(data[i]);
            }
        }
        data = _newData;
    }
    return data;
}

function getData(reportID) {
    var formsTable = $('#formsTable');
    $.ajax({
        url: '/lists/get/' + reportID,
        type: 'GET',
        success: function success(results) {
            data = JSON.parse(results);
            data = filterData(data);
            if (formsTable.attr('data-tableType') == 'feesTable') {
                buildFeesTable(data);
            } else if (formsTable.attr('data-tableType') == 'datesTable') {
                buildDatesTable(data);
            } else {
                buildFormsTable(data);
            }
        },
        error: function error(_error2) {
            displayError({
                header: 'AJAX Error',
                message: 'AJAX Failed because: ' + _error2.message
            });
        }
    });
}

function hideMessages() {
    $('#errorHeader').parent().hide();
    $('#successHeader').parent().hide();
}

function displaySuccess(success) {
    $('#successHeader').html(success.header);
    $('#successMessage').html(success.message);
    $('#successMessage').parent().show();
    setTimeout(function () {
        hideMessages();
    }, 10000);
}

/**
 *
 * @param {JSON} errors
 */
function displayError(errors) {
    $('#errorHeader').html(errors.header);
    $('#errorMessage').html(errors.message);
    $('#errorMessage').parent().show();
    setTimeout(function () {
        hideMessages();
    }, 10000);
}

$(document).on('click', 'button[id*="addNoteButton"]', function (event) {
    event.preventDefault();
    var formID = $(this).attr('id').substring(13);
    var parent = $(this).parent();
    axios.all([getFormInfo(formID), getFormStatus(formID), getPaymentTypes()]).then(axios.spread(function (info, statusInfo, paymentTypes) {
        var formInfo = info.data;
        var statuses = statusInfo.data;
        paymentTypes = paymentTypes.data;
        var newNoteSection = '\n            <form method=\'POST\' name=\'addNoteToForm\' id=\'noteForm' + formID + '\'>\n            <input type=\'hidden\' value=\'' + formID + '\' id=\'formID\'/>';
        if (formInfo.reportType == 'fee') {
            newNoteSection = addPaymentSection(formID, newNoteSection, paymentTypes);
        }
        newNoteSection += '\n            <div class=\'field\' id=\'statusField\'>\n            <label class=\'ui header\'>Change Status: </label>';
        $.each(statuses, function (index, status) {
            var checked = '';
            if (status.active) {
                checked = "checked";
            }
            newNoteSection += '\n\n                        <div id=\'checkBoxDiv' + formID + '\' class=\'ui toggle checkbox\'>\n                            <input id=\'' + status.name + formID + '\' ' + checked + ' name=\'Status\' type=\'radio\' tabindex=\'0\' class=\'hidden\' value=\'' + status.id + '\'>\n                            <label for=\'' + status.name + formID + '\' >' + status.name + '</label>\n                        </div>\n\n                ';
        });
        newNoteSection += '</div>\n            <div class=\'ui divider\'></div>\n            <div class=\'required field\'>\n                <h3 class=\'ui header\'>Add Note <span>(Required)</span></h3>\n                <textarea id=\'note\' name=\'notes\' style=\'margin: 0; width: 100%; height:157px;\'></textarea>\n            </div>';

        newNoteSection += '\n            <button class=\'ui positive button\' id=\'submitNoteFor' + formID + '\' type=\'submit\'>Submit</button>\n            <button class=\'ui negative button\' id=\'cancelButtonFor' + formID + '\' type=\'button\'>Cancel</button>\n        </form>';
        parent.empty();
        parent.append(newNoteSection);
        initInputElements(formID);
    }));
});

function initInputElements(formID) {

    if ($('#noteForm' + formID).children('#paymentTypeField').length > 0) {
        $('#noteForm' + formID).children('#paymentTypeField').children('select').dropdown();
    }

    if ($('#paymentSatisfied' + formID).length > 0) {

        var paymentSatisfied = $('#paymentSatisfied' + formID).children('div.ui.checkbox');

        paymentSatisfied.checkbox({
            onChange: function onChange() {
                var checkbox = $(this).parent();
                if (checkbox.checkbox('is checked')) {
                    $('#paymentAmount').prop('disabled', 'disabled');
                    $('#Complete' + formID).prop('checked', 'checked');
                    $.ajax({
                        url: '/form/amount/' + formID,
                        type: 'GET',
                        success: function success(amount) {
                            $('#paymentAmount').val(parseFloat(amount).toFixed(2));
                        },
                        error: function error(_error3) {
                            console.log(_error3);
                        }
                    });
                } else {
                    $('#paymentAmount').val('');
                    $('#paymentAmount').removeAttr('disabled');
                    $('#Pending' + formID).prop('checked', 'checked');
                }
            }
        });
    }
}

function addPaymentSection(formID, newNoteSection, paymentTypes) {
    newNoteSection += '\n\n    <label class=\'ui header\'>Payment:</label>\n    <div class=\'field\' id=\'paymentSatisfied' + formID + '\'>\n    <label >Satisfy Payment: </label>\n        <div class=\'ui checkbox\'>\n            <input type=\'checkbox\'/>\n            <label></label>\n        </div>\n    </div>\n    <div class=\'field\' id=\'paymentTypeField\'>\n        <label >Payment Type:</label>\n        <select class=\'ui dropdown\'>\n            <option value=\'\'>Choose Type</option>';
    $.each(paymentTypes, function (index, paymentType) {
        newNoteSection += '<option value=\'' + paymentType.id + '\'>' + paymentType.name + '</option>';
    });

    newNoteSection += '\n        </select>\n    </div>\n    <div class=\'field\' id=\'paymentField\'>\n        <label >Payment Amount: </label>\n        <div class=\'ui labeled input\'>\n            <label for=\'payment\' class=\'ui label\'>$</label>\n            <input type=\'text\' placeholder=\'Amount\' id=\'paymentAmount\' />\n        </div>\n        <label class=\'ui header\' style=\'color: RED\'>(Payment amount is applied to Student\'s COMS Balance ONLY)</label>\n    </div>\n    <div class=\'ui divider\'></div>\n    ';
    return newNoteSection;
}

function getFormStatus(formID) {
    return axios.get('/form/statuses/' + formID);
}

function getFormInfo(formID) {
    return axios.get('/form/' + formID);
}

function getPaymentTypes() {
    return axios.get('/form/payment/types');
}

$(document).on('click', 'button[id*=submitNoteFor]', function (event) {
    // event.preventDefault();
    var formID = $(this).parent().children('input[id=formID]').val();
    var readyToSubmit = true;
    var note = $(this).parent().children('div').children('textarea[id=note]').val();
    var status = $(this).parent().find('input[name=Status]:checked').val();
    var data = {
        "formID": formID,
        "note": note,
        "status": status
    };
    if ($('#paymentField').length > 0) {
        var payment = $('#paymentField').children('div').children('input').val();
        var paymentType = $('#paymentTypeField').children('div').children('select').dropdown('get value');
        if (payment.length == 0) {
            payment = 0;
        }
        if (!isNaN(payment)) {
            if (payment.length <= 0) {
                payment = '0.00';
            }
            payment = parseFloat(payment).toFixed(4);
            /* if($('#paymentSatisfied').children('div').children('input').prop('checked')){
                payment = 'satisfied';
            } */
            data.payment = payment;
            if (payment == 0) {
                paymentType = 0;
            }
            if (isNaN(paymentType) || paymentType.length == 0 && payment > 0 || payment == 'satisfied' && paymentType.length == 0) {
                readyToSubmit = false;
                displayError({
                    header: 'ERROR',
                    message: 'Payment Type is required'
                });
            } else {
                data.paymentType = paymentType;
            }
        } else {
            readyToSubmit = false;
            displayError({
                header: 'ERROR',
                message: 'Payment Value is not in the form of a Number'
            });
        }
    }

    if (note.length <= 0 || status.length <= 0 || formID.length <= 0) {
        readyToSubmit = false;
        if (note.length <= 0) {
            displayError({
                header: 'ERROR',
                message: "Missing Note Content"
            });
        }
        if (status.length <= 0) {
            displayError({
                header: 'INTERNAL ERROR',
                message: 'No Status has been Selected'
            });
        }
        if (formID.length <= 0) {
            display({
                header: 'Internal Error',
                message: "Unable to retrieve Form ID"
            });
        }
    }

    if (readyToSubmit) {
        console.log(data);
        postData(data);
    } else {
        console.log('Not Ready to Submit new Note');
    }
});

$(document).on('click', 'button[id*=cancelButtonFor]', function () {
    var parent = $(this).parent().parent();
    var formID = $(this).attr('id');
    formID = formID.substring(15);
    parent.empty();
    parent.append('<button class=\'ui primary button\' id=\'addNoteButton' + formID + '\'>Add Note</button>');
});

function buildDatesTable(data) {
    console.log(data);
    formsTable.DataTable({
        paging: true,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'inline'
            },
            orthogonal: 'responsive'
        },
        processing: true,
        data: data,
        destroy: true,
        dom: "l<'formsTableButtons'Bf>rtip",
        rowId: 'DT_RowId',
        columns: [{ 'data': 'empty',
            'orderable': false }, { 'data': 'selectBox',
            'render': function render(data, type, row, meta) {
                return '<div class=\'ui toggle checkbox\'><input name=\'formSelection\' type=\'checkbox\' value=\'' + row.DT_RowId + '\'><label></label></div>';
            },
            'orderable': false
        }, { 'data': 'studentID' }, { 'data': 'studentInfo',
            'render': function render(data, type, row, meta) {
                return "<a href='/profile/student/" + data.studentID + "' id='" + data.studentID + "'>" + data.studentLastName + ", " + data.studentFirstName + "</a>";
            }
        }, { 'data': 'reportInfo',
            'render': function render(data, type, row, meta) {
                return "<a href='/lists/" + data['id'] + "' id='" + data['id'] + "'>" + data['name'] + "</a>";
            }
        }, { 'data': 'startDate' }, { 'data': 'endDate' }, { 'data': 'submittedDate' }, { 'data': 'submittedUser' }, { 'data': 'status',
            'render': function render(status, type, row, meta) {

                status = '<div class=\'ui ' + status.color + ' horizontal label\'>' + status.name + '</div>';
                return status;
            }
        }, { 'data': 'message',
            'render': function render(description, type, row, meta) {
                var message = '\n                    <h3>Description: </h3>\n                    <p>' + description + '</p>\n                    ';
                return message;
            }
        }, { 'data': 'notes',
            'render': function render(data, type, row, meta) {
                comments = '<div id=\'noteSection' + row.DT_RowId.toString() + '\'>';
                if (data.length > 0) {
                    comments += '<div class=\'ui comments\'><div class=\'ui divider\'></div>';
                    $.each(data, function (index, note) {
                        if (index < 5) {
                            comments += '\n                                <div class=\'comment\'>\n                                    <div class=\'content\'>\n                                        <div class=\'author\'>' + note.user + '</div>\n                                    </div>\n                                    <div class=\'metadata\'>\n                                        <span class=\'date\'>' + note.submittedDate + '</span>\n                                    </div>\n                                    <div class=\'text\'>\n                                        ' + note.content + '\n                                    </div>\n                                </div>\n                                <div class=\'ui divider\'></div>\n                            ';
                        } else if (index == 5) {
                            comments += '<div class=\'ui header\'>... More ...</div>';
                            //TODO See next 5 Comments
                        } else {
                            return false;
                        }
                    });
                    comments += '</div>';
                } else {
                    comments += "No Notes have been Recorded";
                }
                comments += '</div>';
                return comments;
            }
        }, { 'data': 'writeAccess',
            'render': function render(data, type, row, meta) {
                if (data == true && row.status.noFurtherActionRequired == 0) {
                    return '<button class=\'ui primary button addNoteButton\' id=\'addNoteButton' + row.DT_RowId + '\'>Add Note</button>';
                } else {
                    return '';
                }
            }
        }],
        buttons: ['copyHtml5', 'csvHtml5', 'excelHtml5', 'pdfHtml5'],
        language: {
            thousands: ",",
            aria: {
                sortAscending: "Used to sort column ascending",
                sortDesending: "Used to sort column descending",
                paginate: {
                    first: "Return to First",
                    last: "Return to Last",
                    next: "Go to Next",
                    previous: "Return to previous"
                }
            },
            searchPlaceholder: "Student Name OR ID",
            zeroRecords: "There are no forms that fit your Settings Configuration. Please try to expand your search for more results",
            infoEmpty: "No Forms to List",
            processing: "Loading Forms",
            decimal: "."
        },
        drawCallback: function drawCallback(setting) {
            var selectors = $('input[name=formSelection]');
            $.each(selectors, function (index, select) {
                var selection = $(select);
                selection.prop('checked', false);
                $('#bulkOfficePassButton').hide();
            });
        }
    });
}

function buildFormsTable(data) {
    formsTable.DataTable({
        paging: true,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'inline'
            },
            orthogonal: 'responsive'
        },
        fixedHeader: true,
        processing: true,
        data: data,
        destroy: true,
        dom: "l<'formsTableButtons'Bf>rtip",
        rowId: 'DT_RowId',
        columns: [{ 'data': 'empty',
            'orderable': false }, { 'data': 'selectBox',
            'render': function render(data, type, row, meta) {
                return '<div class=\'ui toggle checkbox formItemSelect\'><input name=\'formSelection\' type=\'checkbox\' value=\'' + row.DT_RowId + '\'><label></label></div>';
            },
            'orderable': false
        }, { 'data': 'studentID',
            'className': 'formTableStudentID' }, { 'data': 'studentInfo',
            'render': function render(data, type, row, meta) {
                return "<a href='/profile/student/" + data.studentID + "' id='" + data.studentID + "'>" + data.studentLastName + ", " + data.studentFirstName + "</a>";
            }
        }, { 'data': 'reportInfo',
            'render': function render(report, type, row, meta) {
                return "<a href='/lists/" + report.id + "' id='" + report.id + "'>" + report.name + "</a>";
            } }, { 'data': 'submittedDate' }, { 'data': 'submittedUser' }, { 'data': 'status',
            'render': function render(status, type, row, meta) {

                status = '<div class=\'ui ' + status.color + ' horizontal label\'>' + status.name + '</div>';
                return status;
            }
        }, { 'data': 'message',
            'render': function render(description, type, row, meta) {
                var message = '\n                    <h3>Description: </h3>\n                    <p>' + description + '</p>\n                    ';
                return message;
            }
        }, { 'data': 'notes',
            'render': function render(data, type, row, meta) {
                comments = '\n                    <h3 class=\'ui header\'>Notes</h3>\n                    <div id=\'noteSection' + row.DT_RowId.toString() + '\'>';
                if (data.length > 0) {
                    comments += '<div class=\'ui comments\'><div class=\'ui divider\'></div>';
                    $.each(data, function (index, note) {
                        if (index < 5) {
                            comments += '\n                                <div class=\'comment\'>\n                                    <div class=\'content\'>\n                                        <div class=\'author\'>' + note.user + '</div>\n                                    </div>\n                                    <div class=\'metadata\'>\n                                        <span class=\'date\'>' + note.submittedDate + '</span>\n                                    </div>\n                                    <div class=\'text\'>\n                                        ' + note.content + '\n                                    </div>\n                                </div>\n                                <div class=\'ui divider\'></div>\n                            ';
                        } else if (index == 5) {
                            comments += '<div class=\'ui header\'>... More ...</div>';
                            //TODO See next 5 Comments
                        } else {
                            return false;
                        }
                    });
                    comments += '</div>';
                } else {
                    comments += "No Notes have been Recorded";
                }
                comments += '</div>';
                return comments;
            }
        }, { 'data': 'writeAccess',
            'render': function render(data, type, row, meta) {
                if (data == true && row.status.noFurtherActionRequired == 0) {
                    buttons = '<button class=\'ui primary button addNoteButton\' id=\'addNoteButton' + row.DT_RowId + '\'>Add Note</button>';
                    //TODO ADD Make Payment Button
                    return buttons;
                } else {
                    return '';
                }
            }
        }],
        buttons: ['copyHtml5', 'csvHtml5', 'excelHtml5', 'pdfHtml5'],
        language: {
            thousands: ",",
            aria: {
                sortAscending: "Used to sort column ascending",
                sortDesending: "Used to sort column descending",
                paginate: {
                    first: "Return to First",
                    last: "Return to Last",
                    next: "Go to Next",
                    previous: "Return to previous"
                }
            },
            searchPlaceholder: "Student Name OR ID",
            zeroRecords: "There are no forms that fit your Settings Configuration. Please try to expand your search for more results",
            infoEmpty: "No Forms to List",
            processing: "Loading Forms",
            decimal: "."
        },
        drawCallback: function drawCallback(setting) {
            var selectors = $('input[name=formSelection]');
            $.each(selectors, function (index, select) {
                var selection = $(select);
                selection.prop('checked', false);
                $('#bulkOfficePassButton').hide();
            });
        }
    });
}

function buildFeesTable(data) {
    console.log(data);
    formsTable.DataTable({
        paging: true,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'inline'
            },
            orthogonal: 'responsive'
        },
        processing: true,
        data: data,
        destroy: true,
        dom: "l<'formsTableButtons'Bf>rtip",
        rowId: 'DT_RowId',
        columns: [{ 'data': 'empty',
            'orderable': false }, { 'data': 'selectBox',
            'render': function render(data, type, row, meta) {
                return '<div class=\'ui toggle checkbox\'><input name=\'formSelection\' type=\'checkbox\' value=\'' + row.DT_RowId + '\'><label></label></div>';
            },
            "orderable": false
        }, { 'data': 'studentID' }, { 'data': 'studentInfo',
            'render': function render(data, type, row, meta) {
                return "<a href='/profile/student/" + data.studentID + "' id='" + data.studentID + "'>" + data.studentLastName + ", " + data.studentFirstName + "</a>";
            } }, { 'data': 'reportInfo',
            'render': function render(report, type, row, meta) {
                return "<a href='/lists/" + report.id + "' id='" + report.id + "'>" + report.name + "</a>";
            } }, { 'data': 'amountOwed',
            'render': function render(data, type, row, meta) {
                var currency = "$" + data.toString();
                return currency;
            }
        }, { 'data': 'submittedDate' }, { 'data': 'submittedUser' }, { 'data': 'status',
            'render': function render(status, type, row, meta) {

                status = '<div class=\'ui ' + status.color + ' horizontal label\'>' + status.name + '</div>';
                return status;
            }
        }, { 'data': 'message',
            'render': function render(data, type, row, meta) {
                var description = "<h3>Description: </h3>" + "<p>" + data + "</p>";
                return description;
            } }, { 'data': 'notes',
            'render': function render(data, type, row, meta) {
                comments = '<div id=\'noteSection' + row.DT_RowId.toString() + '\'>';
                if (data.length > 0) {
                    comments += '<div class=\'ui comments\'><div class=\'ui divider\'></div>';
                    $.each(data, function (index, note) {
                        if (index < 5) {
                            comments += '\n                                <div class=\'comment\'>\n                                    <div class=\'content\'>\n                                        <div class=\'author\'>' + note.user + '</div>\n                                    </div>\n                                    <div class=\'metadata\'>\n                                        <span class=\'date\'>' + note.submittedDate + '</span>\n                                    </div>\n                                    <div class=\'text\'>\n                                        ' + note.content + '\n                                    </div>\n                                </div>\n                                <div class=\'ui divider\'></div>\n                            ';
                        } else if (index == 5) {
                            comments += '<div class=\'ui header\'>... More ...</div>';
                            //TODO See next 5 Comments
                        } else {
                            return false;
                        }
                    });
                    comments += '</div>';
                } else {
                    comments += "No Notes have been Recorded";
                }
                comments += '</div>';
                return comments;
            }
        }, { 'data': 'writeAccess',
            'render': function render(data, type, row, meta) {
                if (data == true && row.status.noFurtherActionRequired == 0) {
                    return '<button class=\'ui primary button addNoteButton\' id=\'addNoteButton' + row.DT_RowId + '\'>Add Note</button>';
                } else {
                    return '';
                }
            }
        }],
        buttons: ['copyHtml5', 'csvHtml5', 'excelHtml5', 'pdfHtml5'],
        language: {
            thousands: ",",
            aria: {
                sortAscending: "Used to sort column ascending",
                sortDesending: "Used to sort column descending",
                paginate: {
                    first: "Return to First",
                    last: "Return to Last",
                    next: "Go to Next",
                    previous: "Return to previous"
                }
            },
            searchPlaceholder: "Student Name OR ID",
            zeroRecords: "There are no forms that fit your Settings Configuration. Please try to expand your search for more results",
            infoEmpty: "No Forms to List",
            processing: "Loading Forms",
            decimal: "."
        },
        drawCallback: function drawCallback(setting) {
            var selectors = $('input[name=formSelection]');
            $.each(selectors, function (index, select) {
                var selection = $(select);
                selection.prop('checked', false);
                $('#bulkOfficePassButton').hide();
            });
        }
    });
}
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

$(document).ready(function () {

  // TODO:List();

  $('.TODO:ListItem').on('click', function (event) {
    console.log($(this));
  });

  $(window).resize(function () {
    TODO: List();
  });
});

window.todoList = function () {
  clearLists();
  $.ajax({
    url: 'TODO:List/collective',
    type: 'GET',
    contentType: 'application/json; charset=utf-8',
    success: function success(results) {
      var lists = results;
      $.each(lists, function (index, value) {
        addCollectiveList(value);
      });
    },
    error: function error(errors) {
      console.log(errors);
    }
  });
  $.ajax({
    url: 'TODO:List/individual',
    type: 'GET',
    contentType: 'application/json; charset=utf-8;',
    statusCode: {
      404: function _() {
        alert('Not Found');
      }
    },
    success: function success(results) {
      //lists = JSON.parse(results);
      var lists = results;
      if (lists.length > 0) {
        $.each(lists, function (index, value) {
          addIndividualList(value);
        });
      } else {
        noReportInfo();
      }
    },
    error: function error(errors) {
      console.log(errors);
    }
  });
};

function noReportInfo() {
  var individualListDiv = $('#individualLists').parent().parent();
  individualListDiv.append('' + '<div class=\'ui message\' id=\'noReportInfo\'>' + '<div class=\'header\'> No Lists Avaiable </div>' + '<p>There are currently No Lists that have Concerns that need to be addressed at this time</p>' + '</div>' + '');
}

function clearLists() {
  $('#collectiveLists').empty();
  $('#individualLists').empty();
  if ($('#noReportInfo').length > 0) {
    $('#noReportInfo').remove();
  }
}

function addCollectiveList(list) {
  var collectiveListDiv = $('#collectiveLists');
  var description = list.description;
  var screenWidth = $(window).width();
  var displayedDescription = '';

  // if(screenWidth <= 425){
  if (description.length > 30) {
    displayedDescription = description.substring(0, 27) + '...';
  } else {
    displayedDescription = description;
  }
  /* } else {
      if(description.length > 40){
          displayedDescription = description.substring(0,37)+"...";
      } else {
          displayedDescription = description;
      }
  } */

  collectiveListDiv.append('' + '<a class=\'column item TODO:ListItem\' href=\'/lists/' + list.id + '\'>' + '<i class=\'' + list.icon + ' huge icon\' style=\'background-color: ' + list.color + '\'></i>' + '<div class=\'content\'>' + '<h3 class=\'header\'>' + list.name + '</h3>' + '<div class=\'description\' data-content=\'' + description + '\'>' + displayedDescription + '</div>' + '</div>' + '<div class=\'floating ui yellow label\'>' + list.total + '</div>' + '</a>' + '');
}

function addIndividualList(list) {
  var individualListDiv = $('#individualLists');
  var description = list.description;
  var screenWidth = $(window).width();
  var displayedDescription = '';

  if (screenWidth <= 425) {
    if (description.length > 30) {
      displayedDescription = description.substring(0, 27) + '...';
    } else {
      displayedDescription = description;
    }
  } else {
    if (description.length > 45) {
      displayedDescription = description.substring(0, 42) + '...';
    } else {
      displayedDescription = description;
    }
  }

  individualListDiv.append('<a class=\'column item TODO:ListItem\' href=\'/lists/' + list.id + '\'>' + '<i class=\'' + list.icon + ' huge icon\' style=\'background-color: ' + list.color + ';\'></i>' + '<div class=\'content\'>' + '<h3 class=\'header\'>' + list.name + '</h3>' + '<div class=\'description\' data-content=\'' + description + '\' data-position=\'bottom center\'>' + displayedDescription + '</div>' + '</div>' + '<div class=\'floating ui red label\'>' + list.total + '</div>' + '</a>');

  $('a.column.item.TODO:ListItem').children('div').children('div.description').popup();
}

/***/ })
/******/ ]);
$(document).ready(function () {
    studentSearchForForm = $('#studentFormSearch');

    studentFormInput = $('#studentFormInput');

    studentSearchForForm.search();

    studentSearchForForm.search({
        //type: 'category',
        fullTextSearch: true,
        cache: false,
        minCharacter: 3,
        maxResults: 100,
        apiSettings: {
            url: '/search/students?key={query}',
            onResponse: function onResponse(studentResults) {
                var response = {
                    results: []
                };
                var key = $('#studentFormInput').val();
                $.each(studentResults, function (index, value) {
                    var school = value.studentID || 'unknown';
                    response.results.push({
                        title: value.displayName,
                        description: 'ID: ' + value.studentID + ' | Grade: ' + value.grade,
                        url: "concerns/" + value.studentID
                    });
                });
                return response;
            }

        },
        onSelect: function onSelect(result, response) {
            console.log(result);
        },
        selectFirstResult: true
    });

    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
});

$(document).ready(function () {
    $('#detentionCheckBox').checkbox();

    reasons = $('.ui.slider.checkbox.studentForm');
    reasons.on('click', function (event) {
        event.preventDefault();
        console.log("I'm here");
        if ($('#feeDiv').length > 0) {
            $('#feeDiv').remove();
        }
        if ($('#dateDivs'.length > 0)) {
            $('#dateDivs').remove();
        }
        $(this).children('input').prop('checked', 'checked');
        var reportID = $(this).children('input').val();
        var report = $(this);
        $.ajax({
            url: "/report/" + reportID,
            type: 'GET',
            success: function success(results) {
                if (results == 'fee') {
                    addFeeBlock(report);
                } else if (results == 'dates') {
                    addDateBlocks(report);
                }
            },
            error: function error(_error) {
                console.log(_error);
            }
        });
        return true;
    });

    var form = $('#studentConcernsForm');

    form.on('submit', function (event) {
        event.preventDefault();
        showLoadingButton();

        var studentID = $('#studentID').val();
        var reason = $('input[name=reason]:checked').val();
        var description = $('#description').val();
        console.log(description);
        var data = {
            reason: reason,
            description: description
        };

        if ($('#feeDiv').length > 0) {
            var fee = $('#fee').val();
            fee = formatFee(fee);
            data.fee = fee;
        }

        if ($('#dateDivs').length > 0) {
            var startDate = $('#startDate').calendar('get date');
            var endDate = $('#endDate').calendar('get date');
            data.startDate = formatDate(startDate);
            data.endDate = formatDate(endDate);
        }

        // console.log(studentID);
        // console.log(data);

        try {
            $.ajax({
                'url': '/concerns/' + studentID,
                'type': 'POST',
                'data': data,
                'success': function success(results) {
                    console.log(results);
                    if (results == 'true') {
                        window.location.href = "/concerns/success";
                    } else {
                        showSubmitButton();
                    }
                }
            });
        } catch (exception) {
            console.log(data);
            console.log(exception);
            showSubmitButton();
        }
    });
});

function formatFee(fee) {
    if (fee.indexOf('.') > -1) {
        fee = fee.split('.');
        dollars = fee[0];
        cents = fee[1];
        if (cents.length < 2) {
            cents = cents + "00";
        };
        if (cents.length > 2) {
            cents = cents.substr(0, 2);
        };
        fee = dollars + "." + cents;
    } else {
        fee = fee + ".00";
    }
    return fee;
}

function formatDate(date) {
    year = date.getFullYear();
    month = date.getMonth();
    day = date.getDay();
    return year + "/" + month + "/" + day;
}

function addDateBlocks(input) {
    input.parent().append("" + "<div class='fields' id='dateDivs'>" + "<div class='field'>" + "<div class='ui calendar' id='startDate'>" + "<div class='ui labeled input'>" + "<label class='ui label'>Start Date</label>" + "<input type='text' placeholder='Start Date'>" + "</div>" + "</div>" + "</div>" + "<div class='field'>" + "<div class='ui calendar' id='endDate'>" + "<div class='ui labeled input'>" + "<label class='ui label'>End Date</label>" + "<input type='text' placeholder='End Date'>" + "</div>" + "</div>" + "</div>" + "</div>" + "");

    $('#startDate').calendar({
        type: 'date',
        endCalendar: $('#endDate'),
        formatter: {
            date: function date(_date, settings) {
                if (!_date) return '';
                day = _date.getDate() + '';
                if (day.length < 2) {
                    day = '0' + day;
                }
                var month = _date.getMonth() + 1 + '';
                if (month.length < 2) {
                    month = '0' + month;
                }
                var year = _date.getFullYear();
                return year + '/' + month + '/' + day;
            }
        }
    });

    $('#endDate').calendar({
        type: 'date',
        startCalendar: $('#startDate'),
        formatter: {
            date: function date(_date2, settings) {
                if (!_date2) return '';
                var day = _date2.getDate() + '';
                if (day.length < 2) {
                    day = '0' + day;
                }
                var month = _date2.getMonth() + 1 + '';
                if (month.length < 2) {
                    month = '0' + month;
                }
                var year = _date2.getFullYear();
                return year + '/' + month + '/' + day;
            }
        }
    });
}

function addFeeBlock(input) {
    input.parent().append("" + "<div class='ui labeled input' id='feeDiv'>" + "<label class='ui label'>$</label>" + "<input type='text' placeholder='Amount' id='fee'>" + "</div>" + "");

    $('#fee').blur(function () {
        if (!$.isNumeric($(this).val())) {
            $(this).val("");
        }
    });
}

function showLoadingButton() {
    var loading = $('#studentFormLoadingButton');
    var submit = $('#studentFormSubmitButton');
    var cancel = $('#studentFormCancelButton');
    cancel.hide();
    submit.hide();
    loading.show();
}

function showSubmitButton() {
    var loading = $('#studentFormLoadingButton');
    var submit = $('#studentFormSubmitButton');
    var cancel = $('#studentFormCancelButton');
    loading.hide();
    cancel.show();
    submit.show();
}
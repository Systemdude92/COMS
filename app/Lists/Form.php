<?php

namespace App\Lists;

use App\Student;
use App\User;
use App\Lists\Report;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Lists\FormStatus;
use DateTime;
use DateTimeZone;
use Exception;

class Form
{

    public const STATUS_PENDING = 'Pending';
    public const STATUS_COMPLETE = 'Complete';
    public const STATUS_PENDING_SKYWARD = 'Pending - Skyward';
    public const STATUS_NO_SHOW = 'No Show';
    public const STATUS_ON_HOLD = 'On Hold';
    private const TIME_ZONE = 'America/Chicago';

    public $id;
    public $submittedUser;
    public $submittedUserFirstName;
    public $submittedUserLastName;
    public $submittedDate;
    public $reportID;
    public $reportName;
    public $studentID;
    public $studentFirstName;
    public $studentLastName;
    public $studentGrade;
    public $schoolYear;
    public $message;
    public $feeAmount = 0;
    public $startDate;
    public $endDate;
    public $completedBy = '';
    public $completedUserFirstName = '';
    public $completedUserLastName = '';
    public $completedByName;
    public $completedDate = '';
    public $status;

    public $notes = [];

    public function __construct(int $formID = null)
    {

        if ($formID != null) {
            $coms = DB::connection('ComsDB');
            $info = $coms
                ->select("SELECT TOP 1
                Forms.id,
                Forms.studentID,
                submittedUser,
                submittedDate,
                submitted.lastName AS submittedUserLastName,
                submitted.firstName AS submittedUserFirstName,
                Forms.reportID,
                Forms.schoolYear,
                ISNULL(AliasReport.aliasName, DefaultReportLists.name) as reportName,
                message,
                feeAmount,
                startDate,
                endDate,
                completedDate,
                completedBy,
                completed.firstName AS completedUserFirstName,
                completed.lastname AS completedUserLastName,
                Forms.status,
                Students.firstName AS studentFirstName,
                Students.lastName AS studentLastName,
                Students.grade AS grade
            FROM Forms
                JOIN Students ON Students.StudentID = Forms.studentID
                JOIN Users submitted ON submitted.id = Forms.submittedUser
                LEFT JOIN Users completed ON completed.id = Forms.completedBy
                JOIN DefaultReportLists ON DefaultReportLists.id = Forms.reportID
                LEFT JOIN AliasReport ON AliasReport.ReportID = DefaultReportLists.id
                JOIN FormStatuses ON FormStatuses.id = Forms.status
                WHERE Forms.id = $formID");
            if (count($info) > 0) {
                $info = $info[0];
                $timeZoneObject = new DateTimeZone(self::TIME_ZONE);
                $this->id = $info->id;
                $this->submittedUser = $info->submittedUser;
                $this->submittedDate = new DateTime($info->submittedDate, $timeZoneObject);
                $this->submittedUserLastName = $info->submittedUserLastName;
                $this->submittedUserFirstName = $info->submittedUserFirstName;
                $this->reportID = $info->reportID;
                $this->reportName = $info->reportName;
                $this->studentID = $info->studentID;
                $this->message = $info->message;
                $this->feeAmount = $info->feeAmount;
                $this->startDate = new DateTime($info->startDate, $timeZoneObject);
                $this->endDate = new DateTime($info->endDate, $timeZoneObject);
                $this->completedBy = $info->completedBy;
                $this->completedUserFirstName = $info->completedUserFirstName;
                $this->completedUserLastName = $info->completedUserLastName;
                $this->completedDate = $info->completedDate;
                $this->status = new FormStatus($info->status);
                $this->studentFirstName = $info->studentFirstName;
                $this->studentLastName = $info->studentLastName;
                $this->studentGrade = $info->grade;
                $this->notes = self::getNotes($this->id);
                $this->schoolYear = $info->schoolYear;
            } else {
                throw new Exception('Unable to create Form Object for Form: '.$formID);
            }
        }
    }

    private function getNotes()
    {
        $id = $this->id;
        $coms = DB::connection('ComsDB');
        $notes = $coms->table('Notes')
                        ->select('Notes.id',
                                'Notes.content',
                                'Notes.submittedDate',
                                'Users.displayName AS user')
                        ->join('Users', 'Users.id', '=', 'Notes.submittedUser')
                        ->where('formID', '=', $id)
                        ->orderBy('Notes.submittedDate', 'DESC')
                        ->get();
        return $notes;
    }

    public function getFormatedNotes()
    {
        $notes = $this->notes;
        foreach($notes as $note){
            $note->submittedDate = (new DateTime($note->submittedDate))->format('F d, Y');
        }
        return $notes;

    }

    public function setStatus(string $status)
    {
        $status = new FormStatus($status);
        $userID = Session::get('user')->id;
        $comsDB = DB::connection('ComsDB');
        $comsDB->table('Forms')
                ->where('id', '=', "$this->id")
                ->update(['status' => $status->id]);
        if($status->noFurtherActionRequired){
            $comsDB->table('Forms')
                    ->where('id', '=', "$this->id")
                    ->update(['completedDate' => DB::raw('GETDATE()'),
                                'completedBy' => $userID]);

        }
    }

    public function createForm(Student $student, User $user, Report $report, array $data)
    {
        $studentID = $student->studentID;
        $submittedUser = $user->getUserID();
        $reportID = $report->id;
        $message = $data['description'];
        $feeAmount = isset($data['fee'])? $data['fee'] : null;
        $startDate = isset($data['startDate'])? $data['startDate'] : null;
        $endDate = isset($data['endDate'])? $data['endDate'] : null;
        $status = 1;

        $coms = DB::connection('ComsDB');
        $success = $coms->table('Forms')
                        ->insertGetId([
                            'studentID' => $studentID,
                            'submittedUser' => $submittedUser,
                            'submittedDate' => DB::raw('GETDATE()'),
                            'reportID' => $reportID,
                            'message' => $message,
                            'feeAmount' => $feeAmount,
                            'startDate' => $startDate,
                            'endDate' => $endDate,
                            'status' => $status,
                            'schoolYear' => $student->schoolYear
                        ]);
        if($success > 0){
            return true;
        } else {
            return false;
        }
    }

    public function addNote(string $data)
    {
        $user = Session::get('user');
        if(in_array($this->reportID, $user->writeLists)){
            $comsDB = DB::connection('ComsDB');
            $success = $comsDB->table('Notes')
                                ->insertGetId([
                                    'formID' => $this->id,
                                    'content' => $data,
                                    'submittedUser' => $user->id,
                                    'submittedDate' => DB::raw('GETDATE()')
                                ]);
            if($success)
            {
                return [
                    'success' => 'true',
                    'message' => 'Note was successfully added to Form: '.$this->id,
                ];
            } else {
                return [
                    'success' => 'false',
                    'message' => 'Note Failed to add to Form: '.$this->id
                ];
            }
        } else {
            return [
                'success' => 'false',
                'message' => 'Unable to add note. You do Not have write permissions for Report: '.$this->reportName
            ];
        }
    }

    public function writable()
    {
        $user = Session::get('user');
        if(in_array($this->reportID, $user->writeLists)){
            return true;
        } else {
            return false;
        }
    }

}

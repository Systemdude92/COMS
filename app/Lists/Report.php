<?php

/**
 * Object : List
 * Created On : McCourry, W. Logan
 * Created By : 10/19/2017
 * Description : Object to for the reports
 */

 namespace App\Lists;

 use Illuminate\Support\Facades\DB;
 use Exception;
 use Illuminate\Support\Facades\Session;
 use App\Lists\Form as Form;
 use App\Student;
 use App\User;
 use App\Settings;

class Report
{

    const COLLECTIVE_LISTS = [25, 26];
    const INDIVIDUAL_LIST_IDS = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
    const ALL_LIST = 25;
    const ALL_FINES_LIST = 26;
    const DATES_LISTS = [14,18,20,24];
    const FEE_LISTS = [1,10,11,15,17,19];
    const SINGLE_VALUE_LIST_FILTER = ['counselor', 'special', 'activity', 'other user'];
    const MULTI_VALUE_LIST_FILTER =  ['alpha', 'grade'];
    const THIRD_PARTY_SOURCE_LISTS = [11, 12, 13, 22];

    public $id = '';
    public $name = '';
    public $description = '';
    public $icon = '';
    public $color = '';
    public $fee = 0;
    public $dates = 0;
    public $forms = [];
    public $total = 0;
    public $filteredTotal = 0;
    public $filteredForms = [];

    public function __construct(string $id = null)
    {
        if (!empty($id)) {
            if (in_array($id, self::COLLECTIVE_LISTS)) {

            } else if (!in_array($id, self::INDIVIDUAL_LIST_IDS)) {
                throw new Exception("List ID: ".$id." does not exist");
            }
            $reportInfo = self::getReportInfo($id);
            $this->id = $reportInfo->ID;
            $this->name = $reportInfo->name;
            $this->description = $reportInfo->description;
            $this->icon = $reportInfo->icon;
            $this->color = $reportInfo->color;
            $this->fee = $reportInfo->Fee;
            $this->dates = $reportInfo->Dates;
            $this->total = self::getFormsThis($id);
            $this->filteredForms = self::getFilteredForms();
            $this->filteredTotal = count($this->filteredForms);
        }
    }

    /**
     * returns the report type rather Collective, Date, Fee, or Default
     *
     * @return string|null
     */
    public function getReportType()
    {
        if(!empty($this->id)){
            $reportType = '';
            if(in_array($this->id, self::COLLECTIVE_LISTS)){
                $reportType = 'collective';
            } else if(in_array($this->id, self::DATES_LISTS)){
                $reportType = 'date';
            } else if(in_array($this->id, self::FEE_LISTS)){
                $reportType = 'fee';
            } else {
                $reportType = 'default';
            }
            return $reportType;
        } else {
            return null;
        }

    }

    /**
     * Retrieves all List Names for Student Concern Form
     *
     * @return array
     */
    public function getAllListsForStudentForm()
    {
        $array = [];
        $orgCode = Session::get('user')->getOrgCode();
        $coms = DB::connection('ComsDB');
        $lists = $coms->table('DefaultReportLists')
                        ->select('DefaultReportLists.id', DB::raw("ISNULL(AliasReport.aliasName, DefaultReportLists.name) AS name"))
                        ->leftjoin('AliasReport', function($join) use ($orgCode){
                                    $join->on('AliasReport.ReportID', '=', 'DefaultReportLists.id')
                                        ->where('AliasReport.orgCode', '=', $orgCode);
                        })
                        ->where('skyward', '=', '0')
                        ->get();
        foreach($lists as $list){
            array_push($array, $list);
        }
        return $array;
    }



    /**
     * Returns the current Report Information in Array From
     * Report MUST have ID
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        if(!empty($this->id)){
            $array = [
                'id' => $this->id,
                'name' => $this->name,
                'description' => $this->description,
                'icon' => $this->icon,
                'color' => $this->color,
                'total' => $this->filteredTotal
            ];
        }
        return $array;
    }

    /**
     * Filters Forms by User Settings AND Read Access Ability
     *
     * @return array
     */
    private function getFilteredForms()
    {
        $user = Session::get('user');
        $setting = $user->getActiveSetting();
        $i = 0;
        $returnedForms = [];

        if(strtolower($setting->name) === strtolower('Other User') ){
            $setting = self::otherUserFilter($setting);
        }

        if(strtolower($setting->name) == 'all students'){
            $returnedForms = $this->forms;
        } else {
            if ($setting->name == 'All Students') {
                $returnedForms =  $this->forms;
            } elseif (in_array(strtolower($setting->name), self::MULTI_VALUE_LIST_FILTER)) {
                $returnedForms = self::filteredMultiValue($setting);
            } elseif (in_array(strtolower($setting->name), self::SINGLE_VALUE_LIST_FILTER)) {
                if($returnedForms != 'Other User') {
                    $returnedForms = self::filteredSingleValue($setting);
                } else {
                    $returnedForms = self::otherUserFilter($setting);
                }
            }
        }
        $countForms = count($returnedForms);
        $filteredForms = [];
        foreach($returnedForms as $form){
            if(in_array($form->reportID, $user->getReadableLists()) || in_array($form->reportID, $user->writeLists)){
                array_push($filteredForms, $form);
            }
        }
        return $filteredForms;
    }

    private function filteredMultiValue(Settings $setting)
    {
        $value1 = !intval($setting->value1) ? strtolower($setting->value1) : intval($setting->value1);
        $value2 = !intval($setting->value2) ? strtolower($setting->value2).'z' : intval($setting->value2);
        $filteredForms = [];
        foreach ($this->forms as $form) {
            $comparingValue = '';
            if (strtolower($setting->name) == 'alpha') {
                $comparingValue = strtolower($form->studentLastName);
            } elseif (strtolower($setting->name) == 'grade') {
                $comparingValue = $form->studentGrade;
            } else {
                throw new Exception('No Match Found for MultiValue Filter');
            }

            if(gettype($value1) == gettype($value2)){
                if($value1 != $value2){
                    if ($comparingValue >= $value1 && $comparingValue <= $value2) {
                        array_push($filteredForms, $form);
                    }
                } else {
                    if($comparingValue == $value1){
                        array_push($filteredForms, $form);
                    }
                }
            } else {
                throw new Exception("Values are not the same type");
            }

        }
        return $filteredForms;
    }

    private function filteredSingleValue(Settings $setting)
    {
        $value = $setting->value1;
        $forms = [];
        foreach ($this->forms as $form) {
            $student = new Student($form->studentID);
            $acceptedForm = false;
            if ($setting->name == 'Counselor') {
                $acceptedForm = $student->hasCounselor($value);
            } elseif ($setting->name == 'Sepcial') {
                $acceptedForm = $student->inSpecial($value);
            } elseif ($setting->name == 'Activity') {
                $acceptedForm = $student->inActivity($value);
            }
            if ($acceptedForm) {
                array_push($forms, $form);
            }
        }
        return $forms;
    }

    private function otherUserFilter(Settings $setting)
    {
        //TODO Add Other User Filter
        $otherUserID = $setting->value1;
        $otherUserActiveSetting = $setting->getOtherUserSettings($otherUserID);
        $activeSetting = new Settings($otherUserActiveSetting);
        return $activeSetting;

    }

    private function getFormsThis(string $id = null)
    {
        if($id == null){
            $id = $this->id;
        }
        $user = Session::get('user');
        $orgCode = $user->orgCode;
        $orgCodeClause = '';
        $coms = DB::connection('ComsDB');
        $forms = [];
        if ($id == self::ALL_LIST) {
            $forms = $coms->table('Forms')
                        ->select('Forms.ID')
                        ->join('Students', 'Students.StudentID', '=', 'Forms.studentID')
                        ->where('Students.orgCode', '=', $orgCode)
                        ->where(function($query){
                            $query->whereNull('completedDate')
                                    ->orWhere(DB::raw('DATEDIFF(day, completedDate, GETDATE())'), '<', '1');
                        })
                        ->orderby('Forms.studentID')
                        ->get();
        } elseif ($id == self::ALL_FINES_LIST) {
            $forms = $coms->table('Forms')
            ->select('Forms.ID')
            ->join('Students', 'Students.StudentID', '=', 'Forms.studentID')
            ->join('DefaultReportLists', 'DefaultReportLists.id', '=', 'Forms.reportID')
            ->where('Students.orgCode', $orgCode)
            ->where('DefaultReportLists.Fee', '1')
            ->where(function($query){
                $query->whereNull('completedDate')
                    ->orWhere(DB::raw('DATEDIFF(day, completedDate, GETDATE())'), '<', '1');
            })
            ->orderby('Forms.studentID')
            ->get();
        } else {
            $forms = $coms->table('Forms')
            ->select('Forms.ID')
            ->join('Students', 'Students.StudentID', '=', 'Forms.studentID')
            ->where('Students.orgCode', $orgCode)
            ->where('reportID', '=', $id)
            ->where(function($query){
                $query->whereNull('completedDate')
                    ->orWhere(DB::raw('DATEDIFF(day, completedDate, GETDATE())'), '<', '1');
            })
            ->orderby('Forms.studentID')
            ->get();
        }
        foreach ($forms as $formID) {
            $form = new Form($formID->ID);
            array_push($this->forms, $form);
        }
        return count($this->forms);
    }

    private function getReportInfo(string $id)
    {
        $orgCode = Session::get('user')->getOrgCode();
        $coms = DB::connection('ComsDB');
        $reportInfo = '';
        $reportInfo = $coms->select(
           "SELECT DefaultReportLists.ID,
                ISNULL(AliasReport.aliasName, DefaultReportLists.Name) as name,
                ISNULL(AliasReport.aliasDescription, DefaultReportLists.Description) as description,
                icon,
                color,
                Fee,
                Dates
        FROM DefaultReportLists
        LEFT JOIN AliasReport ON AliasReport.ReportID = DefaultReportLists.ID
            AND AliasReport.orgCode = $orgCode
        WHERE id = '$id'"
        );
        return $reportInfo[0];
    }

    /**
     * Returns array of Database row Objects holding Report information
     *
     * @return array
     */
    public function getAllLists($orgCode = null)
    {
        if($orgCode == null){
            $orgCode = Session::get('user')->orgCode;
        }
        $comsDB = DB::connection('ComsDB');
        $lists = $comsDB->select(
            "
            SELECT DefaultReportLists.ID AS id,
                DefaultReportLists.Name AS defaultName,
                ISNULL(AliasReport.aliasName, DefaultReportLists.Name) AS name,
                ISNULL(AliasReport.aliasDescription, DefaultReportLists.description) AS description
            FROM DefaultReportLists
                LEFT JOIN AliasReport ON AliasReport.ReportID = DefaultReportLists.ID
                    AND AliasReport.orgCode = $orgCode
            ORDER BY name
            "
        );
        return $lists;
    }

    /**
     * Returns Report information if Report ID is in the incoming Array of List IDs
     *
     * @param Array $listIDs
     * @return Array
     */
    public function getReports(Array $listIDs)
    {
        if(count($listIDs) > 0){
            $lists = [];
            $comsDB = DB::connection('ComsDB');
            $listData = $comsDB->select(
                "
                SELECT DefaultReportLists.ID as id,
                    ISNULL(AliasReport.aliasName, DefaultReportLists.Name) as name
                FROM DefaultReportLists
                    LEFT JOIN AliasReport ON AliasReport.ReportID = DefaultReportLists.ID
                ORDER BY name
                "
            );
            foreach($listData as $key => $list){
                if(in_array($list->id, $listIDs)){
                    array_push($lists, $list);
                }
            }

            return $lists;
        } else {
            return false;
        }
    }

    public function updateAlias(int $listID, string $alias)
    {
        $orgCode = Session::get('user')->orgCode;

        try{
            DB::connection('ComsDB')
                ->table('AliasReport')
                ->where('ReportID', '=', $listID)
                ->where('orgCode', '=', $orgCode)
                ->update(['aliasName' => $alias]);
        } catch(Exception $e){
            DB::connection('ComsDB')
                ->table('AliasReport')
                ->insert([
                    'ReportID' => $listID,
                    'orgCode' => $orgCode,
                    'aliasName' => $alias
                ]);
        }

    }
}

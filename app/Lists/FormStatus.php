<?php

namespace App\Lists;

use Illuminate\Support\Facades\DB;
use Exception;

class FormStatus
{

    public $id = 0; // ID of Status
    public $name = ''; // Name of Status
    public $color = '#000000'; // Hex Color Value
    public $reports = [];
    public $noFurtherActionRequired = 0; //BIT Value

    /**
     * Constructor for FormStatus Object
     *
     * @param int $id
     */
    public function __construct(int $id = null)
    {
        if(!empty($id)){
            $comsDB = DB::connection('ComsDB');
            $statusInfo = $comsDB->table('FormStatuses')
                                ->where('id', '=', $id)
                                ->first();
            $this->id = $statusInfo->id;
            $this->name = $statusInfo->name;
            $this->color = $statusInfo->color;
            $this->reports = $statusInfo->reportUses;
            $this->noFurtherActionRequired = $statusInfo->noFurtherAction;
        }
    }

    /**
     * Gets Statues for a particular Report
     *
     * @param int $reportID
     * @return array|bool
     */
    public function getStatusesForReport(int $reportID)
    {
        $comsDB = DB::connection('ComsDB');
        $statuses = $comsDB->table('FormStatuses')->get();
        $statusesToBeReturned = [];
        foreach($statuses as $status){
            $reportIDs = explode(',',$status->reportUses);
            if(in_array($reportID, $reportIDs)){
                array_push($statusesToBeReturned, $status);
            }
        }
        if(count($statusesToBeReturned) > 0 ){
            return $statusesToBeReturned;
        } else {
            throw new Exception('No statuses were added');
            return false;
        }
    }

}

<?php

namespace App\Console\DatabaseConnection;

use Illuminate\Support\Facades\DB;
use Exception;

class Coms2DB
{

    private const IPAD_LIST_TYPES = ['unpaid' => 11, 'optstatus' => 12, 'unsign' => 13];

    private const SYSTEM_ADMIN_USER_ID = 14;

    private const IPAD_LIST_UA_FORM = 13;
    private const IPAD_LIST_OPTSTATUS = 12;
    private const IPAD_LIST_NO_INSURANCE_PAYMENT = 11;

    public $conn;

    private $orgCode;

    public function __construct()
    {
        $this->conn = DB::connection('ComsDB');

        if(!$this->conn){
            throw new Exception("Unable to Connect to Coms Database for Artisan Commands");
        }
    }

    /**
     * Evaluates Students and adds/updates lists appropriate
     * based on Type
     *
     * @param string $type
     * @param array $students ex: [ Object $student1, Object $student2]
     * @return bool Success or Fail
     */
    public function updateComsIpadLists(string $type, array $students)
    {
        //Makes sure the $type variable is a valid entry
        $validType = false;
        foreach(self::IPAD_LIST_TYPES as $key => $value){
            if(strtolower($type) === strtolower($key)){
                $validType = true;
            }
        }


        if(!$validType){
            throw new Exception("iPad List Type Given is invalid");
        } else {

            //Cleaning up Variables to Save Memory
            unset($validType);


            $ipadListID = 0;

            //Obtains the IPAD LIst ID from Array
            foreach(self::IPAD_LIST_TYPES as $key => $value){
                if(strtolower($key) === strtolower($type)){
                    $ipadListID = $value;
                }
            }

            $currentEntries = [];
            try {
                //Obtains Current Entries for that iPad List
                $currentEntries = self::currentEntries($ipadListID);
            } catch(Exception $e){
                //TODO Add Error Logging
                return false;
            }

            //Loops through to make sure if student still shows up
            // ON new list and are in the Database If not, the form is resolved
            foreach($currentEntries as $entry){
                $isStillValid = false;
                foreach($students as $student){
                    if($student->studentID == $entry->studentID){
                        $isStillValid = true;
                    }
                }

                if(!$isStillValid){
                    //TODO Add Error Logging
                    self::resolveForm($entry->ID);
                }

            }

            foreach($students as $student){
                $isNewEntry = true;
                foreach($currentEntries as $entry){
                    if($entry->studentID == $student->studentID){
                        $isNewEntry = false;
                    }
                }

                if($isNewEntry){
                    //TODO Add Error Logging
                    self::insertForm($ipadListID, $student);
                }
            }

            return true;
        }

    }

    /**
     * Returns ALL Entries for
     *
     * @param int $ipadListID
     * @return array
     */
    private function currentEntries(int $ipadListID)
    {
        $currentEntries = $this->conn
                                ->select("
                                    SELECT *
                                    FROM Forms
                                    WHERE reportID = '$ipadListID'
                                ");

        return $currentEntries;
    }

    /**
     * Inserts New Form into Forms Table when student appears
     * on list but does not have a form previously
     *
     * @param int $reportID
     * @param object $studentID
     * @return bool
     */
    private function insertForm($reportID, $student)
    {
        //Assigning Constant to Variable to use in String Query
        $adminID = self::SYSTEM_ADMIN_USER_ID;

        //Assigning Variables to be used int String Query

        $studentID = $student->studentID;
        $schoolYear = $student->schoolYear;

        $message = '';
        if($reportID == self::IPAD_LIST_NO_INSURANCE_PAYMENT){
            $message = $student->insurancePaymentNote;
        } else if($reportID == self::IPAD_LIST_UA_FORM){
            $message = $student->uaNote;
        } else if($reportID == self::IPAD_LIST_OPTSTATUS){
            $message = $student->optStatusNote;
        }

        //Try to Insert Record into Database
        try{
            $this->conn->select("
                INSERT INTO Forms (studentID, submittedUser, reportID, message, status, schoolYear)
                VALUES ('$studentID', '$adminID', '$reportID', '$message', 1, $schoolYear)
            ");
        } catch(Exception $e){
            return false;
        }

        return true;

    }

    /**
     * Resolves Form when Student ID drops off the list
     *
     * @param string $formID
     * @return bool
     */
    private function resolveForm(string $formID)
    {
        //Assigning Constant to Variable to use in String Query
        $adminID = self::SYSTEM_ADMIN_USER_ID;

        //Try to Complete Form Record
        try{
            $this->conn->select("
                UPDATE Forms
                SET completedDate = GETDATE(),
                completedBy = $adminID,
                status = 2
                WHERE ID = $formID
            ");
        } catch(Exception $e){
            return false;
        }
        return true;
    }

}

<?php

namespace App\Console\DatabaseConnection;

use Illuminate\Support\Facades\DB;

class TipWeb
{

    protected $conn;

    public function __construct()
    {
        $this->conn = DB::connection('TipWeb');
    }

    public function returnedInfo($orgCode = 000)
    {

        $filter = '';
        if($orgCode != 000){
            $filter = "AND SiteID = '$orgCode'";
        }

        $results = $this->conn->select("
        SELECT returnInfo.*

        FROM

        ( SELECT returned.studentID, MAX (returnDate) as mostRecent

        FROM

        (
        SELECT tblTechInventory.tag, tblTechInventory.Serial,studentID,siteID, FirstName, LastName, tblTechInventoryHistory.LastModifiedDate returnDate
        FROM tblTechInventoryHistory
        JOIN tblStudents ON tblTechInventoryHistory.OriginEntityUID = tblStudents.StudentsUID
        JOIN tblTechSites ON tblTechInventoryHistory.SiteUID = tblTechSites.SiteUID
        JOIN tblTechInventory ON tblTechInventoryHistory.InventoryUID = tblTechInventory.InventoryUID

        WHERE

        OriginEntityTypeUID = 4
        AND tblTechInventoryHistory.entityTypeUID != 4
        AND tblTechInventoryHistory.statusUID IN ( 26, 30 )
        --and OriginEntityUID = 636509

        ) AS returned

        LEFT JOIN (

        SELECT studentID, tag, serial, LastModifiedDate
        FROM tblTechInventory
        JOIN tblStudents ON tblTechInventory.EntityUID = tblStudents.StudentsUID
        WHERE entityTypeUID = 4
        ) AS currently ON returned.StudentID = currently.StudentID

        WHERE currently.studentID is null
        GROUP BY returned.studentID
        ) as MostRecentReturn

        JOIN

        (SELECT returned.* FROM
            (
        SELECT tblTechInventory.tag, tblTechInventory.Serial,studentID,siteID, FirstName, LastName, tblTechInventoryHistory.LastModifiedDate returnDate
        FROM tblTechInventoryHistory
        JOIN tblStudents ON tblTechInventoryHistory.OriginEntityUID = tblStudents.StudentsUID
        JOIN tblTechSites ON tblTechInventoryHistory.SiteUID = tblTechSites.SiteUID
        JOIN tblTechInventory ON tblTechInventoryHistory.InventoryUID = tblTechInventory.InventoryUID
        WHERE
        OriginEntityTypeUID = 4
        AND tblTechInventoryHistory.entityTypeUID != 4
        AND tblTechInventoryHistory.statusUID  IN ( 26, 30 )
        --and OriginEntityUID = 636509
            ) AS returned

        LEFT JOIN ( SELECT studentID, tag, serial, LastModifiedDate
        FROM tblTechInventory
        JOIN tblStudents ON tblTechInventory.EntityUID = tblStudents.StudentsUID
        WHERE entityTypeUID = 4
            ) AS currently ON returned.StudentID = currently.StudentID

        WHERE currently.studentID is null) as returnInfo ON MostRecentReturn.StudentID = returnInfo.StudentID
        AND MostRecentReturn.mostRecent = returnInfo.returnDate
        AND MostRecentReturn.mostRecent > '07/01/2017'
        $filter
        order by returnInfo.studentID
            ");
        return $results;
    }

    public function tipWebInfo($orgCode = 000)
    {

        $filter = '';
        if($orgCode != 000){
            $filter = "AND tblStudents.CampusID = '$orgCode'";
        }

        $results = $this->conn->select(
            "
            SELECT
                studentID,
                lastname,
                firstname,
                middlename,
                tag,
                serial

            FROM tblstudents

            JOIN tblTechInventory ON tblStudents.StudentsUID = tblTechInventory.EntityUID
                AND dbo.tblTechInventory.EntityTypeUID = 4
                AND dbo.tblTechInventory.ArchiveUID = 0

            WHERE statusUID NOT IN (59, 87, 90)

            AND tblTechInventory.ItemUID IN
                (
                SELECT itemUID
                FROM tblTechItems
                WHERE itemTypeUID IN
                        (
                    SELECT itemTypeUID
                    FROM tblTechItemTypes
                    WHERE itemTypeName like '%IPAD%'
                        )
                )

            $filter

            ORDER BY  lastname, firstname, middlename
            "
                        );

        return $results;
    }

    public function getStudentsWithDevices($orgCode = 000)
    {

        if($orgCode != 000){
            $filter = "AND tblStudents.CampusID = $orgCode";
        } else {
            $filter = '';
        }

        $students = $this->conn
                        ->select("
                    SELECT DISTINCT
                        tblStudents.*
                    FROM tblStudents
                        JOIN tblTechInventory ON tblTechInventory.EntityUID = tblStudents.studentsUID
                        JOIN tblTechItems ON tblTechInventory.ItemUID = tblTechItems.ItemUID
                        JOIN tblTechItemTypes ON tblTechItemTypes.ItemTypeUID = tbltechItems.ItemTypeUID
                    WHERE tblStudents.ActiveIT = 1
                    AND DATEDIFF(DAY,  tblStudents.ModifiedDate, GETDATE()) < 2
                    $filter
                    ORDER BY tblStudents.LastName
                        ");
        return $students;
    }

}

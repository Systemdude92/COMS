<?php

namespace App\Console\DatabaseConnection;

use Illuminate\Support\Facades\DB;

class Pro2Sql
{

    protected $conn;

    public function __construct()
    {
        $this->conn = DB::connection('Pro2Sql');
    }

    public function currentStudents($orgCode = 000)
    {
        $filter = '';
        if($orgCode != 000){
            $filter = "AND entity.entity_id = $orgCode";
        }

        $results = $this->conn->select(
            "
            SELECT entity.entity_id AS schoolCode,

            entity.entity_name AS schoolName,

            student.student_id,

            student.other_id AS studentID,

            grade,

            name.last_name,

            name.first_name,
            
            CurrentYear.school_year

            FROM entity

            JOIN CALENDAR_MASTER ON ENTITY.ENTITY_ID = CALENDAR_MASTER.ENTITY_ID
            AND ENTITY.SCHOOL_YEAR = CALENDAR_MASTER.SCHOOL_YEAR
            AND CALENDAR_MASTER.CALENDAR_ID != 'PK'

            JOIN student_ew ON entity.entity_id = student_ew.entity_id
            AND ( student_ew.withdrawal_date IS NULL OR student_ew.withdrawal_date > getDate() )
            AND student_ew.percent_enrolled > 0
            AND STUDENT_EW.EW_DATE <= CALENDAR_MASTER.CAL_STP_DTE
            AND STUDENT_EW.EW_DATE >= CALENDAR_MASTER.CAL_STR_DTE

            JOIN student ON student_ew.student_id =  student.student_id
            JOIN student_entity ON student_entity.student_id = student.student_id

            JOIN name ON student.name_id = name.name_id
            
            JOIN entity CurrentYear ON CurrentYear.entity_id = '000'

            JOIN ent_grd_gy_xref ON student.grad_yr = ent_grd_gy_xref.grad_yr
            AND entity.school_year = ent_grd_gy_xref.school_year
            WHERE student_entity.x_default_entity = 1
            AND student_entity.student_status = 'A'
            $filter
            "
        );

        return $results;
    }

    public function noIpadFeeTable($orgCode = 000)
    {
        $filter = "";
        if($orgCode != 000){
            $filter = "AND entity.entity_id = $orgCode";
        }
        $results = $this->conn->select(
            "
            SELECT DISTINCT entity.entity_id, student.other_id, student.full_name_lfm,fm_bill_code, fm_bd_desc,  student.student_id
        FROM ENTITY
        JOIN FM_BILLING_DETAIL ON ENTITY.ENTITY_ID = FM_BILLING_DETAIL.entity_id
        AND  FM_BILLING_DETAIL.fm_bd_school_year = ENTITY.school_year

        JOIN FM_BILL_CODE ON FM_BILLING_DETAIL.fm_bc_id = FM_BILL_CODE.fm_bc_id

        JOIN student on FM_BILLING_DETAIL.fm_name_id_payor = student.name_id

        JOIN CALENDAR_MASTER ON ENTITY.ENTITY_ID = CALENDAR_MASTER.ENTITY_ID
        AND ENTITY.SCHOOL_YEAR = CALENDAR_MASTER.SCHOOL_YEAR

        JOIN student_ew ON  student.student_id = student_ew.student_id
                            AND (student_ew.withdrawal_date IS NULL OR student_ew.withdrawal_date > getDate() )
                            and student_ew.percent_enrolled > 0
        AND STUDENT_EW.EW_DATE <= CALENDAR_MASTER.CAL_STP_DTE
        AND STUDENT_EW.EW_DATE >= CALENDAR_MASTER.CAL_STR_DTE

        WHERE FM_BILL_CODE.FM_BILL_CODE IN (  'NOIPAD'  )

        and fm_bd_date_added >= '07/01/2017'

        AND FM_BILLING_DETAIL.fm_name_id_payor NOT IN (

        SELECT  student.student_id
        FROM ENTITY
        JOIN FM_BILLING_DETAIL ON ENTITY.ENTITY_ID = FM_BILLING_DETAIL.entity_id
        AND  FM_BILLING_DETAIL.fm_bd_school_year = ENTITY.school_year

        JOIN FM_BILL_CODE ON FM_BILLING_DETAIL.fm_bc_id = FM_BILL_CODE.fm_bc_id

        JOIN student on FM_BILLING_DETAIL.fm_name_id_payor = student.name_id

        JOIN CALENDAR_MASTER ON ENTITY.ENTITY_ID = CALENDAR_MASTER.ENTITY_ID
        AND ENTITY.SCHOOL_YEAR = CALENDAR_MASTER.SCHOOL_YEAR

        JOIN student_ew ON  student.student_id = student_ew.student_id
                            AND (student_ew.withdrawal_date IS NULL OR student_ew.withdrawal_date > getDate() )
                            and student_ew.percent_enrolled > 0
        AND STUDENT_EW.EW_DATE <= CALENDAR_MASTER.CAL_STP_DTE
        AND STUDENT_EW.EW_DATE >= CALENDAR_MASTER.CAL_STR_DTE

        WHERE FM_BILL_CODE.FM_BILL_CODE IN (  'NOIPAD'  )
        AND FM_BD_DESC LIKE '%VOID%'
        and fm_bd_date_added >= '07/01/2017'
        )
        $filter
            "
        );

        return $results;
    }

    public function parentOptInOrOut($orgCode = 000)
    {

        $filter = '';
        if($orgCode != 000){
            $filter = "AND student_entity.entity_id = $orgCode";
        }

        $results = $this->conn->select(
            "
            SELECT quddat_src_id AS skyID,

                quddat_logical AS optOut, --field not used any more

                CASE WHEN quddat_char = 'OPT IN' THEN 'OptIn'
                WHEN quddat_char = 'OPT OUT' THEN 'OptOut'
                ELSE 'ERROR'
                END AS iPadOption,

                modified_date,
                student.other_id

            FROM quddat_data

            LEFT JOIN student ON quddat_data.quddat_src_id = student.name_id
            LEFT JOIN student_entity ON student_entity.student_id = student.name_id

            WHERE qudtbl_table_id = 59

                AND qudfld_field_id = 6816 --3206 2017 value

                AND quddat_storage_type = 'Custom Student'

                AND quddat_unique_id = 0

                AND quddat_char != ''

                $filter

                ORDER BY modified_date
            "
        );

        return $results;
    }

    public function ipadInsurance($orgCode = 000)
    {
        $filter = '';
        if($orgCode != 000){
            $filter = "WHERE entity.entity_id = $orgCode";
        }

        $results = $this->conn->select("
        SELECT student_ew.entity_id as campus,
            full_name_lfm,
            other_id as studentID,
            insurranceOption,
            CASE WHEN insurranceOption IS NULL THEN 'No Insurance'
            WHEN insurranceOption IN ( 'FREE YEAR ',  '1:X Insurance' ) THEN 'Replacement Option'
            WHEN insurranceOption IN (  'CarryFwd 1:X Dp', '1:X Deposit' ) THEN 'One Time refundable deposit'
            ELSE 'ERROR' END AS iPadOption
        FROM ENTITY

        JOIN CALENDAR_MASTER ON ENTITY.ENTITY_ID = CALENDAR_MASTER.ENTITY_ID
        AND ENTITY.SCHOOL_YEAR = CALENDAR_MASTER.SCHOOL_YEAR

            JOIN STUDENT_EW ON ENTITY.ENTITY_ID = STUDENT_EW.ENTITY_ID
        AND STUDENT_EW.EW_DATE <= CALENDAR_MASTER.CAL_STP_DTE
        AND STUDENT_EW.EW_DATE >= CALENDAR_MASTER.CAL_STR_DTE
            AND student_ew.percent_enrolled > 0
            AND (student_ew.withdrawal_date IS NULL OR student_ew.withdrawal_date > getDate())
            JOIN STUDENT ON STUDENT_EW.STUDENT_ID = STUDENT.STUDENT_ID
            JOIN NAME ON STUDENT.NAME_ID = NAME.NAME_ID
            LEFT JOIN (

        SELECT distinct skyID,
            --campus,
            other_id AS studentID,
            studentName,
            fm_bc_sdesc as insurranceOption,
            'Fully Paid' as paymentStatus
        FROM (

        SELECT student.student_id AS skyID, FM_BILLING_DETAIL.entity_id AS campus, student.other_id,student.full_name_lfm AS studentName, FM_BILLING_DETAIL.name_id, fm_bill_code, fm_bc_sdesc, SUM( fm_bd_amount ) as totalBilled, SUM(fm_bd_paid_amt) AS totalPaid, count(*) as NumOfRows
        FROM ENTITY
            JOIN FM_BILLING_DETAIL ON ENTITY.ENTITY_ID = FM_BILLING_DETAIL.entity_id
            AND FM_BILLING_DETAIL.fm_bd_school_year = ENTITY.school_year
            JOIN FM_BILL_CODE ON FM_BILLING_DETAIL.fm_bc_id = FM_BILL_CODE.fm_bc_id
            JOIN student on FM_BILLING_DETAIL.name_id = student.name_id

        JOIN CALENDAR_MASTER ON ENTITY.ENTITY_ID = CALENDAR_MASTER.ENTITY_ID
        AND ENTITY.SCHOOL_YEAR = CALENDAR_MASTER.SCHOOL_YEAR

            JOIN student_ew ON student.student_id = student_ew.student_id
        AND STUDENT_EW.EW_DATE <= CALENDAR_MASTER.CAL_STP_DTE
        AND STUDENT_EW.EW_DATE >= CALENDAR_MASTER.CAL_STR_DTE
            AND student_ew.percent_enrolled > 0
            AND (student_ew.withdrawal_date IS NULL OR student_ew.withdrawal_date > getDate())
        WHERE FM_BILL_CODE.FM_BILL_CODE IN ('1:x','CF1:x','1XI','BOGO16')
            AND fm_bd_date_added >= '07/01/2017'
        --and name_id = 23558
        GROUP BY student.student_id, FM_BILLING_DETAIL.entity_id, student.other_id,student.full_name_lfm, FM_BILLING_DETAIL.name_id, fm_bill_code, fm_bc_sdesc

        ) AS allPeople
        WHERE totalBilled = totalPaid
            AND NOT (NumOfRows > 1 AND totalBilled = 0)
            AND campus NOT IN ('007', '085')

        ) AS iPadOption ON student.student_id = iPadOption.skyID
        $filter
        ");



        return $results;
    }

    public function getRevTracStudents()
    {

        $results = $this->conn->select("
            SELECT FM_BD_ID_CHARGE, fm_name_id_payor FROM (

                SELECT FM_BD_ID_CHARGE, fm_name_id_payor, SUM (fm_bd_payment_amt) as totals
                FROM
                ( SELECT FM_BILL_CODE.FM_BC_SCHOOL_YEAR, FM_BILL_CODE.ENTITY_ID, fm_billing_detail.fm_name_id_payor,

                FM_TRACK_PAYMENTS.FM_BD_ID_CHARGE, FM_TRACK_PAYMENTS.fm_bd_payment_amt,

                FM_BILL_CODE.FM_BC_LDESC, FM_BILLING_DETAIL.FM_BD_AMOUNT AS totalbilled, FM_BILLING_DETAIL.FM_BD_PAID_AMT AS totalPaid,

                payments.FM_BD_PAID_AMT AS revTrakPaid, FM_BILLING_DETAIL.fm_bd_date_added

                FROM ENTITY

                JOIN FM_BILL_CODE ON ENTITY.ENTITY_ID = FM_BILL_CODE.ENTITY_ID
                AND ENTITY.SCHOOL_YEAR = FM_BILL_CODE.FM_BC_SCHOOL_YEAR

                JOIN FM_BILLING_DETAIL ON FM_BILL_CODE.FM_BC_ID = FM_BILLING_DETAIL.FM_BC_ID

                JOIN FM_TRACK_PAYMENTS ON FM_TRACK_PAYMENTS.FM_BD_ID_CHARGE = FM_BILLING_DETAIL.FM_BD_ID

                JOIN FM_BILLING_DETAIL AS payments ON FM_TRACK_PAYMENTS.FM_BD_ID_PAYMENT = payments.fm_bd_id

                JOIN FM_BILL_CODE AS revTrakPayment ON payments.FM_BC_ID = revTrakPayment.FM_BC_ID
                AND revTrakPayment.FM_BILL_CODE = 'PREVTRAK'

                WHERE FM_BILL_CODE.FM_BILL_CODE IN ('1XI','CF1:X','PCF1:XDP')
                AND FM_BILLING_DETAIL.fm_bd_date_added >= '07/01/'+ CAST( ENTITY.SCHOOL_YEAR - 1 AS VARCHAR(10) )

                ) AS paidTotals

                GROUP BY FM_BD_ID_CHARGE, fm_name_id_payor
                --order by totals
                ) as totalQuery
                WHERE totals >= .01
                ORDER BY totals
        ");

        return $results;
    }

}

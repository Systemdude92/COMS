<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;

class Student
{

    public $studentID;
    public $skywardID;
    public $tipweb = 0;
    public $optStatus = 0;
    public $insurancePayment = 0;
    public $uaSigned = 0;
    public $noIpadPayment = 0;
    public $uaNote = "";
    public $optStatusNote = "";
    public $insurancePaymentNote = "";
    public $schoolYear = 0;

    public $firstName = "";
    public $lastName = "";

    public function __construct($studentInfo)
    {
        $this->studentID = $studentInfo->studentID;
        $this->firstName = $studentInfo->first_name;
        $this->lastName = $studentInfo->last_name;
        $this->skywardID = $studentInfo->student_id;
        $this->schoolYear = $studentInfo->school_year;
    }

}

<?php

namespace App\Console\Commands\iPad;

use App\Console\DatabaseConnection\Coms2DB;
use DateTimeZone;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Console\DatabaseConnection\Pro2Sql;
use App\Console\DatabaseConnection\TipWeb;
use App\Console\DatabaseConnection\SkywardProdDB;
use App\Console\Commands\Student;
use SplFileObject;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ipad:update {--orgcode=000}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates iPad tables';


    /**
     * Logging Directory for the Day
     *
     * @var string
     */
    protected $logDirectory = "";

    /**
     * Logging File for the Day And/OR School
     *
     * @var SplFileObject
     */
    protected $logFile;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $currentDateString = date('Y-m-d');

        if(!file_exists(__DIR__ . "/../../../../logs/$currentDateString")){
            $isSuccessful = mkdir(__DIR__ . "/../../../../logs/$currentDateString");
            if(!$isSuccessful){
                throw new Exception("Unable to make Directory");
            }
        }

        $this->logDirectory = __DIR__ . "/../../../../logs/$currentDateString";



    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $orgCode = $this->option('orgcode');

        if($this->hasOption('orgcode')){

            if(!file_exists($this->logDirectory."/$orgCode")){
                mkdir($this->logDirectory."/$orgCode");
            }

            $this->logDirectory = $this->logDirectory."/$orgCode";
        }

        $this->logFile = new SplFileObject($this->logDirectory."/log.txt", 'c+');

        $timeZone = new DateTimeZone("America/Chicago");

        $startTime = new DateTime("now", $timeZone);
        $pro2sql = new Pro2Sql();
        $tipWeb = new TipWeb();

        $this->logFile->fwrite("[".(new DateTime('now', $timeZone))->format('Y-m-d h:i:s A')."] - iPad List Update Process Started");

        $orgCodes = [];

        if($orgCode == 000){
            $orgs = DB::connection('SkywardDataExtracts')
                    ->select(/** @lang SQL */
                        "
                        SELECT schoolCode
                        FROM tempIpadRecon_schoolCode_list
                    ");
            foreach($orgs as $org){
                array_push($orgCodes, $org->schoolCode);
            }
            unset($orgs);
        } else {
            array_push($orgCodes, $orgCode);
        }

        $noInsuranceStudents = []; // Used for Students that Have not Paid Insurance and NEEDS to
        $noUAFormStudents = []; // Used for Students that need to Sign their UA Form
        $optStatusStudents = []; //Used for Students have an Opt in/out conflict

        foreach($orgCodes as $orgCode) {
            $this->comment('Starting Update for: ' . $orgCode);

            // Obtaining Current Students for Campus
            $currentStudentsTable = $pro2sql->currentStudents($orgCode);
            $students = [];

            //Creating Student Objects
            $this->comment('Student Object Transformation');
            foreach ($currentStudentsTable as $studentTable) {
                $stu = new Student($studentTable);
                array_push($students, $stu);
            }
            unset($currentStudentsTable);
            $this->comment('Starting TipWeb Compare');
            $tipWebInfo = $tipWeb->tipWebInfo($orgCode);
            foreach ($students as $student) {
                foreach ($tipWebInfo as $info) {
                    if ($info->studentID == $student->studentID) {
                        $student->tipweb = 1;
                    }
                }
            }
            unset($tipWebInfo);
            $this->comment('Starting Fee Management Compare for '.$orgCode);
            $feeManagement = $pro2sql->ipadInsurance($orgCode);
            foreach ($students as $student) {
                foreach ($feeManagement as $fee) {
                    if ($fee->studentID == $student->studentID && $fee->iPadOption != 'No Insurance') {
                        $student->insurancePayment = 1;
                    }
                }
            }
            unset($feeManagement);

            // Starting Opt In/Out Login to figure out
            // 1: If they have signed the UA
            // 2: If they did, have they opted in or out to obtain an iPad
            $this->comment('Starting Opt Status Compare for '.$orgCode);
            $optStatusTable = $pro2sql->parentOptInOrOut($orgCode);
            foreach ($students as $student) {
                foreach ($optStatusTable as $optStatus) {
                    if($optStatus->skyID == $student->skywardID){
                        $student->uaSigned = 1;
                    }
                    if ($optStatus->skyID == $student->skywardID && $optStatus->iPadOption == 'OptIn') {
                        $student->optStatus = 1;
                    }
                }
            }
            unset($optStatusTable);

            //Starts Zero out process for for Returned iPads
            // If a student had returned an iPad, the stu
            $this->comment('Start Zero Out for Returned iPads for '.$orgCode);
            $returnedDevices = $tipWeb->returnedInfo($orgCode);
            foreach ($students as $student) {
                foreach ($returnedDevices as $device) {
                    if ($device->studentID == $student->studentID) {
                        $student->tipWeb = 0;
                        $student->insurancePayment = 0;
                        $student->optStatus = 0;
                        $student->uaSigned = 0;
                    }
                }
            }
            unset($returnedDevices);


            $this->comment("Starting No iPad Payment Type Compare for ".$orgCode);
            $noIpadFee = $pro2sql->noIpadFeeTable();
            foreach ($students as $student) {
                foreach ($noIpadFee as $noIpad) {
                    if ($student->studentID == $noIpad->other_id) {
                        $student->insurancePayment = 0;
                        $student->noIpadPayment = 1;
                    }
                }
            }
            unset($noIpadFee);

            //Searches to see if Student paid through RevTrak
            //If So, updates optStatus and uaSigned to 1
            $this->comment('Start RevTrak Compare for '.$orgCode);
            $revTrac = $pro2sql->getRevTracStudents();
            foreach ($students as $student) {
                foreach ($revTrac as $rev) {
                    if ($student->skywardID == $rev->fm_name_id_payor) {
                        $student->optStatus = 1;
                        $student->uaSigned = 1;
                    }
                }
            }
            unset($revTrac);

            $this->comment('Starting Student Evaluation for No Payments for '.$orgCode);

            // Starts Evalucations for COMS 2 iPad Forms
            foreach ($students as $student) {

                // Evaluating if the Current Student has not paid their Insurance for the Following Reasons:
                // 1: They have an iPad and have not Paid
                // 2: they have Opted In and they still need to pay for insurance
                if (($student->insurancePayment == 0 && $student->tipweb == 1 & $student->noIpadPayment == 0)) {
                    $student->insurancePaymentNote = $student->firstName." ".$student->lastName." has an iPad but has not paid for his/her insurance.";
                    array_push($noInsuranceStudents, $student);
                } else if(($student->insurancePayment == 0 && $student->optStatus == 1 && $student->tipweb == 0 && $student->noIpadPayment == 0)){
                    $student->insurancePaymentNote = $student->firstName." ".$student->lastName." has opted in for a district iPad but has not yet paid for his/her insurance";
                    array_push($noInsuranceStudents, $student);
                }

                //Evaluates current student to see if they have an iPad but have not signed their User Agreement Form
                if($student->uaSigned == 0 && $student->tipweb == 1){
                    $student->uaNote = 'StudentID: '.$student->studentID.': '.$student->firstName." ".$student->lastName." has an iPad but has NOT signed their User Agreement Form";
                    array_push($noUAFormStudents, $student);
                }

                //Evaluated current student to see if they have opted Status:
                // 1: If opted in, does Student have an iPad and have have paperwork filed properly
                // 2: If opted out, does the Student not have an iPad
                if($student->optStatus == 0 && $student->tipweb == 1 && $student->uaSigned == 1){
                    $student->optStatusNote = "StudentID: ".$student->studentID.': '.$student->firstName.' '.$student->lastName.' has an iPad but has Opted Out of the District iPad Program';
                    array_push($optStatusStudents, $student);
                }
                /*if($student->optStatus == 1 && $student->tipweb == 0 && $student->uaSigned == 1 && $student->insurancePayment == 1){
                    $student->optStatusNote = "StudentID: ".$student->studentID.': '.$student->firstName.' '.$student->lastName.' does not have an iPad but has completed all necessary documentation to receive one';
                    array_push($optStatusStudents, $student);
                }*/


            }

            $this->comment("End Process for ".$orgCode);

            // Used to Find Student for Testing
            /*$this->comment("Searching for Test Student");
            foreach($students as $student){
                if($student->studentID == 402289){  // Use LISD Student ID for comparison. Will print out Student Info
                    $this->comment(print_r($student));
                }
            }*/
        }

        unset($students);
        unset($orgCodes);
        $this->comment("End Evaluation Process");

        //Start Updating COMS Lists
        $this->comment("Start COMS Update Process");

        $count = count($noInsuranceStudents)+count($optStatusStudents)+count($noUAFormStudents);

        $coms = new Coms2DB();

        $this->comment('Start Update for No Insurance');
        //Update Insurance List
        $coms->updateComsIpadLists('unpaid', $noInsuranceStudents);
        unset($noInsuranceStudents);

        $this->comment('Start Update for No UA Form Signed');
        //Update No UA List
        $coms->updateComsIpadLists('unsign', $noUAFormStudents);
        unset($noUAFormStudents);

        $this->comment('Start Update for Opt-Status Conflicts');
        //Update Opt Status Lists
        $coms->updateComsIpadLists('optStatus', $optStatusStudents);
        unset($optStatusStudents);

        $this->comment("End COMS Update Process");



        //Script Analytics
        $endTime = new DateTime();
        $ellapsedTime = $endTime->diff($startTime)->format('%i:%S');

//        $count = count($noInsuranceStudents)+count($optStatusStudents)+count($noUAFormStudents);

//        $this->comment(print_r($noInsuranceStudents));
//        $count = count($noInsuranceStudents);

        $this->comment("
            Script Runtime: $ellapsedTime
            Number of Evaluated Students: $count
        ");



        return true;

    }
}

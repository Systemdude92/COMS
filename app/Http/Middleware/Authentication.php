<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\User;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if($request->is('/')
            || $request->is('login')
            || $request->is('logout')){
                return $next($request);
        } else {
            if($request->session()->has('user')){
                $user = $request->session()->get('user');
                if($request->is('lock')
                || $request->is('unlock')){
                    return $next($request);
                }
                if($user->state == 'locked'){
                    return redirect('/lock');
                } else if($user->comsRole != $user::USER_NORMAL
                            || $request->is('concerns')
                            || $request->is('concerns/*')
                            || $request->is('concerns/success')
                            || $request->is('search/students')
                            || $request->is('settings/*')){
                    return $next($request);
                } else {
                    return redirect('/')->withErrors('Not Allowed at that location');
                }
            } else {
                return redirect('/')->withErrors('Please Login');
            }
        }

    }
}

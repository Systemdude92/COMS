<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\User;

class AdminMiddleWare
{

    /**
     * Validatates that the current User is an Admin or Site Admin to access page
     *
     * @param Request $request
     * @param Closure $next
     * @return void
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->session()->get('user');
        if($user->isAdmin() || $user->isSiteAdmin()){
            return $next($request);
        } else {
            return redirect('/dashboard');
        }
    }

}

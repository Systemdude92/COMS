<?php

/*
    |--------------------------------------------------------------------------
    | login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen.
    */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\LDAP;


class LoginController extends Controller
{


    public function home(Request $request)
    {
        if($request->session()->has('user')){
            // $request->session()->forget('user');
        }
        return view('login/login');
    }

    public function logout(Request $request)
    {
        if($request->session()->has('user')){
            $request->session()->forget('user');
        }
        return redirect()->action('LoginController@home');
    }

    public function lock(Request $request)
    {
        if($request->session()->has('user')){
            $request->session()->get('user')->setState('locked');
            return view('login/locked');
        } else {
            return view('login/login');
        }
    }

    public function unlock(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if($request->isMethod('post')){
            $this->validate($request,
                [
                    'password' => 'required'
                ]
            );
            $ldap = new LDAP();
            $unlocked = $ldap->ldapLogin($username, $password);
            if($unlocked){
                $request->session()->get('user')->setState('active');;
                return redirect()->action('DashboardController@home');
            } else {
                return back()->withErrors('Incorrect Password');
            }
        } else {
            return view('login/locked');
        }
    }



}

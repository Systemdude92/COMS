<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lists\Report;
use App\Student;
use App\User;
use App\Lists\Form;
use DateTime;

class ConcernsController extends Controller
{
    public function index(Request $request)
    {
        return view('Concern\concern', ['title' => 'Concerns']);
    }

    public function student($id, Request $request)
    {
        $id = trim($id);
        $student = '';
        try{
            $student = new Student($id);
        } catch(Exception $e){
            //TODO Add Error Messaging for Incorrect Student ID#
            return redirect('/concerns');
        }
        $reports = (new Report())->getAllListsForStudentForm();
        return view('concern\studentForm', ['title' => 'Concerns-'.$id,
                                            'student' => $student,
                                            'reports' => $reports]);
    }

    public function success(Request $request)
    {
        return redirect('/concerns')->with('message', 'Form has been Submitted Successfully');
    }

    public function getReport($id)
    {
        $report = new Report($id);
        if($report->fee == 1){
            return 'fee';
        } else if($report->dates == 1){
            return 'dates';
        } else {
            return 'none';
        }
    }

    public function save($id, Request $request)
    {
        $student = new Student($id);
        $reason = new Report($request->input('reason'));
        $description = $request->input('description');
        if($reason->fee == 1){
            $data = [
                'description' => $description,
                'fee' => $request->input('fee'),
            ];
        } else if($reason->dates == 1){
            $startDate = new DateTime($request->input('startDate'));
            $endDate = new DateTime($request->input('endDate'));
            $data = [
                'description' => $description,
                'startDate' => $request->input('startDate'),
                'endDate' => $request->input('endDate'),
            ];
        } else {
            $data = [
                'description' => $description,
            ];
        }
        $user = $request->session()->get('user');
        $form = new Form();
        $success = $form->createForm($student, $user, $reason, $data);
        if($success){
            return 'true';
        } else {
            return json_encode(['error' => 'Unable to Submit Form']);
        }
    }

}

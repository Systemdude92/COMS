<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lists\Report;
use App\Lists\FormStatus;

class ListController extends Controller
{

    public function index(string $id, Request $request)
    {
        $user = $request->session()->get('user');
        $readLists = $user->readLists;
        $writeLists = $user->writeLists;
        $availableLists = array_merge($readLists, $writeLists);
        $availableLists = array_unique($availableLists);
        $report = new Report($id);
        $data = [
            'title' => 'Lists',
            'report' => $report,
            'statuses' => (new FormStatus())->getStatusesForReport($report->id),
        ];

        if(!in_array($id, $availableLists) && !in_array($id, $report::COLLECTIVE_LISTS)){
            return view('List/error', ['title' => 'Unauthorized',
                                        'report' => $report->name]);
        }
        if(in_array($id, $report::COLLECTIVE_LISTS)){
            $reports = (new Report())->getReports($availableLists);
            $data['reports'] = $reports;

        }
        return view('List/list', $data);
    }

}

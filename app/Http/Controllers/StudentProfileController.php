<?php
/*
|-------------------------------------------------------------------------------
|   app\Http\Controllers\StudentProfileController.php
|-------------------------------------------------------------------------------
|         Author:   Velma Vessels
|           Date:   11/10/2017
|    Description:   Provided the data from the student search, pass the data to 
|                   resources\views\student\profile.blade.php
|-------------------------------------------------------------------------------
|  Modifications:   
|
|
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App;
use App\User;
use App\Settings;
use App\Student;
use App\FeesAndPayments;
use Exception;

class StudentProfileController  extends Controller
{
    public $applyThePayment = [];

            /************************************/
            /***  collect the student data    ***/
            /************************************/
    public function studentProfile(string $id, Request $request)
    {

        $student = new Student($id);                            //  create instance
        $feesAndPayments = new FeesAndPayments($id, $student->schoolYear);          //  create instance

        $classSchedule   = $student->getSchedules();            //  get the student's schedule of classes
        $completedIssues = $student->getHistoryIssues();        //  resolved/completed issues
        $currentIssues   = $student->getCurrentIssues();        //  unresolved/current issues
        $getPicture      = $student->getPicture();              //  get the student's picture

        $completedCount = count($completedIssues);
        $currentCount = count($currentIssues);
        $totalCount = $completedCount + $currentCount;

            /***  display the data to the screen  ***/
        return view('student/profile',['title' => 'Profile', 
                    'student'        =>$student,
                    'classSchedule'  =>$classSchedule,
                    'completedIssues'=>$completedIssues,
                    'currentIssues'  =>$currentIssues,
                    'feesAndPayments'=>$feesAndPayments,
                    'completedCount' =>$completedCount,
                    'currentCount' =>$currentCount,
                    'totalCount' =>$totalCount
                    ]);                    

    }    // end function studentProfile(int $id, Request $request)

    
    //   use the following to make payments   --  addNotes  
    public function addPayment(Request $request)
    {
        $applyThePayment = [];

$studentID = '12014593';   // testing only
        // $studentID = $request->input('studentID');
        self::applyPayment($studentID, $request);
        return json_encode($this->applythePayment);
    }

    private function applyPayment($studentID, Request $request)
    {

//        $note = filter_var(trim($request->input('note')), FILTER_SANITIZE_STRING);

        $data = [
            'studentID'=>$studentID
        ];

 //       $isSuccess = $feesAndPayments->addPayment($note);
 //       array_push($this->applyThePayment, $isSuccess);
 //       if($data['status'] != $form->status->id && !empty($data['status'])){
 //           $form->setStatus($data['status']);
 //       }

        $feesAndPayments->payment = $request->input('payment', 0);


$studentID = '12014593';   // testing only        
$feesAndPayments->payment = '.10';



        if($payment != 0)
        {
            $feesAndPayments->paymentType = $request->input('paymentType', 0);
$feesAndPayments->paymentTypeID = '1';

            if($paymentType > 0)
            {
                $paymentMade = (new FeesAndPayments)->makePayment($studentID, $student->schoolYear, $payment, $paymentType);
                $paymentStatus = [];

                if($paymentMade)
                {
                    $paymentStatus = 
                    [
                        'success' => 'true',
                        'message' => 'Payment was successfully recorded for: '.$student->firstName.' '.$student->lastName
                    ];
                }
                else 
                {
                    $paymentStatus = 
                    [
                        'success' => 'false',
                        'message' => 'Payment failed to recorded for: '.$student->firstName.' '.$student->lastName
                    ];
                }
                array_push($this->$paymentStatus);
            }
        }
        else
        {
            $paymentStatus = [
            'message' => ' *No Payment has been made.* '];
        }
    }    //  end of private function processPayment(Request $request)



    public function getPaymentTypes(Request $request)
    {
        return (new FeesAndPayments())->getPaymentTypes();
    }

    public function getAmount( Request $request)
    {
         $feeAmount = '.10';       // testing
        return feeAmount;
    }   //end function getTheAmount(Request $request)

}   // end class StudentProfileController

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Campus;
use App\Kiosks\DetentionKiosk;
use App\Lists\Report;
use Session;

class SiteAdminController extends Controller
{
    public function index()
    {
        $orgCode = Session::get('user')->orgCode;
        $campus = new Campus($orgCode);
        $detKiosk = new DetentionKiosk(); // Detention Kiosk Object Variables
        $reportInfo = (new Report())->getAllLists();
        return view('Admin.siteadmin', ['title' => "Site Admin",
                                        'campus' => $campus,
                                        'detKiosk' => $detKiosk,
                                        'reports' => $reportInfo,
                                        'reportObject' => (new Report())]);
    }

    public function setKioskSettings(Request $request)
    {

        //Alias Lists
        $lists = $request->input('lists');
        foreach($lists as $list){
            self::updateList($list);
        }

        //Detention Kiosk Section
        $detKiosk = new DetentionKiosk();
        $detentionKioskStatus = $request->input('detentionKiosk');
        $detentionKioskSettings = $request->input('detention');
        $errors = [];
        if($detentionKioskStatus){
            array_push($errors, $detKiosk->updateKiosk($detentionKioskSettings));
        } else {
            array_push($errors, $detKiosk->deactiveKiosk());
        }

        if(!empty($errors[0])){
            return response()->json($errors, 400);
        } else {
            return response('', 200);
        }
    }

    private function updateList(array $list)
    {
        $report = new Report();
        $listsFromDB = $report->getAllLists();
        foreach($listsFromDB AS $dbList){
            if($dbList->id == $list['id']){
                if($dbList->name != $list['aliasName']){
                    $report->updateAlias($list['id'], $list['aliasName']);
                }
            }
        }
    }

}

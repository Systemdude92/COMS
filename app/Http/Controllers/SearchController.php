<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Search;
use App\Student;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        if($request->session()->has('user')){
            $key = $request->query('key');
            $orgCode = $request->session()->get('user')->orgCode;
            $search = new Search();
            $students = $search->studentSearch($orgCode, $key);
            $returnedStudent = [];
            foreach($students as $student){
                $stu = new Student($student->studentID);
                array_push($returnedStudent, $stu);
            }
            return view('search/search', ['title'=> 'Search',
                                            'key' => $key,
                                            'students' => $returnedStudent]);
        }
    }

}

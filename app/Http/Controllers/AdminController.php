<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Session;
use Exception;
use App\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lists\Report;

class AdminController extends Controller
{

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $reports = (new Report)->getAllListsForStudentForm();
        $otherUsers = (new Settings)->getOtherUsers();
        $returnOtherUsers = [];
        foreach($otherUsers as $user){
            if($user->comsRole != Session::get('user')::USER_SITE_ADMIN){
                array_push($returnOtherUsers, $user);
            }
        }
        $emptyReportObject = new Report();
        for($i=0;$i<count($otherUsers);$i++){
            if($i % 2 == 1){
                $otherUsers[$i]->mod = 'oddRow';
            } else {
                $otherUsers[$i]->mod = 'evenRow';
            }
        }
        return view('Admin\admin', ['title' => 'Admin Console',
                                    'reports' => $reports,
                                    'emptyReportObject' => $emptyReportObject,
                                    'reportCount' => count($reports),
                                    'otherUsers' => $otherUsers]);
    }

    /**
     * Returns User Information for Admin Console
     * Based on passed Username
     *
     * @param Request $request
     * @param string $username
     * @return array
     */
    public function getUserInfo(Request $request, string $username)
    {
        $requestedUser = new User($username);
        $report = new Report();
        $allReports = $report->getAllLists();
        $userAttr = [
            'displayName' => $requestedUser->displayName,
            'readLists' => $requestedUser->readLists,
            'writeLists' => $requestedUser->writeLists,
            'comsRoles' => $requestedUser->getComsRoles(),
            'reports' => $allReports,
            'collectiveLists' => $report::COLLECTIVE_LISTS,
        ];
        return $userAttr;
    }

    /**
     * Sets user's Read and Write Lists and Update's COMS Role if needed
     *
     * @param Request $request
     * @param string $username
     * @return bool
     */
    public function setUserInfo(Request $request, string $username)
    {
        $readLists = filter_var($request->input('readLists'), FILTER_SANITIZE_STRING);
        $writeLists = filter_var($request->input('writeLists'), FILTER_SANITIZE_STRING);
        $comsRole = filter_var($request->input('comsRole'), FILTER_SANITIZE_NUMBER_INT);
        $userData = [
            'username' => $username,
            'readLists' => $readLists,
            'writeLists' => $writeLists,
            'comsRole' => $comsRole
        ];
        $isSuccessful = $request->session()->get('user')->setUserPermissions($userData);
        if($isSuccessful){
            return 'true';
        } else {
            return 'false';
        }
    }

}

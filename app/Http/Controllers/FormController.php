<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lists\Form;
use App\Lists\Report;
use App\Lists\FormStatus;
use App\FeesAndPayments;
use Exception;

class FormController extends Controller
{

    public $noteStatus = [];

    /**
     * @param string $id Report ID
     * @param Request $request PSR-7 HTTP Request Object
     * @return array Filtered Forms for Report
     */
    public function getForms(string $id, Request $request)
    {
        $report = new Report($id);
        $forms = $report->filteredForms;
        $user = $request->session()->get('user');
        $readLists = $user->readLists;
        $readLists = $request->session()->get('user')->readLists;
        $writeLists = $request->session()->get('user')->writeLists;
        $availableLists = array_merge($readLists, $writeLists);
        $availableLists = array_unique($availableLists);
        $returnedForms = [];
        foreach ($forms as $form) {
            $reportInfo = ['id' => $form->reportID,
                            'name' => $form->reportName,
                            // 'fee' => in_array($form->id, $report::FEE_LISTS)? true : false
                        ];
            $studentInfo = ['studentID' => $form->studentID,
                            'studentLastName' => $form->studentLastName,
                            'studentFirstName' => $form->studentFirstName];
            $array = [
                'DT_RowId' => $form->id,
                'empty' => "",
                'studentID' => $form->studentID,
                'studentInfo' => $studentInfo,
                'reportInfo' =>  $reportInfo,
                'submittedUser' => $form->submittedUserLastName.', '.$form->submittedUserFirstName,
                'status' => $form->status,
                'message' => $form->message,
                'notes' => $form->getFormatedNotes(),
                'writeAccess' => $form->writable(),
                'submittedDate' => $form->submittedDate->format('F d, Y')
            ];
            if (in_array($id, $report::FEE_LISTS)) {
                $array['amountOwed'] = number_format($form->feeAmount, 2, '.', ',');
            } elseif (in_array($id, $report::DATES_LISTS)) {
                $array['startDate'] = $form->startDate->format('F d, Y');
                $array['endDate'] = $form->endDate->format('F d, Y');
            } else {
                $array['submittedDate'] = $form->submittedDate->format('F d, Y');
            }
            if(in_array($form->reportID, $availableLists)){
                array_push($returnedForms, $array);
            }
        }
        return json_encode($returnedForms);
    }

    public function getForm(string $id, Request $request)
    {
        if(strpos($id, 'For') > -1){
            return '';
        }
        $form = new Form($id);
        $reportType = (new Report($form->reportID))->getReportType();
        $formArray = [
            'id' => $form->id,
            'reportID' => $form->reportID,
            'reportType' => $reportType,
            'submittedUser' => $form->submittedUserLastName.', '.$form->submittedUserFirstName
        ];
        return $formArray;
    }

    public function addNotes(Request $request)
    {
        $formID = $request->input('formID');

        $noteStatus = [];
        if(is_array($formID)){
            foreach($formID as $id){
                self::processNote($id, $request);
            }
        } else {
           self::processNote($formID, $request);
        }
        return json_encode($this->noteStatus);
    }

    private function processNote($formID, Request $request)
    {
        $note = filter_var(trim($request->input('note')), FILTER_SANITIZE_STRING);
        $status = $request->input('status', null);
        $status = isset($status)? filter_var($status, FILTER_SANITIZE_NUMBER_INT) : null;
        $data = [
            'note' => $note,
            'status' => $status
        ];
        $formID = filter_var($formID, FILTER_SANITIZE_NUMBER_INT);
        $form = new Form($formID);
        $report = new Report();
        $isSuccess = $form->addNote($note);
        array_push($this->noteStatus, $isSuccess);
        if($data['status'] != $form->status->id && !empty($data['status'])){
            $form->setStatus($data['status']);
        }
        if(in_array($form->reportID, $report::FEE_LISTS)){
            $payment = $request->input('payment', 0);
            if($payment != 0){
                /* if($payment == 'satisfied'){
                    $payment = $form->feeAmount;
                } */
                $paymentType = $request->input('paymentType', 0);
                if($paymentType > 0){
                    $paymentMade = (new FeesAndPayments)->makePayment($form->studentID, $form->schoolYear, $payment, $paymentType);
                    if($paymentMade){
                        $paymentStatus = [
                            'success' => 'true',
                            'message' => 'Payment was successfully recorded for: '.$form->studentFirstName.' '.$form->studentLastName
                        ];
                    } else {
                        $paymentStatus = [
                            'success' => 'false',
                            'message' => 'Payment failed to recorded for: '.$form->studentFirstName.' '.$form->studentLastName
                        ];
                    }
                    array_push($this->noteStatus, $paymentStatus);
                }
            }
        }
    }

    public function getStatuses(string $id, Request $request)
    {
        $form = new Form($id);
        $statuses = (new FormStatus())->getStatusesForReport($form->reportID);
        $returnValue = [];
        foreach($statuses as $status){
            $array = [
                'id' => $status->id,
                'name' => $status->name,
            ];
            if($form->status->id == $status->id){
                $array['active'] = true;
            }
            array_push($returnValue, $array);
        }
        return $returnValue;
    }

    public function getPaymentTypes(Request $request)
    {
        return (new FeesAndPayments())->getPaymentTypes();
    }

    public function getAmount(string $formID, Request $request)
    {
        $form = new Form($formID);
        $report = new Report();
        if(in_array($form->reportID, $report::FEE_LISTS)){
            return $form->feeAmount;
        } else {
            return '';
        }
    }

}

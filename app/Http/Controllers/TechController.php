<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Campus;

class TechController extends Controller
{
    public function campuses(Request $request)
    {
        $campus = new Campus();
        $campuses = $campus->getCampuses();
        return json_encode($campuses);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return string
     */
    public function save(Request $request)
    {
        if($request->isMethod('post')){
            $request->validate(
                [
                    'orgCode' => 'required'
                ]
            );
            $newOrgCode = $request->input('orgCode');
            $currentOrgCode = $request->session()->get('user')->getOrgCode();
            if($newOrgCode != $currentOrgCode){
                $success = $request->session()->get('user')->setOrgCode($newOrgCode);
            } else {
                $success = 'false';
            }
            if($success){
                $success = 'true';
            } else {
                $success = 'false';
            }
            return $success;
        }
    }

}

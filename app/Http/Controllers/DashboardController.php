<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lists\Report;
use App\DatabaseConnection\SkywardProdDB;

class DashboardController extends Controller
{
    const DESC_LENGTH = 30;

    public function home(Request $request)
    {
        $collective = self::lists('collective', $request);
        $individual = self::lists('individual', $request);

        $collectLength = count($collective);
        $individualLength = count($individual);

        for($i=0;$i<$collectLength;$i++){
            if(strlen($collective[$i]['description']) > self::DESC_LENGTH){
                $collective[$i]['displayedDescription'] = substr($collective[$i]['description'], 0, 27) . "...";
            } else {
                $collective[$i]['displayedDescription'] = $collective[$i]['description'];
            }
        }

        for($i=0;$i<$individualLength;$i++){
            if(strlen($individual[$i]['description']) > self::DESC_LENGTH){
                $individual[$i]['displayedDescription'] = substr($individual[$i]['description'], 0, 27) . "...";
            } else {
                $individual[$i]['displayedDescription'] = $individual[$i]['description'];
            }
        }

        /* foreach($collective as $collect){
            if(count($collect['description']) > self::DESC_LENGTH){
                $collect['displayedDescription'] = substr($collect['description'], 0, 27) . "...";
            } else {
                $collect['displayedDescription'] = $collect['description'];
            }
        }

        foreach($individual as $collect){
            if(count($collect['description']) > self::DESC_LENGTH){
                $collect['displayedDescription'] = substr($collect['description'], 0, 27) . "...";
            } else {
                $collect['displayedDescription'] = $collect['description'];
            }
        } */

        return view('Dashboard/dashboard', ['title' => 'Dashboard',
                                            'collective' => $collective,
                                            'individual' => $individual]);
    }

    public function toDoList($type, Request $request)
    {
        $lists = self::lists($type, $request);
        return response($lists, 200)
                        ->header('Content-Type', 'application/json; charset=utf-8');
    }

    private function lists(string $type, Request $request)
    {
        $lists = [];
        if($type == 'collective'){
            $report = new Report();
            foreach($report::COLLECTIVE_LISTS as $listID){
                $list = new Report($listID);
                array_push($lists, $list->toArray());
            }
        } else if($type == 'individual'){
            $readLists = $request->session()->get('user')->readLists;
            $writeLists = $request->session()->get('user')->writeLists;
            $availableLists = array_merge($readLists, $writeLists);
            $availableLists = array_unique($availableLists);
            asort($availableLists, SORT_NUMERIC);
            foreach($availableLists as $listID){
                if(!empty($listID)){
                    $list = new Report($listID);
                    if(!in_array($list->id, $list::COLLECTIVE_LISTS)){
                        if($list->filteredTotal > 0){
                            array_push($lists, $list->toArray());
                        }
                    }
                }
            }
        }
        return $lists;
    }

}

<?php

/**
 * Controller   : BugReportingController.php
 * Created By   : McCourry, W. Logan
 * Created On   : 10/19/2017
 * Description  : Handles functionality for the Bug Reporting Icon on the Header Bar
 * 
 * NOTE :   Unable to test Bug Reporting Issues ON Local machine.
 *          Must be done on an external server.
 *          Local machines are unable to connect to Soap object Successfully
 * 
 */

namespace App\Http\Controllers\Layout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use SoapClient;

class BugReportingController extends Controller
{

    const ASSET_NUMBER = 'Custom Programming';
    const CLASSIFICATION = '1662';

    public function index(Request $request)
    {
        //TODO Submit Ticket to LiveTime using Genaric Login
        $user = $request->session()->get('user');
        $subject = $request->input('subject');
        $description = $request->input('description');
        $username = $user->getUsername();
        $password = Crypt::decrypt($user->getPassword());
        $soap = new SoapClient( __DIR__ . "/../../../../wsdl/Authenticate.wsdl", ['trace' => true]);
        $results = $soap->connect(1, $username.'@lisd.net', $password);
        $success = self::getAttr('success', $results);
        if($success != 'false'){
            $soap->SoapClient( __DIR__ . "/../../../../wsdl/Request.wsdl", ['trace' => true]);
            $results = $soap->createServiceRequest(self::ASSET_NUMBER, self::CLASSIFICATION, $subject, $description);
            $success = self::getAttr('success', $results);
            if($success == 'false'){
                return "Unable to Process Request";
            } else {
                return self::getAttr('message', $results);
            }
        } else {
            return "Unable to Connect To Live at this Time. Please try again";
        }
    }

    private function getAttr($attr, $res)
	{
		$fin = 'No attribute ' . $attr . ' found.';
		$done = false;
		for($i = 0; !$done && $i < count($res); $i++)
		{
			if($res[$i]->key == $attr)
			{
				$fin = $res[$i]->value;
				$done = true;
			}
		}
		return $fin;
	}

}

<?php

namespace App\Http\Controllers\Layout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Search;
use App\Student;

class SearchController extends Controller
{
    public function studentSearch(Request $request)
    {
        $orgCode = $request->session()->get('user')->getOrgCode();
        $key = filter_var($request->key, FILTER_SANITIZE_STRING);
        $students = (new Search())->studentSearch($orgCode, $key);
        $returningStudents = [];
        foreach($students as $student){
            $stu = new Student($student->studentID);
            $array = [
                'studentID' => $stu->studentID,
                'displayName' => $stu->displayName,
                'campusName' => $stu->campusName,
                'dob' => date('F d, Y', strtotime($stu->dob)),
                'age' => $stu->age,
                'grade' => $stu->grade,
                'gender' => $stu->gender
            ];
            array_push($returningStudents, $array);
        }
        return json_encode($returningStudents);
    }
}

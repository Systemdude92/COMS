<?php

/*
 *  File: SiteNavController.php
 *  Author: W. Logan McCourry
 *  Created On : Oct 04 2017
 *  Description : Controller to handle Site Navication Icon
 */

 namespace App\Http\Controllers\Layout;

 use Illuminate\Http\Request;
 use App\Http\Controllers\Controller;
 use Illuminate\Support\Facades\DB;
 use App\User;

 class SiteNavController extends Controller
 {
    public function index(Request $request)
    {
        $user = $request->session()->get('user');
        $siteNavIcons = $user->getSiteNav();
        $siteNavIcons = json_encode($siteNavIcons);
        return $siteNavIcons;
    }

    public function redirect($id)
    {
        if($id == 'siteDashboard'){
            return redirect("/dashboard");
        } else if($id =='studentForm'){
            return redirect(route('concerns'));
        } else if($id == 'analytics'){
            return redirect('/analytics');
        }
    }
  /**** added this menu fxn for Admins VV ****/
  public function redirectAdmins($id)
  {
      if($id == 'siteDashboard'){
          return redirect("/dashboard");        // directed back to Site Dashboard
      } else if('studentForm'){
          return redirect("/concern");          // redirected to Student Concerns form
      } else if('analytics'){
          return redirect('/analytics');        // redirected to Analytics Dashboard form        
      } else if ('admin'){
          return redirect('/assignRoles');      // redirected to Site Admin form
      }
  }  // end function redirectAdmins($id)


    /**** added this menu fxn for siteAdmins  VV ****/
    public function redirectSiteAdmins($id)
    {
        if($id == 'siteDashboard'){
            return redirect("/dashboard");      // directed back to Site Dashboard
        } else if('studentForm'){               
            return redirect("/concern");        // redirected to Student Concerns form
        } else if('analytics'){
            return redirect('/analytics');      // redirected to Analytics Dashboard form
        } else if ('admin'){
            return redirect('/assignRoles');    // redirected to Admin (Assign Roles) form
        } else if ('siteAdmin'){
            return redirect ('/siteAdmin');     // redirected to Site Admin form
        }
    }  // end function redirectSiteAdmins($id)


    
    private function getFilterNavIconForUser(User $user)
    {

        if($user->getComsRole() == 0){
            return [];
        } else {
            $siteNav = [];
            $coms = DB::connection('ComsDB');
            $siteNavIcons = $coms->table('siteNavIcons')
                ->select('name', 'description', 'shortName', 'color', 'icon', 'production', 'priority')
                ->where('production', '=', 'Yes')
                ->orderBy('priority', 'asc')
                ->get();
            if($user->isSiteAdmin()){
                $siteNav = $siteNavIcons;
                            } else if ($user->isAdmin()){
                foreach($siteNavIcons as $icons){
                    if($icons->shortName != 'siteAdmin')
                    {
                        array_push($siteNav, $icons);
                    }
                }
            } else {
                foreach($siteNavIcons as $icons){
                    if($icons->shortName != 'siteAdmin' && $icons->shortName != 'admin'){
                        array_push($siteNav, $icons);
                    }
                }
            }
            return $siteNav;
        }
    }

 }

<?php

/**
 * File Name: SettingsController.php
 * Created By: W. Logan McCourry
 * Created On: October 9, 2017
 * Created Time: 12:30p.m.
 * Description: Laravel 5 Controller for Settings Configuration
*/

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Settings;
use Exception;

class SettingsController extends Controller
{

    public function save(Request $request)
    {
        $input = $request->all();
        $user = $request->session()->get('user');
        $isSuccessful = $user->setActiveSetting($input);
        return $isSuccessful;
    }

    /**
     * Function: getSettings()
     * @param string $setting Setting name
     * @param Request $request data
     * @return Object
     * Description: grabs the proper information for the setting that is passed in
     */
    public function getSettings(string $setting, Request $request){
        $settingsObject = new Settings();
        $setting = strtolower($setting);
        $user = $request->session()->get('user');
        $settingsData = [];
        if(in_array($setting, $settingsObject::ALL_STUDENT_SETTINGS)){
            return null;
        } else if(in_array($setting, $settingsObject::MULTI_VALUE_SETTINGS)){
            $settingsData = self::getMultiValueSettingData($setting, $user);
        } else if(in_array($setting, $settingsObject::DROP_DOWN_SETTINGS)){
            $settingsData = self::getDropDownOption($setting, $user);
        } else {
            throw new Exception("Incorrect Setting Type was passed");
        }
        return response()->json($settingsData);
    }

    private function getMultiValueSettingData(string $type, User $user)
    {
        $settingsData = [
            'id' => 'n/a',
            'value1' => '',
            'value2' => '',
        ];
        if($type == 'alpha'){
            if($user->hasSetting('Alpha')){
                $setting = $user->getSetting('Alpha');
                $settingsData['id'] = $setting->id;
                $settingsData['value1'] = $setting->value1;
                $settingsData['value2'] = $setting->value2;
            } else {
                $settingsData['value1'] = 'A';
                $settingsData['value2'] = 'Z';
            }
        } else if($type == 'grade'){
            if($user->hasSetting('Grade')){
                $setting = $user->getSetting('Grade');
                $settingsData['id'] = $setting->id;
                $settingsData['value1'] = $setting->value1;
                $settingsData['value2'] = $setting->value2;
            } else {
                $settingsData['value1'] = '1';
                $settingsData['value2'] = '12';
            }
        } else {
            throw new Exception("Incorrect Mutli Type Filter Setting");
        }
        return $settingsData;
    }

    private function getDropDownOption(string $type, User $user)
    {
        $settingsObject = new Settings();
        $dropDownInfo = [];
        if($type == "counselor"){
            $dropDownInfo = $settingsObject->getCounselors();
        } else if($type == 'special'){
            $dropDownInfo = $settingsObject->getSpecials();
        } else if($type == 'activity'){
            $dropDownInfo = $settingsObject->getActivities();
        } else if($type == 'other user'){
            $dropDownInfo = $settingsObject->getOtherUsers();
        } else {
            throw new Exception ('Setting value: '.$type.' Doesn\'t exist');
        }
        return $dropDownInfo;
    }

    /**
     * Function getActiveSetting()
     * @return int Active Setting ID
     * Description: retrieves Active Setting ID
    */
    public function getActiveSetting(Request $request)
    {
        //TODO Function to retrieve Active Setting
        if($request->session()->has('user')){
            $activeSetting = $request->session()->get('user')->getActiveSetting();
            return json_encode($activeSetting);

        } else {
            throw new Exception("Unable to get Active Setting from Session");
        }
    }

    /**
     * @param string $type Setting Type
     * @param Request $request PSR-7 Request Object
     * @return string
     */
    public function getCurrentValue(string $type, Request $request)
    {
        $type = strtolower($type);
        $selectedSetting = '';
        $user = $request->session()->get('user');
        if($user->hasSetting($type)){
            $selectedSetting = $user->getSetting($type);
        }
        return json_encode($selectedSetting);
    }

    /**
     * Function getCurrentValue()
     * @param string $type Setting Type
     * @param Request $request HTTP Request
     * @return array current AND/OR All Settings for
    */
    public function getCurrentValues(string $type, Request $request)
    {
        $settingsObject = new Settings();
        $data = [];
        if($type == 'allStudents'){
            return null;
        }
        if($type == 'otherUser'){
            $type = 'other user';
        }
        //TO DO GET Specific Data for Setting Type
        $user = $request->session()->get('user');
        if(in_array($type, $settingsObject::MULTI_VALUE_SETTINGS)){
            $data = self::getMultiValueSettingData($type, $user);
        } else if(in_array($type, $settingsObject::DROP_DOWN_SETTINGS)) {
            $data = self::getDropDownOption($type, $user);
        } else {
            throw new Exception ("Not Proper Setting Type");
        }
        return json_encode($data);
    }

}

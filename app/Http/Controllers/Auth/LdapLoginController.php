<?php
/*
*   Project:        COMS2
*   Filename:       LdapLoginController.php
*   Author:         Logan Mccourry
*   Date:           10/17/2017
*
*   This code runs from COMS2\app\LDAP.php
*   Description:
*
*/


namespace App\Http\Controllers\Auth;

use App\LDAP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use App\User;
use App\App\LISDException;
use Exception;
use Illuminate\Support\Collection;

class LdapLoginController extends Controller
{

    const ALLOWED_DOMAINS = ['lisd.local', 'lisd.net'];

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if($request->isMethod('post')){
            $this->validate(
                $request,
                [
                    'username' => 'required',
                    'password' => 'required',
                ]
            );

            $ldap = new LDAP();
            $authorized = false;
            try {
                if(strpos($username, '@') > 0){
                    $username = explode('@', $username);
                    if(!in_array(strtolower($username[1]), self::ALLOWED_DOMAINS)){
                        return redirect('/')->withErrors(['Incorrect Domain Specified']);
                    } else {
                        $username = $username[0];
                    }
                }
                $authorized = $ldap->ldapLogin($username, $password);
            } catch (Exception $e){
                return redirect('/')->withErrors(['Issue with connecting to Active Directory']);
            }
            if($authorized) {
                $isStaff = $ldap->checkStaff($username);
                if($isStaff){
                    $password = Crypt::encrypt($password);
                    $user = new User($username, $password);
                    $request->session()->put('user', $user);
                    if($request->session()->has('user')){
                        if($user->getComsRole() == $user::USER_NORMAL){
                            return redirect('/concerns');
                        } else {
                            return redirect(route('dashboard'));
                        }
                    } else {
                        throw new Exception("Unable to add Variable to Session");
                    }
                }
                return redirect('/')->withErrors(['Unauthorized User']);
            } else {
                return redirect('/')->withErrors(['Wrong Username AND/OR Password']);
            }
        } else {
            return redirect('/');
        }
    }
}

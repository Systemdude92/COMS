<?php

namespace App;

use App\Student;
use App\Lists\Form;
use Exception;
use Illuminate\Support\Facades\DB;

class DetentionKiosk
{
    public function addDetention(Student $student, Form $form)
    {
        //TODO Add Student to Detention Table
        return true;
    }

    public function getTotalDentions(string $studentID)
    {
        //TODO return Total Detentions Student has served
    }

    public function getRecentDententions(string $studentID)
    {
        //TODO return ALL RelativeDetentions
    }

    public function served(string $detentionID)
    {
        //TODO set Detention to served
    }

    public function excused(string $detentionID)
    {
        //TODO set Detention to excused
    }

}

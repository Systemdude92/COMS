<?php
/*
*      Project:       COMS2
*      File:          verifyRole.php
*      Date:          10/16/2017
*      Author:        Velma Vessels
*      Description:   Allows the Site Administrators to asssign the roles/accesses/permissions of their people.
*
*/

/*  Have already connected to the Active Directory to verify that the person logged in is a valid staff member.
 *  Go to the COMS2 database, go to the Users table and verify that the person logged in has privileges to give
 *  a user access.
 */

class verifyRole
{
     private $conn;             // define variable

     /*** connect to the database  ***/
     private function __construct()
    {
        $SERVER = 'tisreportingdb\tisreportingdb';
        $USER = 'ProcessUser2';
        $PASSWORD = 'ProcessUser2';
        $DB = 'COMS2_Test';     //  test database
        // $DB = 'COMS2';         production database

        $this->conn = sqlsrv_connect($SERVER, array("UID"=>$USER, "PWD"=>$PASSWORD, "Database"=>$DB));      
        if(!$this->conn)
        {
            echo "MSSQL Connection Failed";
        }  //end if(!$this->conn)

    }  // end public function __construct()



        /*Verify that the user has rights to add another user to a list*/
    public function verifyUserName($username, $isUsername = true)
    {
    	$sql =
    		"	SELECT
    			dbo.Users.username,
                dbo.Users.orgCode,
                dbo.Users.comsRole
    			FROM dbo.Users
                JOIN dbo.ComsRoles
                ON dbo.ComsRoles.id = dbo.Users.comsRole
                     WHERE dbo.Users.username = '$username'	  AND
                     dbo.Users.comsRole > 1  "  ;                        // verify that they have rights to grant privileges
    	
     	
    	$stmt = sqlsrv_query($this->conn, $sql, array($username));
    	
    	$results = array();
    	if($stmt !== false)
    	{
    		while($info = sqlsrv_fetch_array($stmt))
    		{
    			array_push($results, $info);
    		}   //end while
     	}    //end  if($stmt !== false)

        if(count($results) > 0)
        {
        return $results[0];
        }  // end  if(count($results) > 0)
        else
        {
    			throw new Exception('Unable to find Login Users info. verifyUserName. dbo.COMS2_Test.');
        }    // end else

    }    // end function verifyUserName(
    
}   // end of class verifyRole




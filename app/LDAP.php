<?php

namespace App;

use \Exception as Exception;

class LDAP
{
    private const BASE_DN = "";
    private $conn;

    public function getCampusInfo(string $username)
    {
        $results = '';
        try {
            $results = ldap_search($this->conn, self::BASE_DN, "(sAMAccountname={$username})",
                array('campus', 'orgCode'));
        } catch (Exception $e){
            return false;
        }
        if(!$results){
            return false;
        } else {
            $entries = ldap_get_entries($this->conn, $results);
            if($entries['count'] > 0){
                return ['orgCode' => $entries[0]['orgcode'][0],
                        'campus' => $entries[0]['campus'][0]
                        ];
            } else {
                return false;
            }
        }
    }

    public function getUserData(string $username)
    {
        $results = ldap_search($this->conn, self::BASE_DN, "(sAMAccountname={$username})",
                    array('campus', 'orgCode', 'givenName', 'sn', 'displayName'));
        if(!$results){
            throw new Exception("Unable to retrieve User's Data");
        } else {
            $entries = ldap_get_entries($this->conn, $results);
            if($entries['count'] > 0){
                return $entries[0];
            } else {
                throw new Exception("No Data was found for User");
            }
        }
    }

    /**
     * Description: To check whether or not user is a Staff Member
     * @param $username string User's Username for Active Directory
     * @return bool TRUE if Staff or FALSE if student or other entity
     */
    public function checkStaff(string $username)
    {
        $results = '';
        try {
            $results = ldap_search($this->conn, self::BASE_DN, "(sAMAccountname={$username})",
                array('extensionAttribute10'));
        } catch (Exception $e){
            return false;
        }
        if(!$results){
            return false;
        } else {
            $entries = ldap_get_entries($this->conn, $results);
            if($entries['count'] > 0){
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Description: To retrieve user's DN from Active Directory
     * @param $username string Username used for Active Directory authentication
     * @return string User's DN from Active Directory
     */
    public function getDN(string $username)
    {
        $result = "";
        try {
            $result = ldap_search($this->conn, self::BASE_DN, "(sAMAccountname={$username})",
                array("dn"));
        } catch (Exception $e){
            return false;
        }
        if (!$result) {
            return "";
        }
        $entries = ldap_get_entries($this->conn, $result);
        if ($entries["count"] > 0) {
            return $entries[0]["dn"];
        }

        return "";
    }

    public function isPrincipal(string $username)
    {
        $prinDN = self::getDN('All Principals'); // Get DN of All Principals Group
        $assistPrinDN = self::getDN('Assistant Principals'); // Get DN of Assistant Principals Group
        $filter = "(&(|(memberof=$prinDN)(memberof=$assistPrinDN))(sAMAccountName=$username)";
        $userDN = self::getDN($username);
        $results = ldap_search($this->conn, $userDN, $filter, ['memberOf']);
        if($results){
            $entries = ldap_get_entries($this->conn, $results);
            if($entries['count'] > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function isAssistantPrincipal(string $username)
    {
        $filter = "(&(memberof='Assistant Principals')(sAMAccountName=$username))";
        // $userDN = self::getDN($username);
        $results = ldap_search($this->conn, $userDN, $filter, ['sAMAccountName']);
        if($results){
            $entries = ldap_get_entries($this->conn, $results);
            if($entries['count'] > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Description: Authenticate User through Active Directory
     * @param $userDN string User's DN from Active Directory
     * @param $password string User's Password
     * @return bool TRUE if Authenticated FALSE if Failed to authenticate.
     */
    public function ldapLogin(string $username, string $password)
    {
        $success = false;
        $userDN = self::getDN($username);
        if (!empty($this->conn)) {
            $success = false;
            // Get full DN of user
            //$userDN = $this->getDN($username); // DON'T USE DUE TO FORCE TRUE RESULT
            try {
                $success = false;
                $ldapbind = ldap_bind($this->conn, $userDN, $password);
                if ($ldapbind) {
                    $success = true;
                    //echo "Success = True";
                } else {
                    $success = false;
                    // echo "Success = false";
                }
            } catch (Exception $e) {
                $success = false;
            }
        } else {
            $success = false;
            // throw new Exception('Unable to connect to LDAP');
        }
        return $success;
    }

    /**
     * LDAP constructor.
     * Description: Creates the connection to Active Directory
     */
    public function __construct()
    {
        //Setting Variables for the LDAP Connection
        $host = "";
        $port = 0;
        $user = "";
        $password = "";
        $this->conn = ldap_connect($host, $port);
        //or die("Ldap Connection to " . $this->host . " Failed.");

        // needed for AD search.
        // http://stackoverflow.com/questions/6222641/how-to-php-ldap-search-to-get-user-ou-if-i-dont-know-the-ou-for-base-dn
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);

        // ok, have connection? need to bind to a user to be able to read?
        if ($this->conn) {
            $ldapbind = ldap_bind($this->conn, $user, $password);
            if ($ldapbind) {

            } else {
                $this->conn = null;
            }
        }
    }

    /**
     * Description: Closes the Connection to Active Directory
     */
    public function __destruct()
    {
        ldap_close($this->conn);
    }

}

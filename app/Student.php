<?php

/**
 * Object: Student
 * Created By: McCourry, W. Logan
 * Created On: 10/19/2017
 * Description: Student Information Object
 */

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Lists\Form;
use Exception;

class Student
{

    public $studentID;
    public $firstName;
    public $lastName;
    public $middleName;
    public $displayName;
    public $username;
    public $email;
    public $dob;
    public $age;
    public $gender;
    public $orgCode;
    public $campusName;
    public $ethnicity;
    public $skywardID;
    public $counselor;
    public $esl;
    public $bil;
    public $sped;
    public $gt;
    public $avid;
    public $schoolYear;
    public $pictureFile;
    public $classSchedule = [];

    public $concernHistoryCount;
    public $concernCurrentCount;


    /**
     * Function __construct
     * @param {$id} Student ID Number
     * Description: Creates Student Object based on Student ID
     */
    public function __construct(string $id =  null)
    {


        if($id != null){
            $coms = DB::connection('ComsDB');
            $studentInfo = $coms->table('Students')
                                ->where('StudentID', '=', $id)
                                ->first();

            if(count($studentInfo) <= 0 ){
                throw new Exception("Not Valid Student ID");
            }

            $this->studentID = $studentInfo->StudentID;
            $this->firstName = $studentInfo->firstName;
            $this->lastName = $studentInfo->lastName;
            $this->displayName = strtoupper($studentInfo->lastName).", ".strtoupper($studentInfo->firstName);
            $this->middleName = isset($studentInfo->middleName) ? $studentInfo->middleName : '';
            $this->username = $studentInfo->username;
            $this->email = $studentInfo->email;
            $this->dob = $studentInfo->DOB;
            $this->age = $studentInfo->age;
            $this->gender = $studentInfo->gender;
            $this->grade = $studentInfo->grade;
            $this->orgCode = $studentInfo->orgCode;
            $this->campusName = $studentInfo->campusName;
            $this->ethnicity = $studentInfo->ethnicity;
            $this->skywardID = $studentInfo->skywardID;
            $this->counselor = $studentInfo->counselor;
            $this->esl = $studentInfo->ESL;
            $this->bil = $studentInfo->BIL;
            $this->sped = $studentInfo->SPED;
            $this->gt = $studentInfo->GT;
            $this->avid = $studentInfo->AVID;
            $this->schoolYear = $studentInfo->schoolYear;
        }
    }

    public function getCurrentSchoolYear()
    {
        $comsDB = DB::connection('ComsDB');
        $schoolYear = $comsDB->select('
            SELECT TOP 1 MAX(Students.schoolYear) AS CurrentYear
            FROM Students
        ');
        return $schoolYear[0]['CurrentYear'];
    }

    public function hasCounselor(string $counselor)
    {
        if(strtolower($this->counselor) == strtolower($counselor)){
            return true;
        } else {
            return false;
        }
    }   //end function hasCounselor(string $counselor)


    public function inSpecial(string $special)
    {
        $toLower = strtolower($special);
        if($toLower == 'sped'){
            if($this->sped == 1){
                return true;
            } else {
                return false;
            }
        } else if($toLower == 'esl'){
            if($this->esl == 1){
                return true;
            } else {
                return false;
            }
        } else if($toLower == 'bil'){
            if($this->bil == 1){
                return true;
            } else {
                return false;
            }
        } else if($toLower == 'gt'){
            if($this->gt == 1){
                return true;
            } else {
                return false;
            }
        }
    }  //end function inSpecial(string $special)


    public function inActivity(string $activity)
    {
        $toLower = strtolower($activity);
        if($toLower == 'avid'){
            if($this->avid == 1){
                return true;
            } else {
                return false;
            }
        }
    }



    public function getPicture()
    {
        $studentID      = $this->studentID;
        $pictureFile    = 'storage/'.$studentID.'.jpg';
        if (! (file_exists($pictureFile)) )
        {
            $pictureFile   = 'img/blank-profile.jpg';
        }

        $this->pictureFile =    $pictureFile;
        return $pictureFile;
    }   //end function getPicture()





            /***   get the schedule for the given student   ***/
    public function getSchedules()
    {
        $studentID = $this->studentID;
        try  // attempt
        {
            $database = false;
            $database = DB::connection('ComsDB');
            if ($database)
            {
                $studentSchedule = '';
                $studentSchedule = $database->select("SELECT StudentID,
                                        EntityID,
                                        Grade,
                                        SchoolYear,
                                        Period,
                                        room_number AS RoomNumber,
                                        StartTime,
                                        StopTime    AS EndTime,
                                        CourseDescription,
                                        teacherFirstName AS TeacherFirstName,
                                        teacherLastName AS TeacherLastName

                                FROM Schedules

                                WHERE StudentID = $studentID
                                GROUP BY StudentID,
                                        EntityID,
                                        Grade,
                                        SchoolYear,
                                        Period,
                                        room_number,
                                        StartTime,
                                        StopTime,
                                        CourseDescription,
                                        teacherFirstName,
                                        teacherLastName
                                ORDER BY Period ASC");


                if (!$studentSchedule)
                {
                    throw new Exception("Unable to retrieve Schedule");
                }   //end    if (!$studentSchedule)
                else
                {

                    /* add background color to the odd numbered class periods  *
                     * by adding a color field to the array                    */
                    $i = 0;
                    $j = 0;
                    $rowColor = '';

                    for ($i = 0; $i<(count($studentSchedule)); $i++)
                    {

                        $j = ($studentSchedule[$i]->Period % 2);		//modulus
                        if ($j == 1)   		                        //odd numbered periods
                        {
                            $rowColor  = 'oddNumberedPeriodRow';    //background color gray
                        }   // end if ($j == 1)
                        else
                        {
                            $rowColor  = 'evenNumberedPeriodRow';
                        };  // end else

                        // array_push($studentSchedule[$i], ['rowColor' => $rowColor]);
                        $studentSchedule[$i]->rowColor = $rowColor;
                        $teacherDisplayName = "";
                        $studentSchedule[$i]->TeacherName =
                            ($studentSchedule[$i]->TeacherLastName . ', '   . substr(($studentSchedule[$i]->TeacherFirstName),0, 1));

                    }  // end for
                    return $studentSchedule;
                }   //end else
            }  // end if($database)
            else                                // unable to connect to the database
            {
                throw new Exception("Unable to Connect to DB");
                error_log($e->getMessage());    // send error message to an error_log
            }
        }       // end of try
        catch (Exception $e)                    // catch the exception
        {
            throw new Exception($e->getMessage());
            error_log($e->getMessage());        // send error message to an error_log
            return false;                       // return a false to User.php
        }   // end of catch
    }   //  end function getSchedules(




            /**************************************************/
            /***   get the 'completed' Issues for the given student   ***/
            /**************************************************/
        public function getHistoryIssues()
        {
        $studentID = $this->studentID;
        // try  // attempt
        // {   
            $concernHistoryCount =  0;
            $database = false;
            $orgCode = Session::get('user')->getOrgCode();
            $database = DB::connection('ComsDB');
            if ($database)
            {
                $studentIssueHistory = "";
                $studentIssueHistory = $database->select("SELECT
                                        Forms.ID
                                        -- ISNULL(AliasReport.aliasName,DefaultReportLists.Name) AS Issue,
                                        -- Forms.submittedDate AS DateSubmitted,
                                        -- Users.firstName     AS CompletedByFirstName,
                                        -- Users.lastName      AS CompletedByLastName,
                                        -- Forms.completedDate AS dateCompleted
                                FROM Forms
                                JOIN DefaultReportLists
                                  ON Forms.reportID = DefaultReportLists.ID
                                LEFT JOIN AliasReport
                                  ON AliasReport.ReportID = DefaultReportLists.ID
                                  AND AliasReport.orgCode = $orgCode
                                JOIN Users
                                  ON Forms.completedBy = Users.id
                                WHERE StudentID =  $studentID
                                  AND completedDate IS NOT NULL
                                ORDER BY submittedDate DESC");

                if (!$studentIssueHistory)
                {
                    return $studentIssueHistory;
                }   //end    if (!$studentSchedule)
                else
                {
                    $studentHistoryForms = [];
                    for ($i = 0; $i<(count($studentIssueHistory)); $i++)
                    {
                        // $studentIssueHistory[$i]->CompletedByName =
                        //     ($studentIssueHistory[$i]->CompletedByFirstName . ' ' . $studentIssueHistory[$i]->CompletedByLastName);
                        $form = new Form($studentIssueHistory[$i]->ID);
                        $form->completedByName = $form->completedUserFirstName." ".$form->completedUserLastName;
                        array_push($studentHistoryForms, $form);
                    }  // end for

                    // return $studentIssueHistory;
                    return $studentHistoryForms;

                }   //end else
            }  // end if($database)
            else                                // unable to connect to the database
            {
                throw new Exception("Unable to Connect to DB");
                error_log($e->getMessage());    // send error message to an error_log
            }
        /* }       // end of try
        catch (Exception $e)                    // catch the exception
        {
            throw new Exception($e->getMessage());
            error_log($e->getMessage());        // send error message to an error_log
            return false;                       // return a false to User.php
        }  */  // end of catch
    }   //  end function getIssueHistory(



            /***   get the 'uncompleted' ('current') Issues for the given student   ***/
    public function getCurrentIssues()
    {
        $concernCurrentCount = 0;
        $studentID = $this->studentID;
        try  // attempt
        {
            $orgCode = Session::get('user')->getOrgCode();
            $database = false;
            $database = DB::connection('ComsDB');
            if ($database)
            {
                $studentCurrentIssues = "";
                $studentCurrentIssues = $database->select("SELECT
                                Forms.submittedDate AS DateSubmitted,
                                ISNULL(AliasReport.aliasName,DefaultReportLists.Name) AS Issue,
                                     Users.firstName AS FirstName,
                                     Users.lastName  AS LastName
                                FROM Forms
                                JOIN DefaultReportLists
                                  ON Forms.reportID = DefaultReportLists.ID
                                LEFT JOIN AliasReport
                                  ON AliasReport.ReportID = DefaultReportLists.ID
                                  AND AliasReport.orgCode = $orgCode
                                JOIN Users
                                  ON Forms.submittedUser = Users.id
                               WHERE StudentID =  $studentID
                                 AND completedDate IS NULL
                            ORDER BY submittedDate DESC");

                if (!$studentCurrentIssues)
                {
                    return $studentCurrentIssues;
                }   //end    if (!$studentSchedule)
                else
                {
                    for ($i = 0; $i<(count($studentCurrentIssues)); $i++)
                    {
                        $concernCurrentCount++;
                        $studentCurrentIssues[$i]->Name =
                                ($studentCurrentIssues[$i]->FirstName . ' ' . $studentCurrentIssues[$i]->LastName);

                    }  // end for

                    return $studentCurrentIssues;
                }   //end else
            }  // end if($database)
            else                                // unable to connect to the database
            {
                throw new Exception("Unable to Connect to DB");
                error_log($e->getMessage());    // send error message to an error_log
            }
        }       // end of try
        catch (Exception $e)                    // catch the exception
        {
                error_log($e->getMessage());        // send error message to an error_log
                return false;                       // return a false to User.php
        }   // end of catch
    }   //  end function getcurrentIssues(

}  // end class Student

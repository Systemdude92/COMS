<?php

namespace App\Kiosks;

use Session;
use DateTime;
use DB;
use App\User;

class DetentionKiosk
{

    public const DURATIONS = ['Semester', 'Quarterly']; //Types of Duration that's allowed

    private $orgCode;
    public $threshold; //int
    public $startDate; // DateTime()
    public $hoursToServe; // int (hours)
    public $duration; //string
    public $active;  //bool

    /**
     * Build the Detention Kiosk Object Instance
     *
     * @param string $orgCode
     */
    public function __construct(string $orgCode = null)
    {

        //If Org Code is null, then use Session's user's orgcode
        if(empty($orgCode)){
            $orgCode = Session::get('user')->orgCode;
        }
        $this->orgCode = $orgCode;
        // Retrieve the Kiosk Settings from DB
        $kioskSettings = self::getSettings($orgCode);
        //Saving the Variables to Object OR setting Default value if null
        $this->threshold = !empty($kioskSettings->threshold) ? $kioskSettings->threshold : 0; //Saved in int
        $this->startDate = !empty($kioskSettings->startDate) ? new DateTime($kioskSettings->startDate) : null; //Saved in DateTime Object
        $this->hoursToServe = !empty($kioskSettings->hours) ? $kioskSettings->hours : 0; //Saved in Hours
        $this->duration = !empty($kioskSettings->duration) ?  $kioskSettings->duration : null; //Saved as string

        //If Start Date is set, then Assume Active
        $this->active = !empty($this->startDate) ? true : false;

    }

    public function updateKiosk($settings)
    {
        // TODO If changes were made by Front-End
            // Change them in the Database

            //First make sure Kiosk is activated
            // self::activateKiosk();

            //Gathering User input from Site Admin
            //TODO: Change variables
            $startDate = $settings['startDate'];
            $duration = $settings['duration']; //$request->input('detention.duration');
            $threshold = $settings['threshold']; //$request->input('detention.threshold');
            $days = $settings['days']; //$request->input('detention.days');

            $errors = [];

            //Converting Days to Hours for DB
            $hours = $days * 24;

            // Validating Values for input
            $isReady = true;
            try {
                $startDate = new DateTime($startDate);
            } catch (Exception $e){
                array_push($errors, "DETENTION: Start Date is formatted incorrectly");
                $isReady = false;
            }

            $today = (new DateTime())->format('Y-m-d');
            if($today > $startDate->format('Y-m-d')){
                $isReady = false;
                array_push($errors, "DETENTION: Start Date must be today or later");
            }

            if(!in_array($duration, self::DURATIONS)){
                array_push($errors, "DETENTION: Duration is invalid");
                $isReady = false;
            }

            if($isReady){
                if($startDate != $this->startDate){
                    self::updateStartDate($startDate);
                }

                if($duration != $this->duration){
                    self::updateDuration($duration);
                }

                if($threshold != $this->threshold){
                    self::updateThreshold($threshold);
                }

                if($hours != $this->hoursToServe){
                    self::updateHoursToServe($hours);
                };
            } else {
                return $errors;
            }

            return null;

    }

    public function deactiveKiosk()
    {
        try{
        DB::connection('ComsDB')
            ->update("UPDATE mapTardyKiosk_ParticipatingSchools
                    SET StartDate = NULL
                    WHERE SchoolID = $this->orgCode");
        } catch(Exception $e){
            return ["Unable able to Deactivate Kiosk"];
        }

        return null;
    }

    private function updateStartDate(DateTime $startDate)
    {
        $startDate = $startDate->format('Y-m-d');
        DB::connection('ComsDB')
            ->update("
                UPDATE mapTardyKiosk_ParticipatingSchools
                SET StartDate = CAST('$startDate' AS DATETIME)
                WHERE SchoolID = '$this->orgCode'
            ");
    }

    private function updateDuration(string $duration)
    {
        DB::connection('ComsDB')
            ->update("
                UPDATE mapTardyKiosk_ParticipatingSchools
                SET ThresholdDuration = '$duration'
                WHERE SchoolID = '$this->orgCode'
            ");
    }

    private function updateThreshold(int $threshold)
    {
        DB::connection('ComsDB')
            ->update("
                UPDATE mapTardyKiosk_TardyThreshold
                SET TardyThreshold = $threshold
                WHERE SchoolID = '$this->orgCode'
            ");
    }

    private function updateHoursToServe(int $hours)
    {
        DB::connection('ComsDB')
            ->update("
                UPDATE mapTardyKiosk_HoursToServe
                SET HoursToServeDetention = $hours
                WHERE SchoolID = '$this->orgCode'
            ");
    }

    /**
     * Retrieves Settings for Detention Kiosk
     *
     * @param string $orgCode
     * @return PDOObject
     */
    private function getSettings(string $orgCode)
    {
        //Attempts to Retrieve Data from DB
        $settings = self::getSettingsFromDB($orgCode);

        //If Settings are empty, will add Org Code to Participating Schools
        // Then it'll retry to obtain default settings;
        // Sets Active to False at this point.
        if(empty($settings)){
            self::addSchool($orgCode);
            $settings = self::getSettingsFromDB($orgCode);
        }

        return $settings[0];
    }

    /**
     * Returns settings from Database
     *
     * @param string $orgCode
     * @return PDOObject
     */
    private static function getSettingsFromDB(string $orgCode)
    {
        return DB::connection('ComsDB')
            ->select("
            SELECT ISNULL(TardyThreshold, '6') AS threshold,
                StartDate AS startDate,
                ISNULL(HoursToServeDetention, 48) AS hours,
                ISNULL(ThresholdDuration, 'Semester') AS duration
            FROM mapTardyKiosk_ParticipatingSchools
                LEFT JOIN mapTardyKiosk_TardyThreshold  ON mapTardyKiosk_ParticipatingSchools.SchoolID = mapTardyKiosk_TardyThreshold.SchoolID
                LEFT JOIN mapTardyKiosk_HoursToServe ON mapTardyKiosk_HoursToServe.SchoolID = mapTardyKiosk_TardyThreshold.SchoolID
            WHERE mapTardyKiosk_ParticipatingSchools.SchoolID = '$orgCode'
            ");
    }

    /**
     * Adds School to the Participating Schools Table.
     * DOES NOT set any settings
     * @param string $orgCode
     *
     */
    private static function addSchool(string $orgCode)
    {
        DB::connection('ComsDB')
        ->insert("
            INSERT INTO mapTardyKiosk_ParticipatingSchools (SchoolID) VALUES ('$orgCode');
            INSERT INTO mapTardyKiosk_HoursToServe (SchoolID) VALUES ('$orgCode');
            INSERT INTO mapTardyKiosk_TardyThreshold (SchoolID) VALUES('$orgCode');
        ");
    }

}

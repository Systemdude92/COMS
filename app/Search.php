<?php
/**
 * File Name: StudentSearch.php
 * Created By: W. Logan McCourry
 * Created On: 10/16/2017
 * Description: Object To handle ALL Student Search returns
 */

 namespace App;

 use Illuminate\Support\Facades\DB;

class Search
{

    private const HIGHSCHOOLS = ['009', '014', '001', '002', '004', '013', '008', '012', '010', '015'];

    public $students = [];

    public function __construct()
    {
    }

    public function studentSearch(string $orgCode, string $key)
    {
        $students = [];
        $feeder1 = null;
        $feeder2 = null;
        $comsDB = DB::connection('ComsDB');
        $feeders = self::getFeeders($orgCode);
        $feeder1 = isset($feeders[0]) ? $feeders[0] : '';
        $feeder2 = isset($feeders[1]) ? $feeders[1] : '';
        if ($orgCode != 821) {
            $students = DB::connection('ComsDB')
                        ->table('Students')
                        ->select('StudentID as studentID', DB::raw("CONCAT(lastName, ', ', firstName) AS name"), "grade", "campusName as schoolName")
                        ->whereIn('orgCode', [$orgCode, $feeder1, $feeder2])
                        ->where(function ($query) use ($key) {
                            $query->orwhere('studentID', 'like', "%$key%")
                                  ->orwhere('lastName', 'like', "%$key%")
                                  ->orwhere('firstName', 'like', "%$key%")
                                  ->orwhere(DB::raw("CONCAT(lastName, ', ', firstName)"), 'like', "%$key%")
                                  ->orwhere(DB::raw("CONCAT(firstName,' ',lastName)"), 'like', "%$key%");
                        })
                        ->orderby('lastName')
                        ->get();
        } else {
            $students = DB::connection('ComsDB')
                        ->table('Students')
                        ->select('StudentID as studentID', DB::raw("CONCAT(lastName, ', ', firstName) AS name"), "grade", "campusName as schoolName")
                        ->where(function ($query) use ($key) {
                            $query->orwhere('studentID', 'like', "%$key%")
                                ->orwhere('lastName', 'like', "%$key%")
                                ->orwhere('firstName', 'like', "%$key%")
                                ->orwhere(DB::raw("CONCAT(lastName, ', ', firstName)"), 'like', "%$key%")
                                ->orwhere(DB::raw("CONCAT(firstName, ' ',lastName)"), 'like', "%$key%");
                        })
                        ->orderby('lastName')
                        ->get();
        }
        return $students;
    }

    private function getFeeders(string $orgCode)
    {
        $results = [];
        if (in_array($orgCode, self::HIGHSCHOOLS)) {
            if ($orgCode === '001') {
                $results = array( '014', '009');
            } elseif ($orgCode === '014') {
                $results = array('009', '001');
            } elseif ($orgCode === '009') {
                $results = array('001', '014');
            } else {
                if ($orgCode == '002') {
                    $results = array('013');
                } elseif ($orgCode == '013') {
                    $results = array('002');
                } else {
                    if ($orgCode == '004') {
                        $results = array();
                    } else {
                        if ($orgCode == '008') {
                            $results = array('012');
                        } else {
                            if ($orgCode == '012') {
                                $results = array('008');
                            } else {
                                if ($orgCode == '010') {
                                    $results = array('015');
                                } else {
                                    if ($orgCode == '015') {
                                        $results = array('010');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $results;
    }
}

<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Campus
{

    public $orgCode;
    public $campusName;
    public $address;
    public $city;
    public $state;
    public $zip;
    public $type;

    public function __construct(string $orgCode = null)
    {
        //TODO Collect Data on Campuses
        // dd($orgCode);
        if($orgCode != null){
            $campusInfo = self::getCampus($orgCode);
            $this->orgCode = $campusInfo->orgCode;
            $this->campusName = $campusInfo->campusName;
            $this->address = $campusInfo->address;
            $this->city = $campusInfo->city;
            $this->state = $campusInfo->state;
            $this->zip = $campusInfo->zipCode;
            $this->type = $campusInfo->schoolType;
        }

    }

    private static function getCampus(string $orgCode)
    {
        $campus = DB::connection('LISDDW')
                    ->table('Dim_Schools')
                    ->select('SchoolID AS orgCode',
                            'SchoolName AS campusName',
                            'SchoolType AS schoolType',
                            'SchoolAddress AS address',
                            'SchoolCity AS city',
                            'SchoolState AS state',
                            'SchoolZip AS zipCode')
                    ->where('SchoolID', '=', $orgCode)
                    ->first();
        if(empty($campus)){
            $campus = DB::connection('LISDDW')
                    ->table('Dim_Schools')
                    ->select('SchoolID AS orgCode',
                            'SchoolName AS campusName',
                            'SchoolType AS schoolType',
                            'SchoolAddress AS address',
                            'SchoolCity AS city',
                            'SchoolState AS state',
                            'SchoolZip AS zipCode')
                    ->where('SchoolID', '=', "N/A")
                    ->first();
        }
        return $campus;
    }

    public function getCampuses()
    {
        $lisddw = DB::connection('LISDDW');
        $campuses = $lisddw
                    ->table('Dim_Schools')
                    ->select('SchoolID AS orgCode',
                            'SchoolName AS campusName',
                            'SchoolType AS schoolType',
                            'SchoolAddress AS address',
                            'SchoolCity AS city',
                            'SchoolState AS state',
                            'SchoolZip AS zipCode')
                    ->where('SchoolType', '<>', 'ALT')
                    ->whereNotIn('SchoolID', ['005', '000', 'N/A'])
                    ->whereNotBetween('SchoolID', ['080', '089'])
                    ->whereNotBetween('SchoolID', ['200', '999'])
                    ->orderBy('SchoolName', 'ASC')
                    ->get();
        if(count($campuses) > 0){
            return $campuses;
        }
    }

}

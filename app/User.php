<?php

namespace App;

use App\LDAP;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Exception;
use App\Settings;
use App\Lists\Report;

class User
{
    // User Constant Variables
    const USER_ARRAY_NUMARIC = 1; // Used for toArray() function to return Num Array
    const USER_ARRAY_ASSOC = 2; // Used for toArray() function to return Assoc Array
    const USER_ARRAY_DATABASE = 3; // Used for toArray() function to return Database Array
    const USER_STATES = ['active', 'locked']; //States in which a User can be in
    const USER_SITE_ADMIN = 4; // Integer for Site Admin User Control
    const USER_ADMIN = 3; // Integer for Admin User Control
    const USER_DASHBOARD = 2; // Integer for Dashboard User Control
    const USER_NORMAL = 1; // Integer for Normal User Control

    // User's Properties
    public $id = '';
    public $firstName = '';
    public $lastName = '';
    public $username = '';
    private $password = '';
    public $displayName = '';
    public $orgCode = '';
    public $campusName = '';
    public $comsRole = '';
    public $settings = []; // Array of Settings Objects for each setting.
    public $readLists = []; // Numaric Array of Allowed Lists to Read by IDs to view
    public $writeLists = []; //Numaric Array of Allowed Lists to Write by IDS to view
    public $state = ''; // active or locked

    /**
     * Description: Creates User Object
     * User constructor.
     * @param string $username User's Username
     * @param string $password Hashed version of User's password
     */
    public function __construct(string $username, string $password = null)
    {
        if($password != null){
            $this->password = $password;
        }

        $coms = DB::connection('ComsDB');
        if(!is_int($username)){
            $inComs = $coms->select("SELECT username FROM Users WHERE username = '$username'"); // Pulling to see if Username is in the D
            // Sets information From COMS to User Object if User was found
            // Start IF
            if (isset($inComs[0]->username) && strtolower($inComs[0]->username) == strtolower($username)) {
                $userData = $coms->table('Users')
                                ->select('id', 'username', 'firstName', 'lastName', 'displayName', 'orgCode', 'campusName', 'comsRole', 'readLists', 'writeLists')
                                ->where('username', "=", $username)
                                ->first(); // Grabs the FIRST ROW from User table
                if (count($userData) > 0) {
                    // Sets the User Variables FROM COMS into User
                    $this->username = $username;
                    $this->id = $userData->id;
                    $this->firstName = $userData->firstName;
                    $this->lastName = $userData->lastName;
                    $this->displayName = $userData->displayName;
                    $this->orgCode = $userData->orgCode;
                    $this->campusName = $userData->campusName;
                    $this->comsRole = $userData->comsRole;
                    $this->readLists = explode(',', $userData->readLists);
                    $this->writeLists = explode(',', $userData->writeLists);
                    $this->settings = self::getSettings();
                    $campusChange = self::campusChange();
                } else {
                    throw new Exception("User Array is empty");
                }
            } else {
                $ldap = new LDAP();
                $userData = $ldap->getUserData($username);
                $this->firstName = $userData['givenname'][0];
                $this->lastName = $userData['sn'][0];
                $this->username = $username;
                $this->displayName = $userData['displayname'][0];
                $this->orgCode = $userData['orgcode'][0];
                $this->campusName = $userData['campus'][0];
                if($ldap->isPrincipal($username)){
                    $this->comsRole = self::USER_SITE_ADMIN;
                    self::setAllLists();
                }
                else if ($userData['orgcode'][0] == '821' || $ldap->isAssistantPrincipal($username)) {
                    $this->comsRole = self::USER_ADMIN;
                    self::setAllLists();
                } else {
                    $this->comsRole = self::USER_NORMAL; // Role set in COMS DB. Will be able to be set by Principal
                }
                self::recordUser(); // Records Information to COMS DB so that it can be used for future reference
            } // end IF
            $this->state = 'active';
        } else {
            $userInfo = $comsDB->table('User')
                                ->where('id', '=', $username);

        }

    }


    /**
     * @param array $input Setting Information
     */
    public function setActiveSetting(array $input)
    {
        $rows = DB::connection('ComsDB')->update("UPDATE UserSettings SET active = 0 WHERE userID = $this->id");
        $type = $input['type'];
        $value1 = null;
        $value2 = null;
        if ($type == 'allStudents') {
            $type = 'All Students';
        } elseif ($type == 'alpha') {
            $type = 'Alpha';
            $value1 = strtoupper($input['min']);
            $value2 = strtoupper($input['max']);
        } elseif ($type == 'grade') {
            $type = 'Grade';
            $value1 = $input['min'];
            $value2 = $input['max'];
        } elseif ($type == 'special') {
            $type = 'Special';
            $value1 = $input['selected'];
        } elseif ($type == 'activity') {
            $type = 'Activity';
            $value1 =  $input['selected'];
        } elseif ($type == 'otherUser') {
            $type = 'Other User';
            $value1 = $input['selected'];
        } elseif ($type == 'counselor') {
            $type = 'Counselor';
            $value1 = $input['selected'];
        }

        if ($type == 'Alpha' || $type == 'Grade') {
            if ($type == 'Alpha') {
                if (strcmp($value1, $value2) > 0) {
                    $temp = $value2;
                    $value2 = $value1;
                    $value1 = $temp;
                }
            } elseif ($type == 'Grade') {
                if ($value2 < $value1) {
                    $temp = $value2;
                    $value2 = $value1;
                    $value1 = $temp;
                }
            }
        }

        $sql = "
            SET NOCOUNT ON;
            DECLARE @SETTINGSID INT
            SET @SETTINGSID = (SELECT id FROM Settings WHERE Settings.name = '$type')

            IF EXISTS (SELECT settingsID
                        FROM UserSettings
                        WHERE userID = $this->id
                        AND settingsID = @SETTINGSID)
                BEGIN
                    IF @SETTINGSID = 1
                        BEGIN
                            UPDATE UserSettings
                            SET value1 = null, value2 = null, active = 1
                            FROM UserSettings
                            WHERE userID = $this->id
                            AND settingsID = @SETTINGSID
                        END
                    ELSE
                        BEGIN
                            UPDATE UserSettings
                            SET value1 = '$value1', value2 = '$value2', active = 1
                            FROM UserSettings
                            WHERE userID = $this->id
                            AND settingsID = @SETTINGSID
                        END
                END
            ELSE
                BEGIN
                    INSERT INTO UserSettings
                    VALUES (@SETTINGSID, $this->id, '$value1', '$value2', 1)
                END
        ";

        try {
            DB::connection('ComsDB')->update($sql);
        } catch (Exception $e) {
            error_log($e->getMessage());
            return 'false';
        }
        self::refreshSettings();
        return 'true';
    }

    public function refreshSettings()
    {
        $this->settings = [];
        $this->settings = self::getSettings();
    }

    public function hasSetting(string $type)
    {
        $hasSetting = false;
        foreach ($this->settings as $setting) {
            if (strtolower($setting->name) == strtolower($type)) {
                $hasSetting = true;
            }
        }
        return $hasSetting;
    }

    public function getSetting(string $type)
    {
        $settingData = '';
        foreach ($this->settings as $setting) {
            if (strtolower($setting->name) == strtolower($type)) {
                $settingData = $setting;
            }
        }
        return $settingData;
    }

    public function getActiveSetting()
    {
        $active = '';
        foreach ($this->settings as $setting) {
            if ($setting->active == 1) {
                $active = $setting;
            }
        }
        return $active;
    }

    /**
    * Function: isSiteAdmin()
    * @return bool True if Site Admin, else False
    */
    public function isSiteAdmin()
    {
        if ($this->comsRole == self::USER_SITE_ADMIN) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * Function isAdmin()
    * @return bool True if Admin, else False
    */
    public function isAdmin()
    {
        if ($this->comsRole == self::USER_ADMIN) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $typeOfArray int
     * @return array
     */
    public function toArray($arrayType = 1)
    {
        $userDataArray = [];
        if ($arrayType = self::USER_ARRAY_NUMARIC) {
            $userDataArray = [
                $this->username,
                $this->firstName,
                $this->lastName,
                $this->displayName,
                $this->orgCode,
                $this->campusName,
                $this->comsRole
            ];
        }
        if ($arrayType = self::USER_ARRAY_ASSOC) {
            $userDataArray = [
                'username' => $this->username,
                'firstName' => $this->firstName,
                'lastName' => $this->lastName,
                'displayName' => $this->displayName,
                'orgCode' => $this->orgCode,
                'campusName' => $this->campusName,
                'comsRole' => $this->comsRole
            ];
        }
        if ($arrayType = self::USER_ARRAY_DATABASE) {
            $userDataArray = [
                'username' => "'".$this->username."'",
                'firstName' => "'".$this->firstName."'",
                'lastName' => "'".$this->lastName."'",
                'displayName' => "'".$this->displayName."'",
                'orgCode' => "'".$this->orgCode."'",
                'campusName' => "'".$this->campusName."'",
                'comsRole' => "'".$this->comsRole."'",
            ];
        }
        return $userDataArray;
    }

    /**
     * @return string First Name in Upper
     */
    public function getFirstName()
    {
        return strtoupper($this->firstName);
    }

    /**
     * @return string Last Name in UPPER
     */
    public function getLastName()
    {
        return strtoupper($this->lastName);
    }

    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string Display Name in UPPER
     */
    public function getDisplayName()
    {
        return strtoupper($this->displayName);
    }

    /**
     * @return string OrgCode
     */
    public function getOrgCode()
    {
        return $this->orgCode;
    }

    /**
     * @param string $orgCode
     * @return bool success of change
     */
    public function setOrgCode(string $orgCode)
    {
        if(strtolower($this->campusName) == 'technology'){
            $this->orgCode = $orgCode;
            return true;
        } else {
            false;
        }
    }

    /**
     * @return string Campus Name in UPPER
     */
    public function getCampusName()
    {
        return strtoupper($this->campusName);
    }

    /**
    * Function getComsRole()
    * @return int User's COMS' Role
    */
    public function getComsRole()
    {
        return $this->comsRole;
    }

    /**
    * Function getState()
    * @return string User's current State
    */
    public function getState()
    {
        return $this->state;
    }

    public function getUserID()
    {
        return $this->id;
    }

    /**
    * Function setState()
    * @param string User's State
    * @throws Exception $state is not a recognized state
    */
    public function setState(string $state)
    {
        if (in_array(strtolower($state), self::USER_STATES)) {
            $this->state = $state;
        } else {
            throw new Exception ("Given State is not recognized");
        }
    }

    public function getReadableLists()
    {
        return $this->readLists;
    }

    public function getWriteableLists()
    {
        return $this->writeLists;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSiteNav()
    {
        if($this->getComsRole() == 0){
            return [];
        } else {
            $siteNav = [];
            $coms = DB::connection('ComsDB');
            $siteNavIcons = $coms->table('siteNavIcons')
                ->select('name', 'description', 'shortName', 'color', 'icon', 'production', 'priority')
                ->where('production', '=', 'Yes')
                ->orderBy('priority', 'asc')
                ->get();
            if($this->isSiteAdmin()){
                $siteNav = $siteNavIcons;
            } else if ($this->isAdmin()){
                foreach($siteNavIcons as $icons){
                    if($icons->shortName != 'siteAdmin'){
                        array_push($siteNav, $icons);
                    }
                }
            } else if($this->comsRole == self::USER_NORMAL){
                $siteNav = [];
            } else {
                foreach($siteNavIcons as $icons){
                    if($icons->shortName != 'siteAdmin' && $icons->shortName != 'admin'){
                        array_push($siteNav, $icons);
                    }
                }
            }
            return $siteNav;
        }
    }

    /**
     * Returns the name of all COMS Role with Active flag for user's current COMS Role
     *
     * @return void
     */
    public function getComsRoles()
    {
        $comsDB = DB::connection('ComsDB');
        $comsRoles = $comsDB->table('ComsRoles')
                            ->get();
        for($i=0; $i<count($comsRoles); $i++){
            if($comsRoles[$i]->id == $this->comsRole){
                $comsRoles[$i]->active = 1;
            }
        }
        $returnedComsRole = [];
        foreach($comsRoles as $role){
            if($role->id != self::USER_SITE_ADMIN){
                array_push($returnedComsRole, $role);
            }
        }
        return $returnedComsRole;
    }

    /**
     * Updates User's Permissions from Admin Console
     *
     * @param Array $userData ['username'=> 'string', 'readLists' => 'string', 'writeLists', => 'string', 'comsRole' => 'int' ]
     * @return bool of Request status
     */
    public function setUserPermissions(Array $userData)
    {
        if($this->comsRole == self::USER_SITE_ADMIN || $this->comsRole == self::USER_ADMIN){
            $report = new Report();
            foreach($report::COLLECTIVE_LISTS as $id){
                $userData['readLists'] .= ','.$id;
                $userData['writeLists'] .= ','.$id;
            }
            DB::connection('ComsDB')->table('Users')
                                    ->where('username', '=', $userData['username'])
                                    ->update([
                                        'readLists' => $userData['readLists'],
                                        'writeLists' => $userData['writeLists'],
                                        'comsRole' => $userData['comsRole']
                                    ]);
            return true;
        } else {
            throw new Exception('User does not have the appropriate permissions Set Permissions');
            return false;
        }
    }

    /**
    * Function recordUser()
    * @return bool If user was successfully able to record User information to DB
    * Description : Records User's Data to DB: Coms2, Table: User
    */
    private function recordUser()
    {
        // TODO UPDATE/INSERT User into Database for record keeping
        $comsDb = DB::connection('ComsDB');
        $userID = $comsDb->table('Users')->insertGetID([
            'lastName' => $this->lastName,
            'firstName' => $this->firstName,
            'username' => $this->username,
            'orgCode' => $this->orgCode,
            'campusName' => $this->campusName,
            'comsRole' => $this->comsRole,
            'newUser' => 1,
            'displayName' => $this->displayName,
            'readLists' => self::toString($this->readLists),
            'writeLists' => self::toString($this->writeLists),
        ]);
        $comsDb->table('UserSettings')
                ->insert([
                    'settingsID' => 1,
                    'userID' => $userID,
                    'value1' => null,
                    'value2' => null,
                    'active' => 1,
                ]);
        $this->id = $userID;
        self::refreshSettings();
    }

    /**
     * Function: toString()
     * @param array $items
     * @return string format of array
     */
    private function toString(array $items)
    {
        $string = "";
        $length = count($items);
        for ($i=0; $i<$length; $i++) {
            if ($i != ($length-1)) {
                $string .= $items[$i] . ',';
            } else {
                $string .= $items[$i];
            }
        }
        return $string;
    }

    /**
    * Sets lists value to view All Default Lists
    */
    private function setAllLists()
    {
        $coms = DB::connection('ComsDB');
        $lists = $coms->select("
            SELECT id
            FROM DefaultReportLists
        ");
        $listsArray = [];
        foreach ($lists as $list) {
            array_push($listsArray, $list->id);
        }
        $this->readLists = $listsArray;
        $this->writeLists = $listsArray;
    }

    private function getSettings()
    {
        $comsDB = DB::connection('ComsDB');
        $settingsID = $comsDB->table('UserSettings')
                           ->select('id')
                           ->where('userID', '=', $this->id)
                           ->get();
        $userSettings = [];
        if (count($settingsID) > 0) {
            foreach ($settingsID as $setting) {
                $userSetting = new Settings($setting->id);
                array_push($userSettings, $userSetting);
            }
        }
        return $userSettings;
    }

    private function campusChange()
    {
        $username = $this->username;
        $ldap = new LDAP();
        $campusInfo = $ldap->getCampusInfo($username);
        if($campusInfo['orgCode'] != $this->orgCode){
            $comsDB = DB::connection('ComsDB');
            $newOrgCode = $campusInfo['orgCode'];
            $newCampus = $campusInfo['campus'];
            $id = $this->id;
            $success = $comsDB->update(
                    "UPDATE Users
                    SET orgCode = '$newOrgCode', campusName = '$newCampus'
                    WHERE id = '$id'"
                );
            if(!$success){
                throw new Exception("Unable to update Campus Information for ".$this->displayName." in updateCampusInfo()");
            } else {
                $this->orgCode = $newOrgCode;
                $this->campusName = $newCampus;
            }
        }
    }



}

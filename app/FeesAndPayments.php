<?php
/************************************************************************************/
/* Object: FeesAndPayments                                                          */
/* Created By: Velma Vessels                                                        */
/* Created On: 11/30/2017                                                           */
/* Description: Student Fees/Fines and Payments for a given school year.            */
/************************************************************************************/
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Exception;

class FeesAndPayments
{
    public $balance = 0;
    public $fees = [];
    public $payments = [];
    public $feesAndPayments = [];
    public $feeAmount = 0;
    public $paymentTypeID = 1;
    public $paymentType;
    public $database;


    /**
     * @param string $studentID Student ID
     * @param int $schoolYear Current School Year
     */
    public function __construct(string $studentID = null, int $schoolYear = null)
    {
        $rowColor = '';
        if($studentID != null){

            $balance = 0;

                $database = false;
                $database = DB::connection('ComsDB');

                    /*** get the fees and fines  ***/
                if($database){
                $feesAndPayments =
                    $database->select("SELECT 'F' AS feeOrPayment,
                                    Forms.submittedDate AS theDate,
                                    Forms.feeAmount     AS amount,
                                    DefaultReportLists.Name  AS type,
                                    Users.displayName   AS enteredBy

                                FROM  Forms

                         LEFT JOIN  Users
                                ON  Forms.submittedUser = Users.id
                         LEFT JOIN  DefaultReportLists
                                ON  DefaultReportLists.ID = Forms.reportID
                             WHERE  Forms.StudentID     = $studentID
                               AND  Forms.schoolyear    = $schoolYear
                               AND  Forms.feeAmount     <> 0
                    UNION

                        SELECT 'P' AS feeOrPayment,
                                        Payments.paymentDate   AS theDate,
                                        Payments.paymentAmount AS amount,
                                        PaymentType.name       AS type,
                                        Users.displayName      AS enteredBy
                                  FROM  Payments
                             LEFT JOIN  PaymentType
                                    ON  PaymentType.id = Payments.paymentTypeID
                                  JOIN  Users
                                    ON  Users.id  = Payments.userID
                                 WHERE  Payments.StudentID   = $studentID
                                   AND  payments.schoolyear  = $schoolYear
                                   AND  payments.paymentAmount <> 0
                              ORDER BY theDate DESC");


                /* set the array into an object */
                // $this->feesAndPayments = $feesAndPayments;
                $balance = 0;

                    /* calculate the balance, format dollar amounts and set the background row color */
                if ($feesAndPayments)
                {
                    $rowColor = '';
                        for ($i = 0; $i<(count($feesAndPayments)); $i++)
                        {
                            if ($feesAndPayments[$i]->feeOrPayment == 'F')
                            {
                                $rowColor  = 'evenNumberedPeriodRow';
                                $balance   = $balance + $feesAndPayments[$i]->amount;      // fees and fines
                            }
                            else
                            {
                                $rowColor  = 'oddNumberedPeriodRow';    //background color gray
                                $balance   = $balance - ($feesAndPayments[$i]->amount);      // payments
                            }

                                    /* Payments have blue background rowColor */
                            $feesAndPayments[$i]->rowColor = $rowColor;

                                    /* format each currency amount */
                            $feesAndPayments[$i]->amount =
                                number_format($feesAndPayments[$i]->amount,2,'.',',');

                    }   // end  if ($feesAndPayments)
                    $this->feesAndPayments = $feesAndPayments;


                    /* set the array into an object */
                    $this->feesAndPayments = $feesAndPayments;

                            /* format the balance amount */
                    if ($balance <> 0)
                    {
                        $this->balance =
                            number_format($balance,2,'.',',');
                    }
                }  // end if(

            } // end of   if database
        }  // end of if($studentID != null)
    }   // end of construct


    public function getPaymentTypes()
    {
        return DB::connection('ComsDB')
                ->table('PaymentType')
                ->get();
    }


    /**
     * Makes Payment for Student on Form page
     *
     * @param int $formID
     * @param int $studentID
     * @param int $schoolYear
     * @param float $amount
     * @param int $paymentTypeID
     * @return bool
     */
    public function makePayment(int $studentID, int $schoolYear, float $amount, int $paymentTypeID)
    {
        $schoolYear = (new Student($studentID))->schoolyear;
        $comsDB = DB::connection('ComsDB');
        $success = $comsDB->table('Payments')
                            ->insertGetId(
                                [
                                    'studentID' => $studentID,
                                    'schoolYear' => $schoolYear,
                                    'paymentDate' => DB::raw('GETDATE()'),
                                    'paymentAmount' => $amount,
                                    'paymentTypeID' => $paymentTypeID,
                                    'userID' => Session::get('user')->id
                                ]
                            );
        if(!$success){
            return false;
        } else {
            return true;
        }
    }

    //     // write payment record to the db
    // public function insertPayment(array $input)
    // {
    //     $rows = DB::connection('ComsDB')->update("UPDATE UserSettings SET active = 0 WHERE userID = $this->id");
    //     $sql = "
    //                 INSERT INTO Payments
    //                 VALUES (, $this->id, '$value1', '$value2', 1)
    //     ";

    //     try {
    //         DB::connection('ComsDB')->update($sql);
    //     } catch (Exception $e) {
    //         error_log($e->getMessage());
    //         return 'false';
    //     }
    //     self::refreshSettings();
    //     return 'true';
    // }
}  //end class

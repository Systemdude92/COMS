<?php

/*
 * File Name: Settings.php
 * Created By: W. Logan McCourry
 * Created On: October 9, 2017
 * Description: Filter Object for Business Logic
*/

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Settings
{

    const SETTING_TYPE = ['alpha', 'grade', 'special', 'acitivity', 'counselor'];
    const ALL_STUDENT_SETTINGS = ['all students'];
    const MULTI_VALUE_SETTINGS = ['alpha', 'grade'];
    const DROP_DOWN_SETTINGS = ['counselor', 'special', 'activity', 'other user'];

    public $id = '';
    public $name = '';
    public $description = '';
    public $value1 = '';
    public $value2 = null;
    public $active = false;

    /**
    * Function: __constrcut()
    * @param string $id of User's Settings
    * @throws Exception $type is NOT in SETTING_TYPE
    */
    public function __construct($id = null)
    {
        if($id != null){
            $comsDB = DB::connection('ComsDB');
            $setting = $comsDB->table('UserSettings')
                            ->select('UserSettings.id', 'Settings.name AS name', 'Settings.description as description', 'value1', 'value2', 'active')
                            ->join('Settings', 'Settings.id', '=', 'UserSettings.settingsID')
                            ->where('UserSettings.id', '=', $id)
                            ->first();
            if(count($setting) > 0){
                $this->id = $setting->id;
                $this->name = $setting->name;
                $this->description = $setting->description;
                $this->value1 = $setting->value1;
                $this->value2 = $setting->value2;
                $this->active = $setting->active;
            } else {
                throw new Exception("User Setting Does not Exist");
            }
        }
    }

    public function getSpecials()
    {
        $specials = DB::connection('ComsDB')
                    ->table('Specials')
                    ->select('name')
                    ->get();
        return $specials;
    }

    public function getCounselors()
    {
        $orgCode = Session::get('user')->getOrgCode();
        $counselors = DB::connection('ComsDB')
                        ->table('Students')
                        ->distinct()
                        ->select('Counselor AS name')
                        ->where('orgCode', '=', $orgCode)
                        ->orderby('Counselor')
                        ->get();
        return $counselors;
    }

    public function getActivities()
    {
        $activities = DB::connection('ComsDB')
                    ->table('Activities')
                    ->select('name')
                    ->get();
        return $activities;
    }

    public function getOtherUsers()
    {
        $orgCode = Session::get('user')->getOrgCode();
        $otherUsers = DB::connection('ComsDB')
                    ->table('Users')
                    ->select('displayName AS name', 'id', 'username', 'comsRole')
                    ->where('orgCode', '=', $orgCode)
                    ->get();
        return $otherUsers;
    }

    /**
     * returns Setting Object for setting
     *
     * @param int $userID
     */
    public function getOtherUserSettings(int $userID)
    {
        $settings = DB::connection('ComsDB')
                            ->table('UserSettings')
                            ->select('id')
                            ->where('userID', '=', $userID)
                            ->where('active', '=', 1)
                            ->first();
        return $settings->id;
    }

}

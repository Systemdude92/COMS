@extends('Layout.mainLayout')

@section('head')
    <link href='{{asset('css/search.css')}}' rel='stylesheet' type='text/css' />

@endsection

@section('content')
    <h1>Student Results</h1>
    <input id='keysearch' type='hidden' value='{{$key}}'/>
    <div id='searchResultsDiv'>
    {{--  @include('search.searchTable')  --}}
    <table class='ui unstackable table' id='searchResultsTable'>
    <thead>
        <tr>
            <th class='all'></th>
            <th class='all'>Student Name</th>
            <th class='all'>Student ID</th>
            <th>Grade</th>
            <th>School Name</th>
            <th class='none'>Date of Birth:</th>
            <th class='none'>Age:</th>
            <th class='none'>Genger:</th>
            <th class='none' id='buttons'></th>
        </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
        <tr>
            <td></td>
            <td><a href='/profile/student/{{$student->studentID}}'>{{$student->displayName}}</a></td>
            <td>{{$student->studentID}}</td>
            <td>{{$student->grade}}</td>
            <td>{{$student->campusName}}</td>
            <td>{{date('F d, Y', strtotime($student->dob))}}</td>
            <td>{{$student->age}}</td>
            <td>{{$student->gender}}</td>
            <td>
                <div>
                    <a class='ui button' href='http://munisreporting/ReportServer?%2fCOMS+Reports%2fCOMSOfficePass&StudentID={{$student->studentID}}&rs:Command=Render&rs:Format=PDF'><i class='file pdf outline icon'></i>Get Office Pass</a>
                    <a class='ui button' href='/profile/student/{{$student->studentID}}'><i class='user icon'></i>View Profile</a>
                    <a class='ui button' href='/concerns/{{$student->studentID}}'><i class='folder open icon'></i>Submit Student Form</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th>Student Name</th>
            <th>Student ID</th>
            <th>Grade</th>
            <th>School Name</th>
            <th class='none'>Date of Birth</th>
            <th class='none'>Age</th>
            <th class='none'>Genger</th>
            <th class='none'></th>
        </tr>
    </tfoot>
</table>
    </div>
    <script src='{{asset('js/search.js')}}' rel='script' type='text/javascript'></script>
@endsection

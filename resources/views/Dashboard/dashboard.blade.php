<?php
/**
 * FileName: dashboard.blade.php
 * Created by PhpStorm.
 * User: mccourryw
 * Date: 9/14/2017
 * Time: 10:55 AM
 * Description: Landing page for all Users that have rights to the DashBoard.
 */
?>




@extends('Layout.mainLayout')

@section('head')
    <link href='{{asset('css/dashboard.css')}}' rel='stylesheet' type='text/css' />
    <script src='{{asset('js/dashboard.js')}}' rel='script' type='text/javascript'></script>
@endsection

@section('content')

    <h1>To-Do Lists</h1>
    <div class='ui stackable four column grid' >
        <div class='ui very relaxed horizontal list toDoListMenu' id='collectiveLists'>

                @foreach($collective as $collect)
                    <a class='column item toDoListItem' href="/lists/{{$collect['id']}}">
                        <i class='{{$collect['icon']}} huge icon' style='background-color: {{$collect['color']}}'></i>
                        <div class='content'>
                            <h3 class='header'>{{$collect['name']}}</h3>
                            <div class='description' data-content='{{$collect['description']}}'>
                                {{$collect['displayedDescription']}}
                            </div>
                        </div>
                        <div class='floating ui yellow label'>{{$collect['total']}}</div>
                    </a>
                @endforeach
        </div>
    </div>

    <h1>Individual Lists</h1>
    <div class='ui stackable two column grid listGroups'>
        <div class='ui very relaxed horizontal list toDoListMenu' id='individualLists'>
            @if (count($individual) > 0)
                @foreach($individual as $ind)
                <a class='column item toDoListItem' href="/lists/{{$ind['id']}}">
                    <i class='{{$ind['icon']}} huge icon' style='background-color: {{$ind['color']}}'></i>
                    <div class='content'>
                        <h3 class='header'>{{$ind['name']}}</h3>
                        <div class='description' data-content='{{$ind['description']}}'>
                            {{$ind['displayedDescription']}}
                        </div>
                    </div>
                    <div class='floating ui yellow label'>{{$ind['total']}}</div>
                </a>
                @endforeach
            @else
                <div class='ui red segment'>
                    <h2 class='ui header'>No Lists Available</h2>
                    <p>There are currently No Lists that have Concerns that need to be addressed at this time</p>
                </div>
            @endif
        </div>
    </div>
@endsection

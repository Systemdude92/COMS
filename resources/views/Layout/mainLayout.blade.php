<?php
/**
 * FileName: mainLayout.blade.php
 * User: mccourryw
 * Date: 9/13/2017
 * Time: 1:20 PM
 * Description : Main Layout for ALL pages in COMS 2.0
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    {{-- START OF GLOBAL HEAD --}}
    <meta charset="UTF-8">
    <title>COMS 2.0 - {{$title}}</title>
    <meta name="author" content="Logan Mccourry, Velma Vessels">
    <meta name="keyword" content="coms, lisd, lewisville independent school district, school district, lewisville">
    <meta name="description" content="Campus Administrator Tool to maintain and evaluate students.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('Layout.components.head')
    @yield('head')
</head>
<body>

    @include('Layout.components.leftSideBar')

    @include('Layout.components.rightSideBar')

    {{-- START MAIN CONTENT --}}
    <div class='pusher'>
        @include('Layout.components.headerBar')
        @include('Layout.components.siteNav')
        <div id='mainContentContainter'>
            @yield('content')
        </div>
        <footer class="ui bottom fixed menu">
            @include('Layout.components.footerBar')
            @yield('footer')
        </footer>
    </div>

</body>
</html>

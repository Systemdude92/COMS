@if(count(Session::get('user')->getSiteNav()) > 0)
    <h1 class="ui main header">Quick Links</h1>
    <div class="ui six doubling raised link cards" id='siteNavDiv'>
    @foreach(Session::get('user')->getSiteNav() as $nav)
        <div class='card'>
            <a class='center cardLinkTitle image' href='/{{$nav->shortName}}' style='background-color: {{$nav->color}}' target='_parent'>
                <i class='cardIcons huge {{$nav->icon}} icon'></i>
            </a>
            <a class='content' href='/{{$nav->shortName}}' target='_parent'>
                <div class='header'>{{$nav->name}}</div>
                <div class='meta'>
                    <div>Quick Link</div>
                </div>
                <div class='description'>{{$nav->description}}</div>
            </a>
            <a class='extra content' href='/{{$nav->shortName}}' target='_parent'>
                <span class='right floated'>
                    "Click here for More information"
                </span>
            </a>
        </div>
    @endforeach
    </div>
@endif

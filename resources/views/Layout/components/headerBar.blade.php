{{-- START HEADER BAR --}}
@if(Session::get('user')->comsRole != Session::get('user')::USER_NORMAL)
        <header>
            <script>
                $(document).ready(function() {
                    $('.pusher').backstretch('{{asset('img/background/LISDBackground.png')}}');
                });
            </script>
            <div class="ui top fixed menu" id="topHeaderMenuBar">
                <div class="item" id='leftSideBarButton'>
                    <i class="sidebar icon"></i>
                </div>
                    <div class='ui fluid category search' id='headerBarStudentSearch' style='width: 80%'>
                        <div class='ui icon input' style='width: 100%'>
                            <input class='prompt' type='text' placeholder='Student Search'>
                            <i class='search icon'></i>
                        </div>
                    </div>
                <div class="right menu">
                    <div class="item" id='bugReportButton'>
                        <div class="ui image"><i class="bug icon"></i></div>
                    </div>
                    <div class='browse item active' id="headerBarUserInfo">
                            <img src='{{asset("img/LISDLogo/LISD-small.jpg")}}' class='ui mini circular image left floated' alt='User Image'>
                            <span class='right floated hidden-xs'>{{Session::get('user')->getDisplayName()}}</span>
                            <i class="dropdown icon"></i>
                    </div>
                    @if(Session::get('user')->comsRole != Session::get('user')::USER_NORMAL)
                    <div class="item" id='rightSideBarButton'>
                        <i class="setting icon"></i>
                    </div>
                    @endif
                </div>
            </div>

            {{--  START OF BUG MODAL  --}}
            <div class='ui modal' id='bugReportingModal'>
                <div class='header'></div>
                <div class='content'>
                    <div class='ui header'>Bug Report</div>
                    <form class='ui form' action='javascript:void(0)' type='post' id='bugReportForm'>
                        <div class='field'>
                            <label>Subject</label>
                            <input type='text' name='bugSubject' id='bugSubject' placeholder='Subject of issue'>
                        </div>
                        <div class='field'>
                            <label>Description</label>
                            <input type='textarea' name='bugDescription' id='bugDescription' placeholder='Description'>
                        </div>
                        <button class='ui button' type='submit'>Submit</button>
                    </form>
                </div>
            </div>
            {{-- END OF BUG MODAL --}}

            {{--  START USER INFO POPUP CONTENT --}}
            <div class="ui popup bottom left transition hidden" id="userInfoPopup">
                <div class='ui one column relaxed equal height divided grid'>
                    <div class="column" id="userInfoPopupMainColumn">
                        <div class="ui header">
                            <img class='ui small centered circular image' src='{{asset("img/LISDLogo/LISD-small.jpg")}}' alt="Small LISD Logo">
                            <h4 class='ui header profileDropdown'>
                                {{Session::get('user')->getDisplayName()}}
                            </h4>
                            <h5 class='ui header profileDropdown'>
                                Campus: {{Session::get('user')->getCampusName()}}
                            </h5>
                            <small>According to Active Directory</small>
                        </div>
                        <div class="ui three column equal height divided grid">
                            <div class="column"><a href="http://lisd.net/site/Default.aspx?PageType=1&SiteID=4&ChannelID=89&DirectoryType=6\" target="_blank">Resources</a></div>
                            <div class="column"><a href="http://www.lisd.net" target="_blank">LISD Home</a></div>
                            <div class="column"><a href="http://lisd.net/site/Default.aspx?PageType=7&SiteID=899\" target="_blank">Intranet</a></div>
                        </div>
                        <div class="user-footer">
                            <div class="ui two very padded buttons">
                                <a href="{{route('lock')}}" class="ui button secondary"><i class="lock icon"></i>Lock</a>
                                <a href="{{route('logout')}}" class="ui button secondary">Sign Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--  END USER INFO POPUP CONTENT --}}
        </header>
    @endif

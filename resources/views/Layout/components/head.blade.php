{{-- PLACE GLOBAL THIRD PARTY CSS FILES HERE --}}
    <link href='{{asset('img/LISDLogo/LISD-small.jpg')}}' rel='icon' />
    <link href='{{asset('css/vendor.css')}}' type='text/css' rel='stylesheet' />
    <link href='{{asset('semantic/dist/semantic.min.css')}}' type='text/css' rel='stylesheet' />
    <link href='{{asset('DataTables/datatables.css')}}' type='text/css' rel='stylesheet' />

    {{-- PLACE GLOBAL HOME MADE CSS FILES HERE --}}
    <link href='{{asset('css/app.css')}}' type='text/css' rel='stylesheet' />
    {{-- END GLOBAL HOME MADE CSS --}}

    {{-- VENDOR JS Files --}}
    <script src='{{asset('js/manifest.js')}}' type='text/javascript' rel='script'></script>
    <script src='{{asset('js/vendor.js')}}' type='text/javascript' rel='script'></script>
    <script src='{{asset('semantic/dist/semantic.min.js')}}' type='text/javascript' rel='script'></script>
    <script src='{{asset('DataTables/datatables.js')}}' type='text/javascript' rel='script'></script>
    {{--  <script src='{{asset('js/dashboard.js')}}' rel='script' type='text/javascript'></script>  --}}
{{--    <script src='{{asset('js/concerns.js')}}' rel='script' type='text/javascript'></script>--}}
    {{-- END VENDOR JS --}}

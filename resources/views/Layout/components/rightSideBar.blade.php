{{-- RIGHT SIDE BAR --}}
    <div class='ui sidebar overlay inverted right vertical menu' id='mainRightSideBar'>
        <div class='item'>
            <form class='ui form' action='javascript:void(0)' id='userSettingsForm'>
                <h4 class='ui dividing inverted header'>User Setting Config</h4>
                <div id='divAllStudents'>
                    <div class='field'>
                        <div class='ui radio checkbox userSettingsToggle'>
                            <input type='radio' id='allStudents' name='userSetting'>
                            <label for='allStudentsSetting'>All Students</label>
                        </div>
                    </div>
                </div>
                <div id='divAlpha'>
                    <div class='field'>
                        <div class='ui radio checkbox userSettingsToggle'>
                            <input type='radio' id='alpha' name='userSetting'>
                            <label for='alphaSetting'>Alpha</label>
                        </div>
                    </div>
                </div>
                <div id='divGrade'>
                    <div class='field'>
                        <div class='ui radio checkbox userSettingsToggle'>
                            <input type='radio' id='grade' name='userSetting'>
                            <label for='gradeSetting'>Grade</label>
                        </div>
                    </div>
                </div>
                <div id='divCounselor'>
                    <div class='field'>
                        <div class='ui radio checkbox userSettingsToggle'>
                            <input type='radio' id='counselor' name='userSetting'>
                            <label for='counselorSetting'>Counselor</label>
                        </div>
                    </div>
                </div>
                <div id='divSpecial'>
                    <div class='field'>
                        <div class='ui radio checkbox userSettingsToggle'>
                            <input type='radio' id='special' name='userSetting'>
                            <label for='specialSetting'>Specials</label>
                        </div>
                    </div>
                </div>
                <div id='divActivity'>
                    <div class='field'>
                        <div class='ui radio checkbox userSettingsToggle'>
                            <input type='radio' id='activity' name='userSetting'>
                            <label for='activitySetting'>Activity</label>
                        </div>
                    </div>
                </div>
                <div id='divOtherUser'>
                    <div class='field'>
                        <div class='ui radio checkbox userSettingsToggle'>
                            <input type='radio' id='otherUser' name='userSetting'>
                            <label for='activitySetting'>Other User</label>
                        </div>
                    </div>
                </div>
                <button id='userSettingsSubmit' style='display: none;' class='ui positive button' type='submit'>Save</button>
                <button id='userSettingsLoading' class='ui loading button' type='button'>Loading</button>
            </form>
        </div>
        @if(strtolower(Session::get('user')->getCampusName()) == 'technology' && (Session::get('user')->getComsRole() == Session::get('user')::USER_ADMIN || Session::get('user')->getComsRole() == Session::get('user')::USER_SITE_ADMIN))
            <h3 class='ui header'>Tech Options</h3>
            <h4 class='ui header'>Current OrgCode: {{Session::get('user')->getOrgCode()}}</h4>
            <form action='javascript:void(0)' method='POST' id='techOrgCodeForm'>
                <div class='field'>
                    <div class='ui selection dropdown'>
                        <input type='hidden' name='orgCode'>
                        <div class='default text'>Choose Campus</div>
                        <div class='menu'></div>
                    </div>
                </div>
                <button class='ui positive button' type='submit'>Save</button>
            </form>
        @endif
    </div>
    {{-- END RIGHT SIDE BAR --}}

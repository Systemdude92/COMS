{{-- LEFT SIDE BAR --}}
    <div class='ui sidebar overlay inverted labeled icon left vertical menu' id='mainLeftSideBar'>
    @foreach(Session::get('user')->getSiteNav() as $nav)
        <a class='item' href='/{{$nav->shortName}}'>
            <i class='{{$nav->icon}} icon'></i>
            {{$nav->name}}
        </a>
    @endforeach
    </div>
    {{-- END LEFT SIDE BAR --}}

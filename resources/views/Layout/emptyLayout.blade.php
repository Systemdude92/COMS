<?php
/**
 * FileName: emptyLayout.blade.php
 * User: mccourryw
 * Date: 9/14/2017
 * Time: 8:45 AM
 * Description: MainLayout for screens that do NOT need the full layout (i.e. headBar, leftSideBar, rightSideBar, footer)
 */

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>COMS 2.0 - Login</title>
        <link href='{{asset('img/LISDLogo/LISD-small')}}' rel='icon'/>
        <link href='{{asset('css/empty-app.css')}}' type='text/css' rel='stylesheet' />
        <link href='{{asset('semantic/dist/semantic.css')}}' type='text/css' rel='stylesheet'/>
        <script src='{{asset('js/empty-vendor.js')}}' type='text/javascript' rel='script'></script>
        @yield('head')
    </head>
    <body>
        <script>
            $.backstretch('{{asset('img/background/LISDBackground.png')}}');
        </script>
        @yield('content')
        <footer class="ui bottom fixed menu">
            @include('Layout.components.footerBar')
        </footer>

    </body>
</html>
`

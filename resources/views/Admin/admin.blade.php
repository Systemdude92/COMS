
@extends('Layout.mainLayout')

@section('head')
    <link href='{{asset('css/admin.css')}}' type='text/css' rel='stylesheet' />
    <script src='{{asset('js/admin.js')}}' type='text/javascript' rel='script'></script>
@endsection

@section('content')

<div id='adminIndividualUserContainer'>
    <div class='ui raised very padded text container segment'>
        <div id='mainTitleHeader' class='ui header centered'>
            <div class='ui large image titleImage'>
                <img class='titleImage' src='{{asset('img/LISDLogo/LISD-small.jpg')}}'>
            </div>
            Admin Console
            <div class='ui large image titleImage'>
                <img class='titleImage' src='{{asset('img/LISDLogo/LISD-small.jpg')}}'>
            </div>
        </div>
        <div class='ui divider'></div>
        <h2 class='ui header'>Select User: </h2>
        <select class='ui search dropdown' id='userSelector'>
            <option value=''>Choose User</option>
            @foreach($otherUsers as $user)
                @if($user->name != Session::get('user')->displayName)
                <option value='{{$user->username}}'>{{$user->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>

{{--  <div id='adminTableContainer'>
    <table>
        <thead>
            <tr>
                <th></th>
                <th colspan='{{$reportCount * 2}}'>Reports</th>
            </tr>
            <tr id='reportHeader'>
                <th>Users</th>
            @foreach($reports as $report)
                @if(!in_array($report->id, $emptyReportObject::COLLECTIVE_LISTS))
                <th colspan='2'>{{$report->name}}</th>
                @endif
            @endforeach
            </tr>
            <tr id='readWriteHeader'>
                <td></td>
                 @foreach($reports as $report)
                    <th>Read</th>
                    <th>Write</th>
                 @endforeach
            </tr>


        </thead>
        <tbody>
            @foreach($otherUsers as $index => $user)
            <tr class='userColumns {{$user->mod}}'>
                <th>{{$user->name}}</th>
                @foreach($reports as $list)
                <td class='readColumn'>
                    <div class='ui checkbox'>
                        <input type='checkbox' />
                        <label></label>
                    </div>
                </td>
                <td class='writeColumn'>
                    <div class='ui checkbox'>
                        <input type='checkbox' />
                        <label></label>
                    </div>
                </td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>  --}}
@endsection

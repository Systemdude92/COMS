<div class="ui fluid card">
    <form id='detentionKioskForm' action='' class='ui form' method='post'>
        <input type='checkbox' @if($detKiosk->active) checked @endif />
        <h2 class='ui header'>Detention Kiosk</h2>

        <div class='inline field'>
            <label>Start Date:<span data-position='top center' data-html="Start Date: The Day the Kiosk will activate for the school<br/><strong>Must be Today or later</strong>"><i class='ui info circle icon'></i></span></label>
            <input disabled value='@if($detKiosk->active){{$detKiosk->startDate->format(' m/d/Y ')}}@endif' type="text" class='ui input'
                name='startDate' id='detentionStartDate' placeholder='01/01/2018' />
        </div>
        <div class='inline field'>
            <label>Duration of Tardies:<span data-position='top center' data-html="Duration: Sets the time in which Tardy count will reset. <br/><strong>i.e. Semester will reset Tardy Count after every semester</strong>"><i class='ui info circle icon'></i></span></label>
            <select disabled name='detentionDuration' class='ui selection dropdown' value='{{$detKiosk->duration}}'>
                                <option class='item'>Semester</option>
                                <option class='item'>Quarterly</option>
                            </select>
        </div>
        <div class='inline field'>
            <label>Tardy Threshold:<span data-position='top center' data-html="Threshold: The amount of tardies a student may have before automatically becoming a Teacher Referral"><i class='ui info circle icon'></i></span></label>
            <input disabled value='{{$detKiosk->threshold}}' id='threshHoldInput' class='ui input' type='text' />
        </div>
        <div class='inline field'>
            <label>Days to Serve:<span data-position='top center' data-html="Days to Serve: The amount of Business days a student may have in order to serve a Detention for their Tardy.<br/><strong>Does not include the day the tardy was created</strong>"><i class='ui info circle icon'></i></span></label>
            <input disabled value='{{$detKiosk->hoursToServe / 24}}' id='daysServeInput' class='ui input' type='text' />
        </div>
    </form>
</div>

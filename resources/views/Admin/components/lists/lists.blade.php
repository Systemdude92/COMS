<div class="ui section">
    <h2 class="ui header">Report Aliases</h2>
    {{--  <div class="ui two column grid container">
        <div class="row">
            <div class="column">
                <h3 class="ui header">Default Name</h3>
            </div>
            <div class="column">
                <h3 class="ui header">Current Allias</h3>
            </div>
        </div>
    @foreach($reports as $report)
        <div class="row">
            <div class="column">
                <h4><strong>{{$report->defaultName}}</strong></h4></div>
            <div class="column">
                <input type="text" value="{{$report->name}}" class="ui input">
            </div>
        </div>
    @endforeach
    </div>  --}}
    <table class="ui very basic table">
        <thead>
            <tr>
                <td>Default Name</td>
                <td>Current Allias</td>
            </tr>
        </thead>
        <tbody>
            @foreach($reports as $report)
            @if(!in_array($report->id, $reportObject::COLLECTIVE_LISTS) && !in_array($report->id, $reportObject::THIRD_PARTY_SOURCE_LISTS))
            <tr class='aliasList' id='{{$report->id}}'>
                <td class='listDefaultName'>{{$report->defaultName}}</td>
                <td class='listAliasName'><input type="text" value="{{$report->name}}" class="ui input"></td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
</div>

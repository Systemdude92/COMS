

@extends('Layout.mainLayout')

@section('head')
    <link href='{{@asset('css/admin.css')}}' rel='stylesheet' type='text/css' />
@endsection

@section('content')
    <div class='ui three column centered grid' id='campusSettings'>
        <div class='ui raised left aligned stacked segment'>
            <h1 class='ui header'>Campus Settings For: <strong>{{$campus->campusName}}</strong></h1>
            <div class="ui divider"></div>
            <h2 class="ui header">List Settings</h2>
            @include('Admin.components.kiosks.kiosklayout')
            <div class='ui divider'></div>
            @include('Admin.components.lists.lists')
            <div class="ui divider"></div>
            <button id='detentionSave' class='ui positive fluid button' type='submit'>Save</button>
            <button disabled style='display:none' id='detentionLoad' class='ui primary loading fluid button' type='button'>Loading</button>
            <div style='display:none' class="ui negative message">
                <div class="header">Errors!</div>
                <ul></ul>
            </div>
            <div style='display:none' class="ui positive message">
                <div class="header">Success!</div>
                <p>Settings were saved Successfully</p>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src='{{asset('js/siteadmin.js')}}' type='text/javascript' rel='script'></script>
@endsection

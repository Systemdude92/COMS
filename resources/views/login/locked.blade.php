<?php


/**
 * FileName: locked.blade.php
 * Created by PhpStorm.
 * User: mccourryw
 * Date: 9/14/2017
 * Time: 10:55 AM
 * Description: Locked Screen for when user has been away from screen
 */

?>


@extends('Layout\emptyLayout')

@section('content')


    <!-- Future Redesign -->


    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <div class="ui raised centered very padded text container segment" id="loginSegment">
                <img class="ui small centered image" src="{{asset('img/LISDLogo/LISD-small.jpg')}}"></img>
                <h2 class="ui centered header">
                    <div class="content">COMS 2.0 Locked</div>
                </h2>
                <h4>Signed in has <strong>{{Session::get('user')->getDisplayName()}}</strong></h4>
                <p class="login-box-msg">Confirm password to unlock</p>
                <form action="{{action('LoginController@unlock')}}" class="ui large form" method="post">
                    <div class="ui stacked segment" id="loginForm">
                        <input type="hidden" name="username" id="username" placeholder="LISD Username" value="{{Session::get('user')->getUsername()}}">
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" id="password" placeholder="LISD Password">
                            </div>
                        </div>
                        <button type="submit" class="ui fluid large primary submit button">Login</button>
                    </div>
                </form>
                <a href="{{action('LoginController@logout')}}">Sign in as a Different User</a>
            </div>
        </div>
    </div>


<!-- Automatic element centering -->
{{--<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <b>CO</b>MS

    </div>
    <div class="login-box-msg">
        <small>Campus Operations Management System</small>
    </div>

    <!-- User name -->
    <div class="lockscreen-name">{{Session::get('user')->getDisplayName()}}</div>

    <!-- START LOCK SCREEN ITEM -->
    <div class="lockscreen-item scene_element scene_element--fadein">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <img src="{{asset('img/LISDLogo/LISD-small.jpg')}}" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->
        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" autocomplete="off" role="form" action="" method="post">
            <div class="input-group">
                <input type="text" class="form-control" name="username" autocomplete="new-password" placeholder="username" value="{{Session::get('user')->getUsername()}}" style="display: none;">
                <input id="password" type="password" autocomplete="new-password" class="form-control" name="password" placeholder="password" required="required" pattern=".{1,}" title="2 characters minimum">
                <div class="ui button">
                    <button type="submit" class="ui icon button"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
            </div>
        </form>
        <!-- /.lockscreen credentials -->

    </div>
    <!-- /.lockscreen-item -->
    <div class="help-block text-center">
        Enter your password to retrieve your session
    </div>
    <div class="text-center">
        <a href="sitesControl/logout.php">Or sign in as a different user</a>
    </div>
    <div class="lockscreen-footer text-center">
        <b><a href="https://lisd.net" class="text-black">Lewisville Independent School District</a></b>
    </div>
</div>--}}

@endsection
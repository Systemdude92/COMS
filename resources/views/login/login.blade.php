<?php
/**
 * Created by PhpStorm.
 * User: mccourryw
 * Date: 9/13/2017
 * Time: 1:15 PM
 */
?>

@extends('Layout.emptyLayout')

@section('head')
    <link href="{{asset('css/login.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <div class="ui raised centered very padded text container segment" id="loginSegment">
                <img class="ui small centered image" src="{{asset('img/LISDLogo/LISD-small.jpg')}}"></img>
                <h2 class="ui centered header">
                    <div class="content">COMS 2.0 Login</div>
                </h2>
                <p class="login-box-msg">Sign in with your LISD username/email and password</p>
                <form action="{{route('login')}}" class="ui large form" method="post">
                    <div class="ui stacked segment" id="loginForm">
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="user icon"></i>
                                <input type="text" name="username" id="username" placeholder="LISD Username"  value="{{old('username')}}">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" id="password" placeholder="LISD Password">
                            </div>
                        </div>
                        <button type="submit" class="ui fluid large primary submit button">Login</button>
                    </div>
                    <div class="ui error message"></div>

                    @if ($errors->any())
                        <div class="ui message" id="loginErrorMessageSegment">
                            <h2 style="text-align: left">Errors!!!</h2>
                            <div class="ui list" >
                                @foreach ($errors->all() as $error)
                                    <div class="item"> {{$error}}</div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                        @if (isset($loginErrors))
                            <div class="ui message" id="loginErrorMessageSegment">
                                <h2 style="text-align: left">Errors!!!</h2>
                                <div class="ui list" >
                                    @foreach ($loginErrors as $error)
                                        <div class="item"> {{$error}}</div>
                                    @endforeach
                                </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@extends('Layout\mainLayout');

@section('head')
    <link href='{{asset('css/concerns.css')}}' type='text/css' rel='stylesheet' />
    <script src='{{asset('js/concerns.js')}}' type='text/javascript' rel='script'></script>
@endsection

@section('content')
    <div id='surroundingFormDiv'>
        <div class='ui raised very padded text container segment'>
            <div id='mainTitleHeader' class='ui header centered'>
                <div class='ui large image titleImage'>
                    <img class='titleImage' src='{{asset('img/LISDLogo/LISD-small.jpg')}}'>
                </div>  Student Concerns Form  <div class='ui large image titleImage'>
                    <img class='titleImage' src='{{asset('img/LISDLogo/LISD-small.jpg')}}'>
                </div><br/>
                    <small>You are currently signed in as {{Session::get('user')->getDisplayName()}}<br/>
                    Logging a form for {{$student->displayName}}
                    </small><br/>
                <a href='/concerns' id='unsetStudentID' class='ui button fluid secondary bottom' type='submit'>Wrong Student? Click here to restart -></a>
            </div>
            <div class='ui divider'></div>
            <form id='studentConcernsForm' class='ui form revalidate'>
                <div class='ui equal width form'>
                    <div class='ui header left'>
                        Student Information<br/>
                        <small style='color: red'>*Note all fields are required</small>
                    </div>
                    <div class='fields'>
                        <div class='required field'>
                            <label>First Name/Middle Name</label>
                            <input id='firstName' name='firstName' data-validate='firstName' readonly type='text' placeholder='First Name' value='{{$student->firstName}}'>
                        </div>
                        <div class='required field'>
                            <label>Last Name</label>
                            <input id='lastName' name='lastName' data-validate='lastName' readonly type='text' placeholder='Last Name' value='{{$student->lastName}}'>
                        </div>
                        <div class='required field'>
                            <label>Student ID</label>
                            <input id='studentID' name='studentID' data-validate='studentID' readonly type='text' placeholder='Student ID' value='{{$student->studentID}}'>
                        </div>
                    </div>
                    <div class='fields'>
                        <div class='required field'>
                            <label>Grade Level</label>
                            <input id='gradeLevel' name='gradeLevel' data-validate='gradeLevel' readonly type='text' placeholder='Grade Level' value='{{$student->grade}}'>
                        </div>
                        <div class='required field'>
                            <label>School ID</label>
                            <input id='schoolID' name='schoolID' data-validate='schoolID' readonly type='text' placeholder='School ID' value='{{$student->orgCode}}'>
                        </div>
                    </div>
                    <div class='ui divider'></div>
                    <div class='grouped fields'>
                    <div class='ui header'>
                        Reason For Concern
                    </div>

                    @foreach($reports as $report)
                    <div class='field'>
                        <div class='ui slider checkbox studentForm'>
                            <input id='{{$report->id}}' name='reason' type='radio' class='hidden reasonSelection' value='{{$report->id}}'>
                            <label>{{$report->name}}</label>
                        </div>
                    </div>
                    @endforeach
                    </div>

                    <div class='ui divider'></div>
                    <div class='ui header'>Description</div>
                    <div class='fields'>
                        <div class='required field'>
                            <textarea required id='description' name='description'></textarea>
                        </div>
                    </div>
                    <div class='ui divider'></div>
                    <div class='buttonDiv'>
                        <button id='studentFormSubmitButton' class='ui positive submit button fluid studentFormButton' type='submit'>Submit</button>
                        <a id='studentFormCancelButton' class='ui negative button fluid studentFormButton' href='/concerns'>Cancel</a>
                        <button id='studentFormLoadingButton' class='ui primary loading button fluid studentFormButton' disabled type='button' style='display: none;'>Loading</button>
                    </div>

                    <div class='ui error message'></div>
                </div>
            </form>
        </div>
<!-- ending surrounding div-->
    </div>

@endsection

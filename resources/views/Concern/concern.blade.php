<?php
/**
 * Created by PhpStorm.
 * User: mccourryw
 * Date: 9/28/2017
 * Time: 8:30 AM
 */
?>

@extends('Layout\mainLayout')

@section('head')
    <link href='{{asset('css/concerns.css')}}' type='text/css' rel='stylesheet' />
@endsection

@section('content')

    @if(Session::has('message'))
        <div class='ui positive message'>
            <i class='close icon'></i>
            <div class='header'>
                Success!
            </div>
            <p>{{Session::get('message')}}</p>
        </div>
    @endif

    <div class='ui raised very padded text container segment'>
        <div id='mainTitleHeader' class='ui header centered'>
            <div class='ui large image titleImage'>
                <img class='titleImage' src='{{asset('img/LISDLogo/LISD-small.jpg')}}'>
            </div>  Student Concerns Form
            <div class='ui large image titleImage'>
                <img class='titleImage' src='{{asset('img/LISDLogo/LISD-small.jpg')}}'>
            </div><br/>
            <small>You are currently signed in as {{Session::get('user')->getDisplayName()}}</small><br/>
        </div>
        <div class='ui divider'></div>
        <form id='studentIdForm' class='ui form' method='post'>
            <div class='ui equal width form'>
                <div class='ui header left'>
                    Enter Student ID Number<br/>
                    <small>*Note all fields are required</small>
                </div>
                <div class='fields'>
                    <input type='hidden' id='studentIDInputForForm'>
                    <div class='required field' id='studentFormSearchField'>
                     <div class='ui category search' id='studentFormSearch'>
                        <div class='ui icon input'>
                            <input id='studentFormInput' class='prompt' type='text' placeholder='Search for Student'>
                            <i class='search icon'></i>
                            </div>
                                <div id='overFlowResults' style='overflow: scroll'>
                                    <div class='results'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class='buttonDiv'>
                    <button id='studentIdSubmit' class='ui primary submit button fluid' type='submit'>Next</button>
                </div>
                <div class='ui error message'></div>
            </div>
        </form>
    </div>
@endsection

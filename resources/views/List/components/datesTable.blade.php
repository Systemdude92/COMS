<table class='ui unstackable table' id='formsTable' data-tableType='datesTable'>
    <thead>
        <tr>
            <th class='childBox all'></th>
            <th></th>
            <th>Student ID</th>
            <th class='all'>Student Name</th>
            <th class='all'>Report Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Submitted Date</th>
            <th>Submitted By</th>
            <th>Status</th>
            <th class='none' id='ticket_description'></th>
            <th class='none'>Notes</th>
            <th class='none' id='buttons'></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    {{--  <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th>Student ID</th>
            <th>Student Name</th>
            <th>Report Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Submitted Date</th>
            <th>Submitted By</th>
            <th>Status</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>  --}}
</table>

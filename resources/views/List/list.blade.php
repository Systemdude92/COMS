<?php

/**
 * File: list.blade.php
 * Created By: W. Logan McCourry
 * Created On: 10/23/2017
 * Description: Page to Display a table to concerns based on Report type
 */
?>

@extends('Layout/mainlayout')

@section('head')
    <link href='{{asset('css/lists.css')}}' rel='stylesheet' type='text/css'>
@endsection

@section('content')

    <div class='ui positive message' style='display:none'>
        <i class='close icon'></i>
        <div class='header' id='successHeader'></div>
        <p id='successMessage'></p>
    </div>

    <div class='ui negative message' style='display:none'>
        <i class='close icon'></i>
        <div class='header' id='errorHeader'></div>
        <p id='errorMessage'></p>
    </div>

    <div id='filterControls'>

        @if(in_array($report->id, $report::COLLECTIVE_LISTS))
            <h3 class='ui header'>List Filter:
                <select name='filters' multiple='' class='ui fluid dropdown' id='listFilter'>
                    <option value=''>Lists Options</option>
                    @foreach($reports as $list)
                        @if(!in_array($list->id, $report::COLLECTIVE_LISTS))
                            <option value='{{$list->id}}'>{{$list->name}}</option>
                        @endif
                    @endforeach
                </select>
            </h3>
        @endif

        <h3 class='ui header'>Status Filter:
        <select name='filters' multiple='' class='ui fluid dropdown' id='statusFilter'>
            <option value=''>Statuses</option>
            @foreach($statuses as $status)
                <option value='{{$status->name}}'>{{$status->name}}</option>
            @endforeach
        </select>

        </h3>
    </div>


    <h2 id='{{$report->id}}'>{{$report->name}}</h2>
    @if(in_array($report->id, $report::COLLECTIVE_LISTS))
        <a href='' id='bulkOfficePassButton' style='display: none' class='ui button'>Bulk Office Pass</a>
    @endif

    @if(in_array($report->id, $report::DATES_LISTS))
        @include('List.components.datesTable')
    @elseif(in_array($report->id, $report::FEE_LISTS))
        @include('List.components.feeTable')
    @else
        @include('List.components.defaultTable')
    @endif

    @if(in_array($report->id, $report::COLLECTIVE_LISTS))
    <form style='display: none;' class='ui form' action='javascript:void(0)' method='post' id='globalListForm'>
        <div class='fields'>
            <div class='six wide field'>
                <label>Multi-Concern Note</label>
                <textarea id='globalNotes' placeholder='Multi-Concern Note'></textarea>
            </div>
        </div>
        <button type='submit'  class='ui positive button'>Submit</button>
    </form>
    @else
    <form style='display: none;' class='ui form' action='javascript:void(0)' method='post' id='globalListForm'>
        <div class='fields'>
            <div class='six wide field'>
                <label>Multi-Concern Note</label>
                <textarea id='globalNotes' placeholder='Multi-Concern Note'></textarea>
            </div>
        </div>
        <button type='submit'  class='ui positive button'>Submit</button>
    </form>
    @endif

@endsection

@section('footer')
    <script src='{{asset('js/lists.js')}}' type='text/javascript' rel='script'></script>
@endsection

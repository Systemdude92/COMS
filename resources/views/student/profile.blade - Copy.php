<?php
/*
|--------------------------------------------------------------------------
| resources\views\student\profile.blade.php
|--------------------------------------------------------------------------
|           Date:   11/10/2017
|    Description:   Display student profile data for the chosen student.
|
|--------------------------------------------------------------------------
|  Modifications:
|
|
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    {{-- START OF GLOBAL HEAD --}}

    <meta charset="UTF-8">
    <title>COMS 2.0 - {{$title}}</title>
    <meta name="author" content="Logan Mccourry, Velma Vessels">
    <meta name="keyword" content="coms, lisd, lewisville independent school district, school district, lewisville">
    <meta name='description' content='COMS 2.0 Profile Page for Students'>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('Layout.components.head')
    <link href='{{asset('css/profile.css')}}' rel='stylesheet' type='text/css' />

    <script src='{{asset('js/profile.js')}}'  rel='script' type='text/javascript'></script>

</head>


{{--  Fees/Fines and Payments Schedule popout  --}}
    <div class="ui modal" id='feeAndPaymentModal'>
        <i class="close icon"></i>
        <div class="header">

            <h5><strong>
                &nbsp &nbsp &nbsp
            Fees/Fines and Payments Schedule
                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
            Balance:  &nbsp &nbsp &nbsp
                <div>{{$feesAndPayments->balance}}</div>
            </strong></h5>
        </div>   {{--  div class="header"  --}}

        <div class="modal-content">
            <table class="ui celled table">

                    <th>Entered Date & Time </th>
                    <th>Entered By </th>
                    <th>Charges/Payments</th>
                    <th>Amount</th>

                    <tbody>

                        @foreach($feesAndPayments->feesAndPayments  as  $feeOrPmt)

                            <tr class='{{$feeOrPmt->rowColor}}'>
                                @if(($feeOrPmt->feeOrPayment) == 'F')
                                    <td>{{date('m-d-Y H:i', strtotime($feeOrPmt->theDate))}}</td>
                                    <td>{{$feeOrPmt->enteredBy}}</td>
                                    <td>{{$feeOrPmt->type}}</td>
                                    <td class='feeAmount'>{{$feeOrPmt->amount}}</td>
                                @else
                                    <td><strong><i>{{date('m-d-Y H:i', strtotime($feeOrPmt->theDate))}}<strong><i></td>
                                    <td><strong><i>{{$feeOrPmt->enteredBy}}<strong><i></td>
                                    <td><strong><i><div align=center>{{$feeOrPmt->type}}</div></i></strong></td>
                                    <td class='pmtAmount'>{{$feeOrPmt->amount}}</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
{{--  end of Fees/Fines and Payments Schedule popout  --}}


{{--  Apply Payment modal popout  --}}
<div  id='paymentModal'  class="ui modal">  
    <form method='POST'  class= 'ui form'>
        <i class="close icon"></i>
        <div class="header">
            <h3><strong>
                &nbsp &nbsp &nbsp
            Apply Payment
                &nbsp &nbsp &nbsp &nbsp  
            </strong></h3>

            <h4><strong>
            &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                Balance:  &nbsp &nbsp &nbsp $ {{$feesAndPayments->balance}}
            </strong></h4>
        </div>   

        <div class="modal-content">
            <table class="ui celled table">
                <tbody>
            <tr>
                <th>Payment Method:  
                        <select id="mySelect">
                                @foreach($feesAndPayments->getPaymentTypes()  as  $pmtType)
                                <option value='{{$pmtType->id}}'>  {{$pmtType->name}}</option>
                                @endforeach
                              </select>
                </th>
                <th>Amount:    $   
                    <input id='inputAmount' class='prompt' type='text'/>
                </th>
            </tr>
                </tbody>

</table>
                {{--  submit button at bottom of modal   --}}
    <div class='submit'>
            <button id='submitPayment' class='ui primary submit button fluid' type='button' >Submit</button>

                {{--  new 02/20 below  --}}
                        {{--  added:  @click="saveAction" on the line above  --}}


                {{--  new 02/20 above  --}}

    </div>
        </div>   
    </form>      
</div>  
{{-- ----- end of applyPayment Modal; popout ----- --}}

{{-- ///////////////////////////////////////////////////////////////// --}}


    {{-- START MAIN CONTENT --}}
    @include('Layout.components.leftSideBar')
    @include('Layout.components.rightSideBar')

        {{--  accumulate current and resolved issues  --}}
  {{--    {{$indexKey1 = 0}}
    {{$indexKey2 = count($completedIssues)}}

    @foreach ($currentIssues as $indexKey1 => $currentIssue)
        {{++$indexKey1}}
    @endforeach   --}}

    {{--  @foreach ($completedIssues as $indexKey2 =>$completedIssue)
        {{++$indexKey2}}
    @endforeach   --}}

    <div class='pusher'>
        @include('Layout.components.headerBar')

      <div class="eight wide column">
          <div class="ui segment">
            <h2>
            {{--  <img src="//nas2/Data Services Shared/Picture ID/{{$student->studentID}}" height="200px" width="200px" />  --}}
              <img src="{{asset('storage/12014456.jpg') }}" height="225px" width="160px"> 

            {{--  <img src="{{asset($student->pictureFile) }}" height="225px" width="160px">  --}}
                            {{--  see link on page:   https://stackoverflow.com/questions/40989290/how-display-image-in-laravel-5-3  --}}

                <strong>
                    &nbsp &nbsp
                    {{$student->firstName}}   &nbsp
                    {{$student->middleName}}  &nbsp
                    {{$student->lastName}}    &nbsp &nbsp &nbsp &nbsp &nbsp
                    Student Id:     &nbsp
                    {{$student->studentID}}   &nbsp&nbsp&nbsp&nbsp
                </strong>
            </h2>

            {{--  <strong> &nbsp&nbsp&nbsp Concerns </strong>  --}}
            <ul class=\"social-stats\">
                <strong>
                <li><strong>  {{$currentCount}}   &nbsp&nbsp&nbsp  </strong>Unresolved Concerns</li>
                <li><strong>  {{$completedCount}}   &nbsp&nbsp&nbsp  </strong>Resolved Concerns</li>
                <li><strong>  {{$totalCount}}   &nbsp&nbsp  </strong>Total Concerns</li>
                </strong>
            </ul>
          </div>
      </div>

      {{--------  Student's Personal Information  --------}}
    <div class="ui stackable grid">
      <div class="eight wide column">
        <div class="ui segments">
            <div class="ui segment">
            <table class="ui celled table">
                <tbody>
                    <tr>
                            {{--  button Fee/Payments  --}}
                        <td>
                        <h5><strong>COMS Balance: &nbsp&nbsp$</strong>
                            {{$feesAndPayments->balance}}
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                            <button type="submit"
                                    name="COMSBalanceDetails"
                                    id='modal'
                                    class='ui primary details button'
                                    class="ui button order-button">
                                        COMS Balance Details
                            </button>
                        </h5>
                        </td>
                        <td>       
                            &nbsp&nbsp&nbsp&nbsp
                            <button type="submit"
                                   name="applyPaymentModal"
                                   id='pModal'
                                   class='ui primary details button'
                                   class="ui button order-button">
                                        Apply a Payment
                            </button>                        

                        </td>
                        <td>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                <a type="submit"
                                        name="CreateAConcern"
                                        id='modal'
                                        class='ui primary details button'
                                        class="ui button order-button"
                                         href='/concerns/{{$student->studentID}}'>
                                            Create a Concern
                                </a>

                        </td>
                    </tr>

			        <tr>
                        <td><strong>Age:&nbsp&nbsp</strong>  {{$student->age}}</td>
                        <td><strong>Gender:&nbsp&nbsp&nbsp</strong>  {{$student->gender}}</td>
                        <td><strong>Grade:&nbsp</strong>  {{$student->grade}}</td>
                    </tr>
                    <tr>
                        <td><strong>Date of Birth:&nbsp&nbsp</strong>  {{$student->dob}}</td>
                        <td><strong>Ethnicity:&nbsp&nbsp&nbsp</strong>  {{$student->ethnicity}}</td>
                        <td><strong>LISD Username:&nbsp</strong>  {{$student->username}}</td>
                    </tr>
                    <tr>
                        <td colspan="3"><strong>Email:&nbsp</strong>  {{$student->email}}</td>
                    </tr>
                    <tr>
                        <td colspan="3"><strong>Student E-Portfolio:&nbsp</strong> {{$student->skywardID}}</td>
                    </tr>
			        <tr>
                        <td><strong>ESL:&nbsp</strong>
                            @if(($student->esl) == '0')
                                No
                            @else
                                Yes
                            @endif
                        </td>

                        <td><strong>BIL:&nbsp</strong>
                            @if(($student->bil) == '0')
                                No
                            @else
                                Yes
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Special Education:&nbsp</strong>
                            @if(($student->sped) == '0')
                                No
                            @else
                                Yes
                            @endif
                        </td>
                        <td><strong> GT:&nbsp</strong>
                            @if(($student->gt) == '0')
                                No
                            @else
                                Yes
                            @endif
                        </td>
                        <td><strong> AVID:&nbsp</strong>
                            @if(($student->avid) == '0')
                                No
                            @else
                                Yes
                            @endif
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>

        {{--------  Student's Class Schedule   --------}}
    <div class="eight wide column">
        <div class="ui segments">
            <div class="ui segment">

                <tr> <td>
                <h4><strong>Class Schedule</strong>
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    {{--  Button:  Printable Class Schedule / Office Pass  --}}
                    <a  type="submit"
                        id='modal'
                        name="OfficePass"
                        class='ui primary details button order-button'
                        href='https://munisreporting/ReportServer?%2fCOMS+Reports%2fCOMSOfficePass&StudentID={{$student->studentID}}&rs:Command=Render&rs:Format=PDF'>
                            Printable Class Schedule / Office Pass
                    </a>
                </h4>
                </td></tr>

                <table class="ui celled table">
                    {{--  column headers    --}}
                    <th></th>
                    <th> Room &nbsp / &nbsp Teacher </th>
                    <th>Class</th>
                    <th>Time</th>
                    <th>Grade</th>

                    <td class="eight wide column">
                    <tbody>

                        @foreach  ($classSchedule as $schedule)
			                <tr class='{{$schedule->rowColor}}'>
                                <td><div class="ui ribbon label">
                                    <strong>Period: {{$schedule->Period}}</strong></div></td>

                                <td>
                                    @if(($schedule->RoomNumber) == '')
                                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - &nbsp
                                    @else
                                        &nbsp   {{$schedule->RoomNumber}}
                                    @endif
                                        &nbsp / &nbsp {{$schedule->TeacherName}}
                                </td>
                                <td> {{$schedule->CourseDescription}}</td>

                                <td align="center"> {{$schedule->StartTime}} &nbsp - &nbsp {{$schedule->EndTime}}</td>
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp

                                <td> &nbsp &nbsp &nbsp &nbsp &nbsp {{$schedule->Grade}} &nbsp &nbsp &nbsp &nbsp &nbsp </td>

                            </tr>
                        @endforeach

                    </tbody>
                    </td>
                </table>
            </div>
        </div>
    </div>

 </div>

        {{--------  Student's Issue/Concern History   --------}}
            <!-- Start Social Content -->
    <div class="social-content clearfix">

        <div class="ui stackable grid">

            <div class="row">
                <div class="eight wide column ">
                    <div class="ui segments">
                        <div class="ui segment">
                            <div class="ui raised padded text segments column">
                                <h4><strong> &nbsp Concerns - History</strong></h4>

                                <table class="ui unstackable celled table" id='completedIssues'>

                                    {{--  column headers    --}}
                                    <thead>
                                        <th> &nbsp&nbsp&nbsp&nbsp&nbsp&nbspConcern Date & Time</th>
                                        <th> Concern </th>
                                        <th> Resolved By</th>
                                        <th> &nbsp&nbsp&nbsp&nbspResolved Date & Time</th>
                                            {{--  dropdown headers  --}}
                                        <th class='none'>Description:</th>
                                        <th class='none'>Notes  (Entered by and the Note):</th>
                                    </thead>
                                    <tbody>
                                        @if(!empty($completedIssues))

                                            @foreach($completedIssues as $completedIssue)
                                                <tr id='{{$completedIssue->id}}'>
                                                    {{--  <td>{{date('m-d-Y H:i', strtotime($completedIssue->submittedDate))}}</td>  --}}
                                                    <td>{{$completedIssue->submittedDate->format('m-d-Y H:i')}}</td>
                                                    <td>{{$completedIssue->reportName}}</td>
                                                    <td>{{$completedIssue->completedByName}}</td>
                                                    <td align="center">
                                                    &nbsp&nbsp {{date('m-d-Y H:i', strtotime($completedIssue->completedDate))}}</td>
                                                    <td>&nbsp&nbsp{{$completedIssue->message}}</td>
                                                    <td>
                                                        @if(!empty($completedIssue->notes))
                                                        @foreach($completedIssue->notes as $note)
                                                            <li class='content'>&nbsp&nbsp{{$note->user}}
                                                                &nbsp&nbsp&nbsp&nbsp{{$note->content}}</li>
                                                        @endforeach
                                                        @endif
                                                    </td>

                                                  </tr>
                                            @endforeach
                                        @else
                                            <td> *No Prior Concerns </td>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


        {{--------  Student's Issue/Concern -  Current   --------}}
        <div class="ui segments">
        <div class="ui segment">

             <div class="ui raised padded text segments column">
                <h4><strong>&nbsp Concerns - Current</strong></h4>

                <table class="ui celled table" id='currentIssues'>
                <thead>
                    {{--  column headers    --}}
                    <th> Concern Date & Time</th>
                    <th> Concern </th>
                    <th> Posted By</th>
                </thead>
                <tbody>
                @if(!empty($currentIssues))
                    @foreach ($currentIssues as $currentIssue)
                    <tr>
                        <td>{{date('m-d-Y H:i', strtotime($currentIssue->DateSubmitted))}}</td>
                        <td>{{$currentIssue->Issue}}</td>
                        <td>{{$currentIssue->Name}}</td>
                    </tr>

                    @endforeach
                @else

                    <td> *No Unresolved Concerns </td>

                @endif
                </tbody>
                </table>

            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

    </div>
    </div>

<footer class="ui bottom fixed menu">
    @include('Layout.components.footerBar')
    </footer>

    @if (getenv('APP_ENV') === 'local')
        <script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.13'><\/script>".replace("HOST", location.hostname));
//]]></script>
    @endif

</body>

<script>
    $('#scheduleModal').modal(
        {
            observeChanges : true
        }
    );
</script>


</html>

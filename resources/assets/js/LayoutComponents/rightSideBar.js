let i = 0;
$(document).ready(function(){

    let rightSideBar = $('#mainRightSideBar');
    let userSettingsForm = $('#userSettingsForm');

    getActiveSetting();

    rightSideBar.sidebar({
        exclusive : false,
        transition: 'overlay',
        cloasable : true,
        dimPage   : true,
        scrollLock: false,
        returnScroll: false
    });
    rightSideBar.sidebar();



    $('#rightSideBarButton').on('click', function(event){
        rightSideBar.sidebar('setting', 'transistion', 'push').sidebar('toggle');
    });

    $('.userSettingsToggle').children('input').on('click', function(event){
        if(i === 1){
            return false;
        } else {
            i++;
            $('#subField').remove();
            selectedSetting = $(this).attr('id');
            let currentValue = getCurrentValue(selectedSetting);
        }
    });

    userSettingsForm.on('submit', function(event){
        event.preventDefault();
        if($('#savedMessage').length > 0){
            $('#savedMessage').remove();
        }
        let selected  = $('.userSettingsToggle input:checked').attr('id');

        saveSettings(selected);

    });

    if($('#techOrgCodeForm').length > 0){

        let techDropDown = $('#techOrgCodeForm').children('div').children('div.ui.selection.dropdown');

        techDropDown.dropdown();

        let techOrgForm = $('#techOrgCodeForm');

        $.ajax({
            url: "/tech/campuses",
            type: 'GET',
            headers: {
                api_key: 'pDvqcZf3EwnS37k5'
            },
            success: function(campuses){
                let $orgDropDown = $('#techOrgCodeForm').children('div')
                                    .children('div')
                                    .children('div.menu');
                campuses = JSON.parse(campuses);
                $.each(campuses, function(index, campus){
                    if(index = 0){
                        $orgDropDown.append("<div class='item selected' data-value='"+campus.orgCode+"'>"+campus.campusName+"</div>");
                    } else {
                        $orgDropDown.append("<div class='item' data-value='"+campus.orgCode+"'>"+campus.campusName+"</div>");
                    }
                });
                techDropDown.dropdown('refresh');
            },
            error: function(error){
                console.log(error);
            }
        });

        techOrgForm.on('submit', function(event){
            event.preventDefault();
            let orgCode = techDropDown.dropdown('get value');
            $.ajax({
                url: "/tech/save",
                type: 'POST',
                data: {
                    orgCode: orgCode
                },
                success: function(results){
                    console.log(results);
                    if(results){
                        window.location.reload();
                    } else {
                        console.log("Failed");
                    }
                }
            })
        })
    }

});


function showLoadingSettingButton()
{
    $('#userSettingsSubmit').hide();
    $('#userSettingsLoading').show();
}

function showSaveSettingButton()
{
    $('#userSettingsSubmit').show();
    $('#userSettingsLoading').hide();
}

function saveSettings(option)
{
    const multiValueSetting = ['alpha', 'grade'];
    const dropDownValueSetting = ['special', 'activity', 'counselor', 'otherUser'];
    showLoadingSettingButton();
    if(option == 'allStudents'){
        data = {type : 'allStudents'};
    } else if($.inArray(option, multiValueSetting) > -1){
        data = {
            type : option,
            min: $('#min').val(),
            max: $('#max').val()
        }
    } else if($.inArray(option, dropDownValueSetting) > -1){
        data = {
            type: option
        };
        if(option != 'otherUser') {
          data.selected = $('#userSettingDropDown option:selected').val();

        } else {
            data.selected = $('#userSettingDropDown option:selected').attr('id');
        }
    } else {
        throw ("UnKnown Setting is selected");
    }

    console.log(data);

    var isReady = false;

    if(data.type.toLowerCase() == 'alpha'){
        if(!parseInt(data.min) && !parseInt(data.max) && data.min != 0 && data.max != 0){
            isReady = true;
        }
    } else if(data.type.toLowerCase() == 'grade'){
        if(parseInt(data.min) && parseInt(data.max)){
            isReady = true;
        }
    } else {
        isReady = true;
    }

    if(isReady){
        $.ajax({
            url: "/settings/save",
            type: 'POST',
            data : data,
            success: function(results){
                // console.log(results);
                showSaveSettingButton();
                $('#userSettingsSubmit').parent().append("<p id='savedMessage' style='color: green; display: inline-block'>Saved!</p>");
                //Refrshing Lists for Site Dashboard
                if($('#collectiveLists').length > 0){
                toDoList();
                }
                if($('#formsTable').length > 0){
                let reportID = $('#formsTable').parent().parent().children('h2').attr('id');
                getData(reportID);
                }
            }
        })
    }
    else {
        showSaveSettingButton();
        $('#userSettingsSubmit').parent().append("<p id='savedMessage' style='color: red; display: inline-block'>ERROR! check input</p>");
    }
}

function getActiveSetting()
{
    $.ajax({
        url: '/settings/active',
        type: 'GET',
        success: function(results){
            if(results)
            console.log(results);
            try{
            activeSetting = JSON.parse(results);
            } catch(e){
                activeSetting = {
                    name: 'none'
                };
            }
            if(activeSetting.name == 'All Students'){
                $('#allStudents').prop('checked', true);
            } else if(activeSetting.name == 'Alpha'){
                // TODO Filter Setting
                $('#alpha').prop('checked', true);
            } else if(activeSetting.name == 'Grade'){
                // TODO Filter Setting
                $('#grade').prop('checked', true);
            } else if(activeSetting.name == 'Counselor'){
                // TODO Filter Setting
                $('#counselor').prop('checked', true);
            } else if(activeSetting.name == 'Special'){
                // TODO Filter Setting
                $('#special').prop('checked', true);
            } else if(activeSetting.name == 'Activity'){
                // TODO Filter Setting
                $('#activity').prop('checked', true);
            } else if(activeSetting.name == 'Other User'){
                // TODO Filter Setting
                $('#otherUser').prop('checked', true);
            }
            getCurrentValue(activeSetting.name.toLowerCase());
        },
        error: function(error){
            console.log(error.serialize());
        }
    })
}

function showMinMax(type, current)
{
    let div = '';
   if(type == 'alpha'){
        div = $('#alpha').parent().parent().parent();
   } else if(type == 'grade'){
    div = $('#grade').parent().parent().parent();
   } else {
       throw "no Min/Max Setting was selected";
   }
   try {
     div.append("" +
         "<div class='fields' id='subField'>" +
         "<div class='field'>" +
         "<div class='ui labeled input'id='minLabeledInput'>" +
         "<label class='ui label'>Min</label>" +
         "<input type='text' class='minInput' id='min' value='" +
         current.value1 + "'>" +
         "</div>" +
         "<div class='ui labeled input' id='maxLabeledInput'>" +
         "<label class='ui label minSettingInput'>Max</label>" +
         "<input type='text' class='maxInput' id='max' value='" +
         current.value2 + "'>" +
         "</div>" +
         "</div>" +
         "</div>");
   } catch(e){}
i = 0;
showSaveSettingButton();
}

function showDropDown(type, current)
{
    //console.log(type);
    let div = '';
    if(type == 'special'){
        div = $('#special').parent().parent().parent();
    } else if(type == 'activity'){
        div = $('#activity').parent().parent().parent();
    } else if(type == 'counselor'){
        div = $('#counselor').parent().parent().parent();
    } else if(type == 'otherUser'){
        div=$('#otherUser').parent().parent().parent();
    }
    $.ajax({
        url: '/settings/value/'+type,
        type: 'GET',
        success: function(results){
            let activeSetting = '';
            if(results.length > 0){
                activeSetting = JSON.parse(results);
            }
            try {
              div.append("" +
                  "<div class='field' id='subField'>" +
                  "<select class='ui dropdown' id='userSettingDropDown'>" +
                  "</select>" +
                  "</div>");


            $.each(current, function(index, value){
                if(value.name == activeSetting.value1){
                    div.children('div').children('select').append(""+
                    "<option selected id='"+value.id+"' class='userDropDownOption'>"+value.name+"</option>"
                    );
                } else {
                    div.children('div').children('select').append(""+
                    "<option id='"+value.id+"' class='userDropDownOption'>"+value.name+"</option>"
                    );
                }
                i = 0;
                showSaveSettingButton();
            })
            } catch(e){
              showSaveSettingButton();
            }
        },
        error: function(error){
            console.log(error);
        }
    })

}

/**
 * Function: getCurrentValue()
 * @param {string} settingValue
 * Description: Gets the Selected
 */
function getCurrentValue(settingValue)
{
    showLoadingSettingButton();
    if($('#savedMessage').length > 0){
        $('#savedMessage').remove();
    }
    if(settingValue == 'all students'){
        settingValue = 'allStudents';
    }
    if(settingValue == 'allStudents'){
        showSaveSettingButton();
        i = 0;
    }
    const multiValueSetting = ['alpha', 'grade'];
    $.ajax({
        url: '/settings/values/'+settingValue,
        type: 'GET',
        success: function(results){
            if(settingValue != 'allStudents'){
                results = JSON.parse(results);
            }
            //console.log(results);
            if($.inArray(settingValue.toLowerCase(), multiValueSetting) >= 0){
                showMinMax(settingValue, results);
            } else if(settingValue != 'allStudents') {
                showDropDown(settingValue, results);
            }
        },
        error: function(error){
            // console.log(error.message);
        }
    })
}

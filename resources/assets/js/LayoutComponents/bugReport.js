// TODO Livetime Reporting System for COMS Issues


let bugReportIcon = $('#bugReportButton');
let bugReportModal = $('#bugReportingModal');

bugReportModal.modal({
    closeable   :   true
});

bugReportModal.modal();

bugReportIcon.on('click', function(event){
    event.preventDefault();
    console.log("I'm here");
    bugReportModal.modal('show');
})

$('#bugReportForm').submit(function(event){
    event.preventDefault();
    let subject = $('#bugSubject').val();
    let description = $('#bugDescription').val();
    $.ajax({
        url: 'bug/report',
        type: 'POST',
        data: {
            subject : subject,
            description : description
        },
        success: function(results){
            alert(results);
        },
        error: function(error){
            console.log(error);
        }
    })
})

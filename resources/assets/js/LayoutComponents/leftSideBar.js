$(document).ready(function(){
    
    var leftSideBar = $('#mainLeftSideBar');
        
    leftSideBar.sidebar({
        exclusive : false,
        transition: 'overlay',
        cloasable : true,
        dimPage   : true,
        scrollLock: false,
        returnScroll: false
    });
    leftSideBar.sidebar(); 


    $('#leftSideBarButton').on('click', function(event){
        leftSideBar.sidebar('setting', 'transistion', 'push').sidebar('toggle');
    })
    
})
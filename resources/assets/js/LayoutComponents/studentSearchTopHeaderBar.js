$(document).ready(function () {

    var studentSearch = $('#headerBarStudentSearch')

    var max = 0;

    if (screen.height <= 768) {
        max = 2;
    } else {
        max = 5
    }

    studentSearch.search();

    studentSearch.search({
        type: 'category',
        fullTextSearch: true,
        cache: false,
        minCharacters: 3,
        maxResults: 100,
        apiSettings: {
            url: '/search/students?key={query}',
            onResponse: function (studentResponse) {
                var key = $("#headerBarStudentSearch div.ui.icon.input input.prompt").val();
                var
                    response = {
                        results: {}
                    };

                // translate GitHub API response to work with search
                $.each(studentResponse, function (index, student) {
                    var schoolName = student.campusName || 'Unknown';
                    if (index >= max) {
                        return false;
                    }
                    // create new categoryName category
                    if (response.results[schoolName] === undefined) {
                        response.results[schoolName] = {
                            name: schoolName,
                            results: []
                        };
                    }

                    // add result to category
                    response.results[schoolName].results.push({
                        title: student.displayName,
                        description: 'ID: ' + student.studentID + ' | Grade: ' + student.grade,
                        image : '/img/blank-profile.jpg',
                        url: '/profile/student/' + student.studentID
                    });

                });
                return response;
            }
        },
    })

    $('#headerBarStudentSearch').children('div').children('input').on('keypress', function(event){
        try{
            if(event.originalEvent.key == 'Enter' || event.original.key == 'Return'){
                let key = $("#headerBarStudentSearch div.ui.icon.input input.prompt").val();
                let results = $('.results.transition.visible').children('div.category')
                console.log(results.length);
                if(results.length > 1){
                    window.location.href = "/search?key="+key;
                } else {
                    if(results.length == 1){
                        results = $('.results.transition.visible').children('div.category').children('a.result');
                        console.log(results.length);
                        if(results.length == 1){
                            let result = $('.results.transition.visible').children('div.category').children('a.result').children('div.content').children('div.description');
                            result = result.text().split(' | ')
                            result = result[0].split("ID: ");
                            window.location.href = "/profile/student/"+result[1].trim()
                        } else {
                            window.location.href = "/search?key="+key;
                        }
                    } else {
                        window.location.href = "/search?key="+key;
                    }
                }
            }
        } catch (e){}
    })

})

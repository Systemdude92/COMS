let formsTable = $('#formsTable');
let reportID = formsTable.parent().children('h2').attr('id');
let listFilterDropDown = $('#listFilter');
listFilterDropDown.dropdown({
    onChange: function(value, text, $choice){
        getData(reportID);
    },
    useLabels: false
})

let statusFilter = $('#statusFilter');
statusFilter.dropdown({
    onChange:function(value, text, $choice){
        getData(reportID);
    },
    useLabels: false
})

$('#errorHeader').parent().children('i').on('click',function(event){
    hideErrors();
})

getData(reportID); //Initiates DataTables

let globalForm = $('#globalListForm');

globalForm.on('submit', function(event){
    event.preventDefault();
    let selectedForms = $("input[name='formSelection']:checked");
    let globalNote = $('#globalNotes').val();
    let status = null;
    let formIDs = [];
    if(globalNote.length > 0){
        $.each(selectedForms, function(index, input){
            console.log(input.value);
            formIDs.push(input.value);
        })
        if($('#globalStatus').length > 0){
            status = $("input[name='globalStatus']:checked");
        }
        data = {
            formID: formIDs,
            note: globalNote,
            status: status
        }
        postData(data);

    } else {
       displayError({
           header: "ERROR",
           message: 'Notes field is empty but is Required'});
    }
})


function modifyBulkPassButton()
{
    let selectedForms = $("input[name='formSelection']:checked").parent().parent().parent().children('td.formTableStudentID');
    let studentIDs = [];
    let href = 'https://munisreporting/ReportServer?%2fCOMS+Reports%2fCOMS_BULK';
    $("input[name='formSelection']:checked").parent().parent().parent().children('td.formTableStudentID').each(function(index, value){
        let id = $(value);
        if($.inArray(id.text(), studentIDs) == -1){
            href += '&StudentIDs='+id.text()
            studentIDs.push(id.text());
        }

    });
    href += '&rs:Command=Render&rs:Format=PDF';
    $('#bulkOfficePassButton').prop('href', href);
    console.log(href);
}

function postData(data)
{
    $.ajax({
        url: '/form/note',
        type: 'POST',
        data: data,
        success: function(results){
            let success = JSON.parse(results);
            let errorMessage = {header: 'ERROR!', message: ''};
            let successMessage ={header: 'SUCCESS!', message: ''};
            $.each(success, function(index, value){
                if(value.success == 'true'){
                    successMessage.message += value.message +"<br/>";
                } else {
                    errorMessage.message += value.message +"<br/>";
                }
            })
            if(errorMessage.message.length > 0){
                displayError(errorMessage);
            }
            if(successMessage.message.length > 0){
                displaySuccess(successMessage);
            }
            $('#globalNotes').val('');
            deSelectSelected();
            $('#globalNotes').parent().parent().parent().hide();
            let reportID = $('#mainContentContainter').children('h2').attr('id');
            getData(reportID);
        },
        error: function(error){
            displayError({
                header: 'Error',
                message: 'Error has Occured when trying to Add Note. Please Try again'
            })
        }
    })
}

function deSelectSelected()
{
    let selected = $("input[name='formSelection']");
    $.each(selected, function(index, value){
        value.checked = false;
    })
}

$(document).on('change', "input[name='formSelection']", function(event){
    let selected = $("input[name='formSelection']:checked");
    if(selected.length > 0){
        // Make Global Boxes appear
        $('#globalListForm').show();
        $('#bulkOfficePassButton').show();
        modifyBulkPassButton();
    } else {
        //Make Global Boxes disappear
        $('#globalListForm').hide();
        $('#bulkOfficePassButton').hide();
    }
})

function addNoteBox(formID)
{
    $('#addNotes'+formID.toString()).show();
}

/**
 * filters out data if there is any filters selected
 * @param {JSON} data
 */
function filterData(data)
{
    let listFilterValues = $('#listFilter').dropdown('get value');
    let statusFilterValues = $('#statusFilter').dropdown('get value');
    if(listFilterValues.length > 0){
        let newData = [];
        let keys = Object.keys(data);
        let dataLength = keys.length;
        for(i=0; i<dataLength; i++){
            if(dataLength > 0){
                if($.inArray(data[i].reportInfo.id, listFilterValues) != -1){
                    newData.push(data[i]);
                }
            }
        }
        data = newData;
    }
    if(statusFilterValues.length > 0){
        let newData = [];
        let keys = Object.keys(data);
        let dataLength = keys.length;
        for(i=0; i<dataLength; i++){
            if($.inArray(data[i].status.name, statusFilterValues) != -1){
                newData.push(data[i]);
            }
        }
        data = newData;
    }
    return data;

}

function getData(reportID)
{
    let formsTable = $('#formsTable');
    $.ajax({
        url: '/lists/get/' + reportID,
        type: 'GET',
        success: function (results) {
            data = JSON.parse(results);
            data = filterData(data);
            if(formsTable.attr('data-tableType') == 'feesTable'){
                buildFeesTable(data);
            } else if (formsTable.attr('data-tableType') == 'datesTable'){
                buildDatesTable(data);
            } else {
                buildFormsTable(data);
            }
        },
        error: function(error){
            displayError({
                header: 'AJAX Error',
                message: 'AJAX Failed because: '+error.message
            });
        }
    })
}

function hideMessages()
{
    $('#errorHeader').parent().hide();
    $('#successHeader').parent().hide();
}

function displaySuccess(success)
{
    $('#successHeader').html(success.header);
    $('#successMessage').html(success.message);
    $('#successMessage').parent().show();
    setTimeout(function(){
        hideMessages();
    }, 10000);
}

/**
 *
 * @param {JSON} errors
 */
function displayError(errors)
{
    $('#errorHeader').html(errors.header);
    $('#errorMessage').html(errors.message);
    $('#errorMessage').parent().show();
    setTimeout(function(){
        hideMessages();
    }, 10000);
}

$(document).on('click', 'button[id*="addNoteButton"]', function(event){
    event.preventDefault();
    const formID = $(this).attr('id').substring(13);
    const parent = $(this).parent();
    axios.all([getFormInfo(formID), getFormStatus(formID), getPaymentTypes()])
        .then(axios.spread(function(info, statusInfo, paymentTypes){
            let formInfo = info.data;
            let statuses = statusInfo.data;
            paymentTypes = paymentTypes.data;
            let newNoteSection = `
            <form method='POST' name='addNoteToForm' id='noteForm${formID}'>
            <input type='hidden' value='${formID}' id='formID'/>`
            if(formInfo.reportType == 'fee'){
                newNoteSection = addPaymentSection(formID, newNoteSection, paymentTypes);
            }
            newNoteSection += `
            <div class='field' id='statusField'>
            <label class='ui header'>Change Status: </label>`
            $.each(statuses, (index, status) => {
                let checked = '';
                if(status.active){
                    checked = "checked";
                }
                newNoteSection += `

                        <div id='checkBoxDiv${formID}' class='ui toggle checkbox'>
                            <input id='${status.name}${formID}' ${checked} name='Status' type='radio' tabindex='0' class='hidden' value='${status.id}'>
                            <label for='${status.name}${formID}' >${status.name}</label>
                        </div>

                `
            });
            newNoteSection += `</div>
            <div class='ui divider'></div>
            <div class='required field'>
                <h3 class='ui header'>Add Note <span>(Required)</span></h3>
                <textarea id='note' name='notes' style='margin: 0; width: 100%; height:157px;'></textarea>
            </div>`;

            newNoteSection += `
            <button class='ui positive button' id='submitNoteFor${formID}' type='submit'>Submit</button>
            <button class='ui negative button' id='cancelButtonFor${formID}' type='button'>Cancel</button>
        </form>`
        parent.empty();
        parent.append(newNoteSection);
        initInputElements(formID);
        }));
})

function initInputElements(formID)
{

    if($('#noteForm'+formID).children('#paymentTypeField').length > 0){
        $('#noteForm'+formID).children('#paymentTypeField').children('select').dropdown();
    }

    if($('#paymentSatisfied'+formID).length > 0){

        let paymentSatisfied = $('#paymentSatisfied'+formID).children('div.ui.checkbox');

        paymentSatisfied.checkbox({
            onChange: function(){
                let checkbox = $(this).parent();
                if(checkbox.checkbox('is checked')){
                    $('#paymentAmount').prop('disabled', 'disabled');
                    $('#Complete'+formID).prop('checked', 'checked');
                    $.ajax({
                        url: '/form/amount/'+formID,
                        type: 'GET',
                        success: function(amount){
                            $('#paymentAmount').val(parseFloat(amount).toFixed(2));
                        },
                        error: function(error){
                            console.log(error);
                        }
                    })
                } else {
                    $('#paymentAmount').val('');
                    $('#paymentAmount').removeAttr('disabled');
                    $('#Pending'+formID).prop('checked', 'checked');
                }
            }
        })

    }
}

function addPaymentSection(formID, newNoteSection, paymentTypes)
{
    newNoteSection += `

    <label class='ui header'>Payment:</label>
    <div class='field' id='paymentSatisfied${formID}'>
    <label >Satisfy Payment: </label>
        <div class='ui checkbox'>
            <input type='checkbox'/>
            <label></label>
        </div>
    </div>
    <div class='field' id='paymentTypeField'>
        <label >Payment Type:</label>
        <select class='ui dropdown'>
            <option value=''>Choose Type</option>`
            $.each(paymentTypes, function(index, paymentType){
                newNoteSection += `<option value='${paymentType.id}'>${paymentType.name}</option>`;
            })

        newNoteSection += `
        </select>
    </div>
    <div class='field' id='paymentField'>
        <label >Payment Amount: </label>
        <div class='ui labeled input'>
            <label for='payment' class='ui label'>$</label>
            <input type='text' placeholder='Amount' id='paymentAmount' />
        </div>
        <label class='ui header' style='color: RED'>(Payment amount is applied to Student's COMS Balance ONLY)</label>
    </div>
    <div class='ui divider'></div>
    `
    return newNoteSection;
}

function getFormStatus(formID)
{
    return axios.get('/form/statuses/'+formID);
}

function getFormInfo(formID)
{
    return axios.get('/form/'+formID);
}

function getPaymentTypes()
{
    return axios.get('/form/payment/types');
}

$(document).on('click', 'button[id*=submitNoteFor]', function(event){
    // event.preventDefault();
    let formID = $(this).parent().children('input[id=formID]').val();
    let readyToSubmit = true;
    let note = $(this).parent().children('div').children('textarea[id=note]').val();
    let status = $(this).parent().find('input[name=Status]:checked').val();
    let data = {
        "formID" : formID,
        "note": note,
        "status": status
    };
    if($('#paymentField').length > 0){
        let payment =  $('#paymentField').children('div').children('input').val();
        let paymentType = $('#paymentTypeField').children('div').children('select').dropdown('get value');
        if(payment.length == 0){
            payment = 0;
        }
        if(!isNaN(payment)){
            if(payment.length <= 0){
                payment = '0.00';
            }
            payment = parseFloat(payment).toFixed(4);
            /* if($('#paymentSatisfied').children('div').children('input').prop('checked')){
                payment = 'satisfied';
            } */
            data.payment = payment;
            if(payment == 0){
                paymentType = 0;
            }
            if(isNaN(paymentType) || (paymentType.length == 0 && payment > 0) || (payment == 'satisfied' && paymentType.length == 0)){
                readyToSubmit = false;
                displayError(
                    {
                        header: 'ERROR',
                        message: 'Payment Type is required'
                    }
                )
            } else {
                data.paymentType = paymentType;
            }
        }  else {
            readyToSubmit = false;
            displayError(
                {
                    header: 'ERROR',
                    message: 'Payment Value is not in the form of a Number'
                }
            )
        }
    }

    if(note.length <= 0 || status.length <= 0 || formID.length <= 0){
        readyToSubmit = false;
        if(note.length <= 0){
            displayError({
                header: 'ERROR',
                message: "Missing Note Content"
            });
        }
        if(status.length <= 0){
            displayError({
                header: 'INTERNAL ERROR',
                message: 'No Status has been Selected'
            })
        }
        if(formID.length <= 0){
            display({
                header: 'Internal Error',
                message: "Unable to retrieve Form ID"
            })
        }
    }

    if(readyToSubmit){
        console.log(data);
        postData(data);
    } else {
        console.log('Not Ready to Submit new Note');
    }
})

$(document).on('click', 'button[id*=cancelButtonFor]', function() {
    let parent = $(this).parent().parent();
    let formID = $(this).attr('id')
    formID = formID.substring(15);
    parent.empty();
    parent.append(`<button class='ui primary button' id='addNoteButton${formID}'>Add Note</button>`)
})

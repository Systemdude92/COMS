function buildDatesTable(data) {
    console.log(data);
    formsTable.DataTable({
        paging: true,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'inline'
            },
            orthogonal: 'responsive'
        },
        processing: true,
        data: data,
        destroy: true,
        dom: "l<'formsTableButtons'Bf>rtip",
        rowId: 'DT_RowId',
        columns: [
            { 'data': 'empty',
            'orderable': false},
            { 'data': 'selectBox',
                'render': function(data, type, row, meta){
                    return `<div class='ui toggle checkbox'><input name='formSelection' type='checkbox' value='${row.DT_RowId}'><label></label></div>`;
                },
                'orderable': false
            },
            { 'data': 'studentID' },
            { 'data': 'studentInfo',
                'render': function(data, type, row, meta){
                    return "<a href='/profile/student/"+data.studentID+"' id='"+data.studentID+"'>"+data.studentLastName+", "+data.studentFirstName+"</a>";
                }
            },
            { 'data': 'reportInfo',
                'render': function(data, type, row, meta){
                    return "<a href='/lists/"+data['id']+"' id='"+data['id']+"'>"+data['name']+"</a>";
                }
            },
            { 'data': 'startDate' },
            { 'data': 'endDate' },
            { 'data': 'submittedDate'},
            { 'data': 'submittedUser' },

            { 'data': 'status',
                'render': function(status, type, row, meta){

                    status = `<div class='ui ${status.color} horizontal label'>${status.name}</div>`
                    return status;
                }
            },
            { 'data': 'message',
                'render': function(description, type, row, meta){
                    let message = `
                    <h3>Description: </h3>
                    <p>${description}</p>
                    `
                    return message;
                }
            },
            { 'data': 'notes',
                'render': function(data, type, row, meta){
                    comments = `<div id='noteSection${row.DT_RowId.toString()}'>`;
                    if(data.length > 0){
                        comments += `<div class='ui comments'><div class='ui divider'></div>`
                        $.each(data, function(index, note){
                            if(index < 5){
                            comments += `
                                <div class='comment'>
                                    <div class='content'>
                                        <div class='author'>${note.user}</div>
                                    </div>
                                    <div class='metadata'>
                                        <span class='date'>${note.submittedDate}</span>
                                    </div>
                                    <div class='text'>
                                        ${note.content}
                                    </div>
                                </div>
                                <div class='ui divider'></div>
                            `
                            } else if(index == 5) {
                                comments += `<div class='ui header'>... More ...</div>`
                                //TODO See next 5 Comments
                            } else {
                                return false;
                            }
                        })
                        comments += `</div>`
                    } else {
                        comments += "No Notes have been Recorded";
                    }
                    comments += `</div>`;
                    return comments;
                }
            },
            {'data': 'writeAccess',
                'render': function(data, type, row, meta, ){
                    if(data == true && row.status.noFurtherActionRequired == 0){
                        return `<button class='ui primary button addNoteButton' id='addNoteButton${row.DT_RowId}'>Add Note</button>`
                    } else {
                        return '';
                    }
                }
            }
        ],
        buttons: [
            'copyHtml5',
            'csvHtml5',
            'excelHtml5',
            'pdfHtml5'
        ],
        language: {
            thousands: ",",
            aria: {
                sortAscending: "Used to sort column ascending",
                sortDesending: "Used to sort column descending",
                paginate: {
                    first: "Return to First",
                    last: "Return to Last",
                    next: "Go to Next",
                    previous: "Return to previous",
                }
            },
            searchPlaceholder: "Student Name OR ID",
            zeroRecords: "There are no forms that fit your Settings Configuration. Please try to expand your search for more results",
            infoEmpty: "No Forms to List",
            processing: "Loading Forms",
            decimal: "."
        },
        drawCallback: function(setting){
            let selectors = $('input[name=formSelection]');
            $.each(selectors, function(index, select){
                let selection = $(select);
                selection.prop('checked', false);
                $('#bulkOfficePassButton').hide();
            })
        }
    })
}

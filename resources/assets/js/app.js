
$(document).ready(function(){

    let lockTimer = 0;

    setInterval(function(){
        if(window.location.pathname != '/lock'){
            if(lockTimer >= 30){
                window.location.href = window.location.protocol+'//'+window.location.host+"/lock";
            } else {
                lockTimer++;
            }
        }
    }, 60000);

});

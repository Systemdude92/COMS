$(document).ready(function(){
    $('#detentionCheckBox').checkbox();

    reasons = $('.ui.slider.checkbox.studentForm')
    reasons.on('click', function(event){
        event.preventDefault();
        console.log("I'm here");
        if($('#feeDiv').length > 0){
            $('#feeDiv').remove();
        }
        if($('#dateDivs'.length > 0)){
            $('#dateDivs').remove();
        }
        $(this).children('input').prop('checked', 'checked');
        let reportID = $(this).children('input').val();
        let report = $(this);
        $.ajax({
            url: "/report/"+reportID,
            type: 'GET',
            success: function(results){
                if(results == 'fee'){
                    addFeeBlock(report);
                } else if(results == 'dates'){
                    addDateBlocks(report);
                }
            },
            error: function(error){
                console.log(error);
            }
        })
        return true;
    })

    let form = $('#studentConcernsForm');

    form.on('submit', function(event){
        event.preventDefault();
        showLoadingButton();

        let studentID = $('#studentID').val();
        let reason = $('input[name=reason]:checked').val();
        let description = $('#description').val();
        console.log(description);
        let data = {
            reason : reason,
            description : description,
        }


        if($('#feeDiv').length > 0){
            let fee = $('#fee').val();
            fee = formatFee(fee);
            data.fee = fee;
        }

        if($('#dateDivs').length > 0){
            let startDate =  $('#startDate').calendar('get date');
            let endDate = $('#endDate').calendar('get date');
            data.startDate = formatDate(startDate);
            data.endDate = formatDate(endDate);
        }

        // console.log(studentID);
        // console.log(data);

        try {
            $.ajax({
                'url': '/concerns/'+studentID,
                'type': 'POST',
                'data': data,
                'success': function(results){
                    console.log(results);
                    if(results == 'true'){
                        window.location.href = "/concerns/success";
                    } else {
                        showSubmitButton();
                    }
                }
            });
    } catch(exception) {
        console.log(data);
        console.log(exception);
        showSubmitButton();
    }

    })

})

function formatFee(fee)
{
    if(fee.indexOf('.') > -1){
        fee = fee.split('.');
        dollars = fee[0];
        cents = fee[1];
        if(cents.length < 2){
            cents = cents+"00";
        };
        if(cents.length > 2){
            cents = cents.substr(0,2);
        };
        fee = dollars+"."+cents;
    } else {
        fee = fee+".00";
    }
    return fee;
}

function formatDate(date)
{
    year = date.getFullYear();
    month = date.getMonth();
    day = date.getDay();
    return year+"/"+month+"/"+day
}

function addDateBlocks(input)
{
    input.parent().append(""+
    "<div class='fields' id='dateDivs'>"+
        "<div class='field'>"+
            "<div class='ui calendar' id='startDate'>"+
                "<div class='ui labeled input'>"+
                    "<label class='ui label'>Start Date</label>"+
                    "<input type='text' placeholder='Start Date'>"+
                "</div>"+
            "</div>"+
        "</div>"+
        "<div class='field'>"+
        "<div class='ui calendar' id='endDate'>"+
            "<div class='ui labeled input'>"+
                    "<label class='ui label'>End Date</label>"+
                    "<input type='text' placeholder='End Date'>"+
                "</div>"+
            "</div>"+
        "</div>"+
    "</div>"+
    "")

    $('#startDate').calendar({
        type: 'date',
        endCalendar: $('#endDate'),
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                 day = date.getDate() + '';
                if (day.length < 2) {
                    day = '0' + day;
                }
                let month = (date.getMonth() + 1) + '';
                if (month.length < 2) {
                    month = '0' + month;
                }
                let year = date.getFullYear();
                return year + '/' + month + '/' + day;
            }
        }
    });

    $('#endDate').calendar({
        type: 'date',
        startCalendar: $('#startDate'),
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                let day = date.getDate() + '';
                if (day.length < 2) {
                    day = '0' + day;
                }
                let month = (date.getMonth() + 1) + '';
                if (month.length < 2) {
                    month = '0' + month;
                }
                let year = date.getFullYear();
                return year + '/' + month + '/' + day;
            }
        }
    });
}

function addFeeBlock(input)
{
    input.parent().append(""+
    "<div class='ui labeled input' id='feeDiv'>"+
        "<label class='ui label'>$</label>"+
        "<input type='text' placeholder='Amount' id='fee'>"+
    "</div>"+
    "")

    $('#fee').blur(function(){
        if(!$.isNumeric($(this).val())){
            $(this).val("");
        }
    })
}

function showLoadingButton()
{
    let loading = $('#studentFormLoadingButton');
    let submit = $('#studentFormSubmitButton');
    let cancel = $('#studentFormCancelButton');
    cancel.hide();
    submit.hide();
    loading.show();
}

function showSubmitButton()
{
    let loading = $('#studentFormLoadingButton');
    let submit = $('#studentFormSubmitButton');
    let cancel = $('#studentFormCancelButton');
    loading.hide();
    cancel.show();
    submit.show();
}

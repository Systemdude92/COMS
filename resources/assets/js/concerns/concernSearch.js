$(document).ready(function () {
    studentSearchForForm = $('#studentFormSearch');

    studentFormInput = $('#studentFormInput')

    studentSearchForForm.search();


    studentSearchForForm.search({
        //type: 'category',
        fullTextSearch: true,
        cache: false,
        minCharacter: 3,
        maxResults: 100,
        apiSettings: {
            url: '/search/students?key={query}',
            onResponse: function (studentResults) {
                var response = {
                    results: []
                };
                var key = $('#studentFormInput').val();
                $.each(studentResults, function (index, value) {
                    var school = value.studentID || 'unknown';
                    response.results.push({
                        title: value.displayName,
                        description: 'ID: ' + value.studentID + ' | Grade: ' + value.grade,
                        url: "concerns/"+value.studentID
                    });
                });
                return response;
            },

        },
        onSelect: function (result, response) {
            console.log(result);
        },
        selectFirstResult: true
    })

    $('.message .close')
    .on('click', function() {
      $(this)
        .closest('.message')
        .transition('fade');
    });




})

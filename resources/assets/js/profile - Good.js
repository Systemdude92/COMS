/*
 *
 *      Velma Vessels
 *
 */

 /* Formatting function for row details - modify as you need */
 function format ( Notes ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>'+Notes.content+'</td>'+
        '</tr>'+
    '</table>';
}


$(document).ready(function () {


    let modalButton = $('#modal');
    modalButton.on('click', function(event){

           event.preventDefault();

        $('#feeAndPaymentModal').modal('show');
    });

// //

    let modalButton1 = $('#pModal');

    modalButton1.on('click', function(event){
        event.preventDefault();
        $('#paymentModal').modal('show');

        // newPaymentSection = `
        //     <option value='${paymentType.id}'>${paymentType.name}</option>`;
        // return newPaymentSection;

    });


// ////


    try{
        let table = $('#completedIssues').DataTable( {
            order: [[0, 'dec']],
            responsive: {
                details: true
            },
            searching: false,
            ordering: false,
            paging: false,
            bInfo: false

        });
    }
    catch(e){
        console.log("completedIssues Error");
    }

    try{
        let table2 = $('#currentIssues').DataTable({
            order: [[0, 'dec']],
            responsive: {
                details: true
            },
            searching: false,
            ordering: false,
            paging: false,
            bInfo: false
        })
    } catch(e){
        console.log(e);
        console.log("currentIssue Error");
    }
})   // end document ready



function getOptions() {
    var pType = document.getElementById("paymentType");
}


//let pAmount = document.getElementById("inputAmount").val();


// /////////////////

// let notesTable = $('#notesTable');
// let notesID = notesTable.parent().children('h2').attr('id');
// let listFilterDropDown = $('#listFilter');
// listFilterDropDown.dropdown({
//     onChange: function(value, text, $choice){
//         getData(notesID);
//     },
//     useLabels: false
// })

// let statusFilter = $('#statusFilter');

// statusFilter.dropdown({
//     onChange:function(value, text, $choice){
//         getData(notesID);
//     },
//     useLabels: false
// })

$('#errorHeader').parent().children('i').on('click',function(event){
    hideErrors();
})



// getData(notesID); //Initiates DataTables

//let globalForm = $('#globalListForm');



$(document).on('click', 'button[id*=submitPayment]', function(event){
    let pAmount = document.getElementById("inputAmount").val();

//  globalForm.on('submit', function(event){
//      event.preventDefault();
//     let selectedForms = $("input[name='formSelection']:checked");
//     let globalNote = $('#globalNotes').val();
//     let status = null;
//     let paymentIDs = [];
//     if(globalNote.length > 0){
//         $.each(selectedForms, function(index, input){
//             console.log(input.value);
//             paymentIDs.push(input.value);
//         })
//         if($('#globalStatus').length > 0){
//             status = $("input[name='globalStatus']:checked");
//         }

         data = {
             paymentID: paymentIDs,
             schoolYear: student,
             note: globalNote,
             status: status
         }
         postData(data);

//     } else {
//        displayError({
//            header: "ERROR",
//            message: 'Notes field is empty but is Required'});
//     }
 })

 function postData(data)
 {
     $.ajax({
         url: '/post/payment',
         type: 'POST',
         data: data,

        //  success: function(results){
        //      let success = JSON.parse(results);
        //      let errorMessage = {header: 'ERROR!', message: ''};
        //      let successMessage ={header: 'SUCCESS!', message: ''};
        //      $.each(success, function(index, value){
        //          if(value.success == 'true'){
        //              successMessage.message += value.message +"<br/>";
        //          } else {
        //              errorMessage.message += value.message +"<br/>";
        //          }
        //      })


            //  if(errorMessage.message.length > 0){
            //      displayError(errorMessage);
            //  }
            //  if(successMessage.message.length > 0){
            //      displaySuccess(successMessage);
            //  }


        //      $('#globalNotes').val('');
        //      deSelectSelected();
        //      $('#globalNotes').parent().parent().parent().hide();

         
        //      let reportID = $('#mainContentContainter').children('h2').attr('id');
        //      getData(reportID);
        //  },
        //  error: function(error){
        //      displayError({
        //          header: 'Error',
        //          message: 'Error has Occured when trying to Add Note. Please Try again'
        //      })
        //  }
     }
    )
 }


$('#theNotes').dataTable({
    paging: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.childRow,
            type: 'inline'
        },
        orthogonal: 'responsive'
    },
    processing: true,
    order: [
        [1, 'asc']
    ],
    destroy: true,
    dom: "l<'formsTableButtons'Bf>rtip",
    rowId: 'DT_RowId',
    buttons: [
        'copyHtml5',
        'csvHtml5',
        'excelHtml5',
        'pdfHtml5'
    ],
    language: {
        thousands: ",",
        aria: {
            sortAscending: "Used to sort column ascending",
            sortDesending: "Used to sort column descending",
            paginate: {
                first: "Return to First",
                last: "Return to Last",
                next: "Go to Next",
                previous: "Return to previous",
            }
        },
        searchPlaceholder: "Notes and/or Descriptions",
        zeroRecords: "No Notes and/or Descriptions",
        infoEmpty: "No Notes and/or Descriptions",
        processing: "Loading Notes",
        decimal: "."
    }
});



// //////////////   Make A Payment code   borrowed from lists.js


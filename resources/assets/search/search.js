

    let searchTable = $('#searchResultsTable');

    let key = $('#keysearch').val();

    /*************************
        USED FOR TESTING
     *************************/
    /* $.ajax({
        url: '/search/students?key='+key,
        type: 'GET',
        success: function(results){
            console.log(results);
        }
    }) */
    /**************************
     *  END TESTING BLOCK
     ***************************/

    searchTable.dataTable();

    $('#searchResultsTable').dataTable({
        paging: true,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.childRow,
                type: 'inline'
            },
            orthogonal: 'responsive'
        },
        processing: true,
        order: [
            [1, 'asc']
        ],
        destroy: true,
        dom: "l<'formsTableButtons'Bf>rtip",
        rowId: 'DT_RowId',
        buttons: [
            'copyHtml5',
            'csvHtml5',
            'excelHtml5',
            'pdfHtml5'
        ],
        columnDefs:[
            {
                targets: [0],
                orderable: false
            },
            {
                targets: [3, 4, 5, 6, 7, 8],
                searchable: false
            }
        ],
        language: {
            thousands: ",",
            aria: {
                sortAscending: "Used to sort column ascending",
                sortDesending: "Used to sort column descending",
                paginate: {
                    first: "Return to First",
                    last: "Return to Last",
                    next: "Go to Next",
                    previous: "Return to previous",
                }
            },
            searchPlaceholder: "Student Name OR ID",
            zeroRecords: " Your search does not return a student.  Expand your search criteria for more results.",
            infoEmpty: "No Forms to List",
            processing: "Loading Forms",
            decimal: "."
        }
    });

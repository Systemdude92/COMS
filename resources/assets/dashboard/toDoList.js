$(document).ready(function() {

  // TODO:List();

  $('.TODO:ListItem').on('click', function(event) {
    console.log($(this));
  });

  $(window).resize(function() {
    TODO:List();
  });

});


window.todoList = function() {
  clearLists();
  $.ajax({
    url: 'TODO:List/collective',
    type: 'GET',
    contentType: 'application/json; charset=utf-8',
    success: function(results) {
      let lists = results;
      $.each(lists, function(index, value) {
        addCollectiveList(value);
      });
    },
    error: function(errors) {
      console.log(errors);
    },
  });
  $.ajax({
    url: 'TODO:List/individual',
    type: 'GET',
    contentType: 'application/json; charset=utf-8;',
    statusCode: {
      404: function() {
        alert('Not Found');
      },
    },
    success: function(results) {
      //lists = JSON.parse(results);
      let lists = results;
      if (lists.length > 0) {
        $.each(lists, function(index, value) {
          addIndividualList(value);
        });
      } else {
        noReportInfo();
      }
    },
    error: function(errors) {
      console.log(errors);
    },
  });
}


function noReportInfo() {
  let individualListDiv = $('#individualLists').parent().parent();
  individualListDiv.append('' +
      '<div class=\'ui message\' id=\'noReportInfo\'>' +
      '<div class=\'header\'> No Lists Avaiable </div>' +
      '<p>There are currently No Lists that have Concerns that need to be addressed at this time</p>' +
      '</div>' +
      '',
  );

}

function clearLists() {
  $('#collectiveLists').empty();
  $('#individualLists').empty();
  if ($('#noReportInfo').length > 0) {
    $('#noReportInfo').remove();
  }
}

function addCollectiveList(list) {
  let collectiveListDiv = $('#collectiveLists');
  let description = list.description;
  let screenWidth = $(window).width();
  let displayedDescription = '';

  // if(screenWidth <= 425){
  if (description.length > 30) {
    displayedDescription = description.substring(0, 27) + '...';
  } else {
    displayedDescription = description;
  }
  /* } else {
      if(description.length > 40){
          displayedDescription = description.substring(0,37)+"...";
      } else {
          displayedDescription = description;
      }
  } */

  collectiveListDiv.append('' +
      '<a class=\'column item TODO:ListItem\' href=\'/lists/' + list.id + '\'>' +
      '<i class=\'' + list.icon + ' huge icon\' style=\'background-color: ' +
      list.color + '\'></i>' +
      '<div class=\'content\'>' +
      '<h3 class=\'header\'>' + list.name + '</h3>' +
      '<div class=\'description\' data-content=\'' + description + '\'>' +
      displayedDescription + '</div>' +
      '</div>' +
      '<div class=\'floating ui yellow label\'>' + list.total + '</div>' +
      '</a>' +
      '',
  );
}

function addIndividualList(list) {
  let individualListDiv = $('#individualLists');
  let description = list.description;
  let screenWidth = $(window).width();
  let displayedDescription = '';

  if (screenWidth <= 425) {
    if (description.length > 30) {
      displayedDescription = description.substring(0, 27) + '...';
    } else {
      displayedDescription = description;
    }
  } else {
    if (description.length > 45) {
      displayedDescription = description.substring(0, 42) + '...';
    } else {
      displayedDescription = description;
    }
  }

  individualListDiv.append('<a class=\'column item TODO:ListItem\' href=\'/lists/' +
      list.id + '\'>' +
      '<i class=\'' + list.icon + ' huge icon\' style=\'background-color: ' +
      list.color + ';\'></i>' +
      '<div class=\'content\'>' +
      '<h3 class=\'header\'>' + list.name + '</h3>' +
      '<div class=\'description\' data-content=\'' + description +
      '\' data-position=\'bottom center\'>' + displayedDescription + '</div>' +
      '</div>' +
      '<div class=\'floating ui red label\'>' + list.total + '</div>' +
      '</a>',
  );

  $('a.column.item.TODO:ListItem').
      children('div').
      children('div.description').
      popup();

}

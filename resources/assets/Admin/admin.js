$(document).ready(function(){
    $('#userSelector').dropdown({
        fullTextSearch: true,
        sortSelect: true,
        onChange: function(value, text, $choice){
            getUserInfo(value);
        }
    });



})

function getUserInfo(userID)
{
    if($('#userInfoDiv').length > 0){
        $('#userInfoDiv').remove();
    }
    $.ajax({
        url: '/admin/user/'+userID,
        type: 'GET',
        success: function(results){
            console.log(results);
            let adminDiv = $('#adminIndividualUserContainer').children('div');
            let userAdminArea = `
            <div id='userInfoDiv'>
            <div class='ui divider'></div>
            <h2 class='ui header'>COMS Role: </h2>
            <select class='ui dropdown' id='comsRoleSelect'>`
            $.each(results.comsRoles, function(index, role){
                if(role.active == 1){
                    userAdminArea += `<option selected value='${role.id}'>${role.name}</option>`;
                } else {
                    userAdminArea += `<option value='${role.id}'>${role.name}</option>`;
                }
            })
            userAdminArea += `</select>
            <div class='ui divider'></div>
            <h2 class='ui header'>Lists</h2>
            <table class='ui celled table'>
                <thead>
                    <tr>
                        <th>Report Name</th>
                        <th class='center aligned'><div>Read</div><div class='ui checkbox' id='readToggleAll'>
                        <label></label>
                        <input type='checkbox'/>
                        </div></th>
                        <th class='center aligned'><div>Write</div><div class='ui checkbox' id='writeToggleAll'>
                        <label></label>
                        <input type='checkbox'/>
                        </div></th>
                    </tr>
                </thead>
                <tbody>`;

            $.each(results.reports, function(index, report){
                if($.inArray(parseInt(report.id), results.collectiveLists) == -1){
                    userAdminArea += `<tr>
                        <td class='reportName' id='${report.id}'>${report.name}</td>`;
                        if($.inArray(report.id, results.readLists) > -1){
                            userAdminArea += `<td class='center aligned'><div class='ui checked checkbox read'>
                                                <label></label>
                                                <input type='checkbox' checked='' />
                                                </div></td>`;
                        } else {
                            userAdminArea += `<td class='center aligned'><div class='ui checkbox read'>
                                                <label></label>
                                                <input type='checkbox'/>
                                                </div></td>`;
                        }

                        if($.inArray(report.id, results.writeLists) > -1){
                            userAdminArea += `<td class='center aligned'><div class='ui checkbox write'>
                                                <label></label>
                                                <input type='checkbox' checked />
                                                </div></td>`;
                        } else {
                            userAdminArea += `<td class='center aligned'><div class='ui checkbox write'>
                                                <label></label>
                                                <input type='checkbox'/>
                                                </div></td>`;
                        }

                    userAdminArea += `</tr>`
                }
            })

            userAdminArea += `</tbody>
            </table>
            <button class='ui positive button' id='adminSaveButton'>Save</button>
            <button style='display:none' class='ui positive loading button' disabled id='adminLoadingButton'>Loading</button>
            </div>`;
            adminDiv.append(userAdminArea);

            $('#comsRoleSelect').dropdown({
                debug: false
            });

            $('.ui.checkbox').checkbox();

            $('#adminSaveButton').on('click', function(event){
                event.preventDefault();
                saveUserInfo();
            })

            $('#readToggleAll').checkbox({
                onChange: function(){
                    let isChecked = $(this).parent().checkbox('is checked');
                    if(isChecked){
                        selectAll('read');
                    } else {
                        deselectAll('read');
                    }
                }
            })

            $('#writeToggleAll').checkbox({
                onChange: function(){
                    let isChecked = $(this).parent().checkbox('is checked');
                    if(isChecked){
                        selectAll('write');
                    } else {
                        deselectAll('write');
                    }
                }
            })



        }
    })
}

function selectAll(type)
{
    typeCheckBoxes = $('div.ui.checkbox.'+type);
    $.each(typeCheckBoxes, function(index, checkBox){
        let box = $(checkBox);
        box.checkbox('set checked');
    });
}

function deselectAll(type)
{
    typeCheckBoxes = $('div.ui.checkbox.'+type);
    $.each(typeCheckBoxes, function(index, checkBox){
        let box = $(checkBox);
        box.checkbox('set unchecked');
    });
}

function saveUserInfo()
{
    showLoadingButton();
    let username = $('#userSelector').dropdown('get value');
    let reports = $('.reportName');
    let readLists = '';
    let writeLists = '';
    $.each(reports, function(index, report){
        report = $(report);
        // console.log(report.attr('id'));
        let isReadable = report.parent().children('td').children('div.ui.checkbox.read').checkbox('is checked');
        let isWritable = report.parent().children('td').children('div.ui.checkbox.write').checkbox('is checked');
        if(isReadable){
            readLists += report.attr('id')+","
        }
        if(isWritable){
            writeLists += report.attr('id')+","
        }
    })
    readLists = readLists.slice(0, -1);
    writeLists = writeLists.slice(0,-1);
    let comsRole = $('#comsRoleSelect').dropdown('get value');

    console.log(readLists);
    console.log(writeLists);
    $.ajax({
        url: '/admin/user/'+username,
        type: 'POST',
        data: {
            'readLists' : readLists,
            'writeLists' : writeLists,
            'comsRole' : comsRole
        },
        success: function(results){
            hideLoadingButton();
            if(results == 'true'){
                $('#adminSaveButton').parent().append(`<h3 class='ui green label'><i class='check circle icon'></i>User Updated!</h3>`);
            } else {
                $('#adminSaveButton').parent().append(`<h3 class='ui red label'><i class='remove circle icon'></i>Failed to Updated User Info!</h3>`);
            }
            console.log(results);
        },
        error: function(error){
            hideLoadingButton();
            $('#adminSaveButton').parent().append(`<h3 class='ui red label'><i class='remove circle icon'></i>Failed to Send Request to Server</h3>`);
        }
    })
}

function showLoadingButton()
{
    $('#adminSaveButton').hide();
    $('#adminLoadingButton').show();
}

function hideLoadingButton()
{
    $('#adminSaveButton').parent().children('h3').remove();
    $('#adminSaveButton').show();
    $('#adminLoadingButton').hide();
}

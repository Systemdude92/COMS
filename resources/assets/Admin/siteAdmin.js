$(document).ready(function() {

    let detentionKioskActive = $('#detentionKioskForm').children('input[type=checkbox]').prop('checked');

    $('label > span').popup();

    if(detentionKioskActive){
        unlockDetentionKiosk();
    }

    $('#detentionSave').on('click', function(event){
        event.preventDefault();
        hideSuccess();
        hideSaveButton();
        clearErrors();

        //Initializing Data Variable
        let data = {};

        //Assume Form is not ready to send
        let isReady = false;
        let errors = [];

        //Collect Variables from form
        if($('#detentionKioskForm').children('input[type=checkbox]').prop('checked')){
            data.detentionKiosk = true; //Setting Detention Kiosk to Active
            data.detention = {};
            //Setting Variables to test for validation before adding them to data object
            let startDate = $('#detentionStartDate').val();
            let duration = $('select[name=detentionDuration]').val();
            let threshold = $('#threshHoldInput').val();
            let days = $('#daysServeInput').val();

            //Validating User Input
            if(startDate.length != 10){
                $('#detentionStartDate').addClass('kioskError');
                errors.push('DETENTION: Please enter valid Start Date');
            }
            if(isNaN(threshold) || threshold == 0){
                $('#threshHoldInput').addClass('kioskError');
                errors.push('DETENTION: Threshold Should be a valid Number');
            }
            if(isNaN(days) || days == 0){
                $('#daysServeInput').addClass('kioskError');
                errors.push('DETENTION: Days to Serve should be a valid number');
            }

            // If no Errors, add variables to Data object

            if(errors.length == 0 ){
                data.detention.startDate = startDate;
                data.detention.duration = duration;
                data.detention.threshold = threshold;
                data.detention.days = days;
            }
        } else {
            //if Detantion Kiosk is not checked, make kiosk false
            data.detentionKiosk = false;
        }


        //If No Errors were found, mark Ready to True
        if(errors.length == 0){
            isReady = true;
        }

        let inputLists = $('.aliasList');
        let lists = [];

        $.each(inputLists, function(index, list){
            let input = $(list);
            let listID = input.attr('id');
            let defaultName = input.children('.listDefaultName').text();
            let aliasName = input.children('.listAliasName').children('input').val();
            lists.push({
                id: listID,
                defaultName: defaultName,
                aliasName: aliasName
            });
        });

        data.lists = (lists);

        console.log(data);

        if(isReady){

            //TODO: Set up AJAX to send to server
           axios({
               url: '/admin/kiosk',
               method: 'post',
               data: data,
               headers: {
                   'Content-Type': 'application/json; charset=utf-8'
               }
           }).then(results => {
               showSaveButton();
               showSuccess();
           }).catch(error => {
               showSaveButton();
               errors = error.response.data;

               showErrors(errors);
               console.log(error.response.data);
           });
        } else {
            showSaveButton();
            showErrors(errors);
        }
    });

    $('#detentionStartDate').keypress(function(event) {
        clearErrors();
        hideSuccess();
        // displays Text as Date format
        // Only allows user to inter digits into the field

        // Setting Variables needed for process
        let key = String.fromCharCode(event.keyCode);
        let input = $('#detentionStartDate').val();

        // If key is not a number or 0 AND if the length of the input is too long, it will deny the entry
        if((!isNumber(key)) || (input.length > 9)) {
            return false;
        }

        if(input.length == 2 || input.length == 5){
            input = input+"/";
        }

        $('#detentionStartDate').val(input);
    });

    $('#threshHoldInput').keypress(function(event){
        clearErrors();
        hideSuccess();
        let key = String.fromCharCode(event.keyCode);
        if(!/\d/.test(key)){
            return false;
        }
    });

    $('#daysServeInput').keypress(function(event){
        clearErrors();
        hideSuccess();
        let key = String.fromCharCode(event.keyCode);
        if(!parseInt(key)){
            return false;
        }
    });

    $('#detentionKioskForm').children('input[type=checkbox]').on('change', function(event){
        event.preventDefault();
        clearErrors();
        hideSuccess();
        if($(this).prop('checked')){
            unlockDetentionKiosk();
        } else {
            lockDetentionKiosk();
        }
    });

});

function isNumber(key){
    return /\d/.test(key);
}

function unlockDetentionKiosk()
{
    $('#detentionStartDate').prop('disabled', false);
    $('#threshHoldInput').prop('disabled', false);
    $('#daysServeInput').prop('disabled', false);
    $('select[name=detentionDuration]').prop('disabled', false);
}

function lockDetentionKiosk()
{
    $('#detentionStartDate').prop('disabled', true);
    $('#threshHoldInput').prop('disabled', true);
    $('#daysServeInput').prop('disabled', true);
    $('select[name=detentionDuration]').prop('disabled', true);
}

/** Hides Save buttona and shows loading button */
function hideSaveButton()
{
    $('#detentionSave').hide();
    $('#detentionLoad').show();
}

/** Shows Save Button and Hides Loading Button */
function showSaveButton()
{
    $('#detentionSave').show();
    $('#detentionLoad').hide();
}

/** Shows Errors on the screen */
function showErrors(errors)
{
    let div = $('.ui.raised.left.aligned.stacked.segment > div.ui.negative.message');
    let list = $('.ui.raised.left.aligned.stacked.segment > div.ui.negative.message > ul');
    //let list = $('#detentionKioskForm').children('div.ui.negative.message').children('ul');
    errors.forEach(function(error){
        list.append(`<li>${error}</li>`);
    });
    div.show();
}

function clearErrors(){
    let div = $('.ui.raised.left.aligned.stacked.segment > div.ui.negative.message');
    let list = $('.ui.raised.left.aligned.stacked.segment > div.ui.negative.message > ul');
    list.empty();
    div.hide();
    $('.kioskError').removeClass('kioskError');
}

function showSuccess()
{
    $('.ui.raised.left.aligned.stacked.segment > div.ui.positive.message').show();
}

function hideSuccess()
{
    $('.ui.raised.left.aligned.stacked.segment > div.ui.positive.message').hide();
}

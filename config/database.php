<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'sqlsrv'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'SkywardDataExtracts' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_SDE_HOST', 'localhost'),
            'port' => env('DB_SDE_PORT', '1433'),
            'database' => env('DB_SDE_DATABASE', 'forge'),
            'username' => env('DB_SDE_USERNAME', 'forge'),
            'password' => env('DB_SDE_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'ComsDB' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_COMS_HOST', 'localhost'),
            'port' => env( 'DB_COMS_PORT', '1433'),
            'database' => env('DB_COMS_DATABASE', 'forge'),
            'username' => env('DB_COMS_USERNAME', 'forge'),
            'password' => env('DB_COMS_PASSWORD', 'forge'),
            'charset' => PDO::SQLSRV_ENCODING_UTF8,
            'prefix' => '',
        ],

        'LISDDW' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_LISDDW_HOST', 'localhost'),
            'port' => env( 'DB_LISDDW_PORT', '1433'),
            'database' => env('DB_LISDDW_DATABASE', 'forge'),
            'username' => env('DB_LISDDW_USERNAME', 'forge'),
            'password' => env('DB_LISDDW_PASSWORD', 'forge'),
            'charset' => PDO::SQLSRV_ENCODING_UTF8,
            'prefix' => '',
        ],

        'Pro2Sql' => [
            'driver' => env('DB_PRO2SQL_CONNECTION','sqlsrv'),
            'host' => env('DB_PRO2SQL_HOST', 'localhost'),
            'port' => env( 'DB_PRO2SQL_PORT', '1433'),
            'database' => env('DB_PRO2SQL_DATABASE', 'forge'),
            'username' => env('DB_PRO2SQL_USERNAME', 'forge'),
            'password' => env('DB_PRO2SQL_PASSWORD', 'forge'),
            'charset' => PDO::SQLSRV_ENCODING_UTF8,
            'prefix' => '',
        ],

        'TipWeb' => [
            'driver' => env('DB_TIPWEB_CONNECTION','sqlsrv'),
            'host' => env('DB_TIPWEB_HOST', 'localhost'),
            'port' => env( 'DB_TIPWEB_PORT', '1433'),
            'database' => env('DB_TIPWEB_DATABASE', 'forge'),
            'username' => env('DB_TIPWEB_USERNAME', 'forge'),
            'password' => env('DB_TIPWEB_PASSWORD', 'forge'),
            'charset' => PDO::SQLSRV_ENCODING_UTF8,
            'prefix' => '',
        ]

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
